<!--begin::Entry-->
<div class="d-flex flex-column-fluid">

	<!--begin::Container-->
	<div class="container-fluid">

		<style type="text/css">
			.tdt td {
				white-space: nowrap;
			}

			.dataTables_wrapper table.dataTable.collapsed > tbody > tr[role="row"] > td:first-child {
			    padding-left: 20px !important;
			}

			.dataTables_wrapper table.dataTable.dtr-inline.collapsed > tbody > tr[role="row"] > td:first-child:before {
				margin-top: -5px;
			}
			
			.dataTables_wrapper .dataTable td{
				padding: 3px 5px !important;
			}

			.form-control[readonly] {
			    background-color: #F3F6F9;
			}
		</style>
		<!--begin::Dashboard-->
		@if (Request::segment(2) == '')

			<!--begin::Card-->
			<div class="card card-custom">
				<div class="card-header">
					<div class="card-title">
						<span class="card-icon">
							<i class="fas fa-users text-primary"></i>
						</span>
						<h3 class="card-label">{{ __('words.users') }}</h3>
					</div>
					<div class="card-toolbar">
						
						
							<a href="{{ url()->current() }}/add" class="btn btn-primary font-weight-bolder"><i class="fas fa-plus"></i> {{ __('words.add_user') }}</a>
						
						
					</div>
				</div>
				<div class="card-body">
					<!--begin: Search Form-->
					<form class="mb-5" id='searchFormm'>
						<div class="row mb-6">
							<!--
							<div class="col-lg-3 mb-5">
								<label> TC </label>
								<input type="text" class="form-control datatable-input" data-col-index="1" />
							</div>
							-->
							<div class="col-lg-4 mb-5">
								<label> {{ __('words.name_surname') }} </label>
								<input type="text" class="form-control datatable-input" data-col-index="2" />
							</div>

							<div class="col-lg-4 mb-5">
								<label> {{ __('words.email') }} </label>
								<input type="text" class="form-control datatable-input" data-col-index="3" />
							</div>

							<div class="col-lg-4 mb-5">
								<label> {{ __('words.phone') }} </label>
								<input type="text" class="form-control datatable-input" data-col-index="6" />
							</div>
							
							<div class="col-lg-4 mb-5">
								<label> {{ __('words.city') }} </label>
								<select class="form-control datatable-input" id="select2_province_search" data-col-index="4">
									<option></option>
									@php
										//RoleProvince
										$roleProvince = collect(Auth::user()->roleprovince)->pluck('province_id');
								        if($roleProvince->count() > 0){
								            $provinces = App\Province::whereIn('id', $roleProvince)->get();
								        }else{
								            $provinces = App\Province::all();
								        }
									@endphp
									@foreach ($provinces as $province)
										<option value="{{ $province->id }}"> {{ $province->name }} </option>
									@endforeach
								</select>
							</div>
							<!--
							<div class="col-lg-3 mb-5">
								<label> İlçe </label>
								<select class="form-control datatable-input" id="select2_district_search" multiple="multiple" data-col-index="4">
								</select>
							</div>

							<div class="col-lg-3 mb-5">
								<label> {{ __('words.authorities') }} </label>
								<select class="form-control datatable-input" id="select2_staff_search">
									<option></option>
									@foreach (Spatie\Permission\Models\Role::all() as $role)
										<option value="{{ $role->name }}"> {{ $role->name }} </option>
									@endforeach
								</select>
							</div>
							
							<div class="col-lg-3 mb-5">
								<label> {{ __('words.user_type') }} </label>
								<select class="form-control datatable-input" id="select2_user_type_search">
									<option></option>
									<option value="superuser"> {{ __('words.super_user') }} </option>
									<option value="coordinator"> {{ __('words.coordinator') }} </option>
									<option value="referee"> {{ __('words.referee') }} </option>
									<option value="player"> {{ __('words.player') }} </option>
									<option value="only_user"> {{ __('words.user') }} </option>
								</select>
							</div>
							-->
							<div class="col-lg-4 mb-5">
								<label> {{ __('words.team') }} </label>
								<select class="form-control datatable-input" id="select2_team_search">
									<option></option>
									<option value="yes"> {{ __('words.available') }} </option>
									<option value="no"> {{ __('words.not_available') }} </option>
								</select>
							</div>
							<!--
							<div class="col-lg-3 mb-5">
								<label> {{ __('words.active') }} </label>
								<select class="form-control datatable-input" id="select2_active_search">
									<option></option>
									<option value="yes"> {{ __('words.yes') }} </option>
									<option value="no"> {{ __('words.no') }} </option>
								</select>
							</div>
							
							<div class="col-lg-3 mb-5">
								<label> {{ __('words.information_sharing_permission') }} </label>
								<select class="form-control datatable-input" id="select2_infoshare_search">
									<option></option>
									<option value="yes"> {{ __('words.yes') }} </option>
									<option value="no"> {{ __('words.no') }} </option>
									<option value="null"> Henüz Seçilmemiş </option>
								</select>
							</div>
							-->
							<div class="col-lg-4 text-right mb-5">
								<label>&#160;</label>
								<span style="display: block;">
									<button class="btn btn-primary btn-primary--icon btn-sm" id="kt_search">
										<span>
											<i class="fas fa-search"></i>
											<span>{{ __('words.search') }}</span>
										</span>
									</button>&#160;&#160;
									<button class="btn btn-secondary btn-secondary--icon btn-sm" id="kt_reset">
										<span>
											<i class="far fa-times-circle"></i>
											<span>{{ __('words.reset') }}</span>
										</span>
									</button>
								</span>
							</div>
						</div>
					</form>
					<!--begin: Datatable-->
					<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

					<table class="table table-bordered table-hover table-checkable tdt" id="kt_datatable_users" style="margin-top: 13px !important">
					</table>
					<!--end: Datatable-->
				</div>
			</div>
			<!--end::Card-->
		@elseif (Request::segment(2) == 'password')
			@include('contents.user_password')
		@else
			@include('form_element._addForm')
		@endif
		<!--end::Dashboard-->

	</div>

	<!--end::Container-->
</div>

<!--end::Entry-->