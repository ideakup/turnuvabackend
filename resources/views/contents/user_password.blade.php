<!--begin::Card-->
<div class="card card-custom">
	<div class="card-header">
		<div class="card-title">
			<span class="card-icon">
				<i class="fas fa-key"></i>
			</span>
			<h3 class="card-label">{{ $model_data->first_name }} {{ $model_data->last_name }} - {{ __('words.change_password') }}</h3>
		</div>
		<div class="card-toolbar">
		</div>
	</div>
	<div class="card-body">

		<form class="form" method="POST" action="{{ url(Request::segment(1).'/save_password') }}" id="userPasswordForm">
	        <div class="mb-3">

                {{ csrf_field() }}
                <input type="hidden" name="segment" value="{{ Request::segment(1) }}">
                <input type="hidden" name="crud" value="{{ Request::segment(2) }}">
                <input type="hidden" name="id" value="{{ Request::segment(3) }}">
				
			</div>

			<div class="form-group row">
			    <label class="col-form-label text-right col-lg-3 col-sm-12">{{ __('words.new_password') }} : </label>
			    <div class="col-lg-6 col-sm-12">
			        <input class="form-control" type="text" name="password" placeholder="{{ __('words.new_password') }}" max="32" required="">
			    </div>
			</div>

			<div class="form-group row">
			    <label class="col-form-label text-right col-lg-3 col-sm-12">{{ __('words.new_password_again') }} : </label>
			    <div class="col-lg-6 col-sm-12">
			        <input class="form-control" type="text" name="password_repeat" placeholder="{{ __('words.new_password_again') }}" max="32" required="">
			    </div>
			</div>


			<div class="row" style="padding: 2rem 0 0 0; border-top: 1px solid #EBEDF3;">
                <div class="col-lg-3"></div>
                <div class="col-lg-6">
                    <button type="submit" class="btn btn-success mr-2">{{ __('words.save') }}</button>
                    <button type="button" class="btn btn-secondary" onclick="history.back()">{{ __('words.cancel') }}</button>
                </div>
            </div>

		</form>
		
		<!--end: Datatable-->
	</div>
</div>
<!--end::Card-->