<!--begin::Card-->
<div class="card card-custom">
	<div class="card-header">
		<div class="card-title">
			<span class="card-icon">
				<i class="fas fa-users text-primary"></i>
			</span>
			<h3 class="card-label">{{ __('words.team') }} : {{ App\Team::find(Request::segment(3))->name }} ({{ __('words.add_player_from_site') }})</h3>
		</div>
		<div class="card-toolbar">
			<!--begin::Button
			@if (Request::segment(2) == '' && Auth::user()->hasPermissionTo('add_user'))
				<a href="{{ url()->current() }}/add" class="btn btn-primary font-weight-bolder"><i class="fas fa-plus"></i> {{ __('words.add_user') }}</a>
			@endif
			end::Button-->
		</div>
	</div>
	<div class="card-body">
		<!--begin: Search Form-->
		<form class="mb-5" id='searchFormm'>
			<div class="row mb-6">

				<div class="col-lg-3 mb-5">
					<label> {{ __('words.name_surname') }} </label>
					<input type="text" class="form-control datatable-input" data-col-index="1" />
				</div>

				<div class="col-lg-3 mb-5">
					<label> {{ __('words.city') }} </label>
					<select class="form-control datatable-input" id="select2_province_search" data-col-index="3">
						<option></option>
						@php
							//RoleProvince
							$roleProvince = collect(Auth::user()->roleprovince)->pluck('province_id');
					        if($roleProvince->count() > 0){
					            $provinces = App\Province::whereIn('id', $roleProvince)->get();
					        }else{
					            $provinces = App\Province::all();
					        }
						@endphp
						@foreach ($provinces as $province)
							<option value="{{ $province->id }}"> {{ $province->name }} </option>
						@endforeach
					</select>
				</div>

				<div class="col-lg-3 mb-5">
					<label> {{ __('words.team') }} </label>
					<select class="form-control datatable-input" id="select2_team_search">
						<option></option>
						<option value="yes"> {{ __('words.available') }} </option>
						<option value="no"> {{ __('words.not_available') }} </option>
					</select>
				</div>

				<div class="col-lg-3 text-right mb-5">
					<label>&#160;</label>
					<span style="display: block;">
						<button class="btn btn-primary btn-primary--icon btn-sm" id="kt_search">
							<span>
								<i class="fas fa-search"></i>
								<span>{{ __('words.search') }}</span>
							</span>
						</button>&#160;&#160;
						<button class="btn btn-secondary btn-secondary--icon btn-sm" id="kt_reset">
							<span>
								<i class="far fa-times-circle"></i>
								<span>{{ __('words.reset') }}</span>
							</span>
						</button>
					</span>
				</div>
			</div>
		</form>
		<!--begin: Datatable-->
		<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
		<input type="hidden" name="team_id" id="team_id" value="{{ Request::segment(3) }}">

		<table class="table table-bordered table-hover table-checkable tdt" id="kt_datatable_player_list" style="margin-top: 13px !important">
		</table>
		<!--end: Datatable-->
	</div>
</div>
<!--end::Card-->