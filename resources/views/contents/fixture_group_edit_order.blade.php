@php
	// dump($model_data);
@endphp
@php
	$data = array();
	$sid = request()->query('tree_id');
	$season = App\Season::find($sid);
	$groups = App\Group::where('season_id',$sid)->orderBy('name')->get();

    $group_data = array();
    $team_ids = $model_data->group_teams->pluck('team_id');

	$teamseason = App\TeamTeamSeason::select('*', 'team_teamseason.season_id as season', 'team_teamseason.squad_locked_player_id as squad_locked_player')->
    leftJoin('teams', 'teams.id', '=', 'team_teamseason.team_id')->
    where('team_teamseason.season_id', $sid)->
    whereIn('team_teamseason.team_id', $team_ids)->
    where('teams.status', '!=', 'passive');
    $teamseason = $teamseason->orderBy('playoff_member', 'desc');
    $teamseason = $teamseason->orderBy('order', 'asc')->orderBy('point', 'desc')->orderBy('goal_for', 'desc')->get();
    
    foreach ($teamseason as $ts) {

        $teamid = $ts->team_id;
        $teamArr = $ts->toArray();

        $teamArr['last_five_match_result'] = array();
        $l5ms = App\Match::where('season_id', $sid)->where('completed', true)->
            where(function ($query) use ($teamid) {
                $query->where('team1_id', $teamid)
                    ->orWhere('team2_id', $teamid);
            })->orderBy('date', 'desc')->limit(5)->get();

        foreach ($l5ms as $l5m) {
            if($l5m->team1_goal == $l5m->team2_goal){
                $teamArr['last_five_match_result'][] = 0;
            }else if($l5m->team1_goal > $l5m->team2_goal){
                $teamArr['last_five_match_result'][] = 1;
            }else if($l5m->team1_goal < $l5m->team2_goal){
                $teamArr['last_five_match_result'][] = 2;
            }
        }
        $teamArr['avg'] = $ts->goal_for - $ts->goal_against;
        $teamArr['team'] = $ts->team->toArray();

        if(!empty($ts->team->image)){
            $teamArr['team']['image_url'] = env('MEDIA_END').$ts->team->image;
        }else{
            $teamArr['team']['image_url'] = url('cimages/flags/'.strtolower(App\CountryList::find($ts->team->country_id)->code).'.png');
        }

        $teamArr['season_performance']['yellow_card'] = $ts->yellow_card;
        $teamArr['season_performance']['red_card'] = $ts->red_card;
        $teamArr['season_performance']['gk_save'] = $ts->gk_save;

        $r = 0.0;
        foreach ($ts->team->team_playerteamhistory as $value) {
            $r += $value->rating;
        }

        $teamArr['season_performance']['total_rating'] = ($ts->team->team_playerteamhistory->count() > 0) ? number_format((float)($r / $ts->team->team_playerteamhistory->count()), 1, '.', '') : 0;

        $group_data[] = $teamArr;
    }

    $data = $group_data;
@endphp

<!--begin::Entry-->
<div class="d-flex flex-column-fluid">

	<!--begin::Container-->
	<div class="container-fluid">

		<!--[html-partial:begin:{"id":"demo1/dist/inc/view/demos/pages/index","page":"index"}]/-->

		<!--[html-partial:begin:{"id":"demo1/dist/inc/view/partials/content/dashboards/demo1","page":"index"}]/-->
		<style type="text/css">
			.tdt td {
				white-space: nowrap;
			}
			.form-control[readonly] {
			    background-color: #F3F6F9;
			}
		</style>
		<!--begin::Dashboard-->

			<!--begin::Card-->
			<div class="card card-custom">
				<div class="card-header">
					<div class="card-title">
						<span class="card-icon">
							<i class="fas fa-crosshairs"></i>
						</span>
						<h3 class="card-label">
					
						@if(!empty(request()->query('tree_id')))
							@php 
								$sss = App\Season::find(request()->query('tree_id'));
							@endphp
							{{ $sss->year.' '.$sss->league->name.' '.$sss->name }} - 
						@endif
						{{ $model_data->name }}</h3>
						
					</div>
					<div class="card-toolbar">
						<!--begin::Button-->
						<!--end::Button-->
					</div>
				</div>
				<div class="card-body">
					<!--begin: Search Form-->
					<!--begin: Datatable-->
					<form class="form" method="POST" action="{{ url('fixture_group/save_order') }}">
	        
	        			{{ csrf_field() }}
						<input type="hidden" name="tree_id" id="tree_id" value="{{ request()->query('tree_id') }}">
						<input type="hidden" name="group_id" id="group_id" value="{{ $model_data->id }}">

						<table class="table table-separate table-head-custom table-hover tab-table">
						    <thead>
						        <tr>
						            <th style="width: 50px; padding: 12px 10px; text-align: center;"> {{ __('words.pos') }} </th>
	                                <th style="width: 100px;"> {{ __('words.order') }} </th>
	                                <th style="text-align: left;"> {{ __('words.team') }} </th>
	                                <th style="width: 50px;"> {{ __('words.p') }} </th>
	                                <th style="width: 50px;"> {{ __('words.w') }} </th>
	                                <th style="width: 50px;"> {{ __('words.d') }} </th>
	                                <th style="width: 50px;"> {{ __('words.l') }} </th>
	                                <th style="width: 50px;"> {{ __('words.gf') }} </th>
	                                <th style="width: 50px;"> {{ __('words.ga') }} </th>
	                                <th style="width: 50px;"> {{ __('words.gd') }} </th>
	                                <th style="width: 50px;"> {{ __('words.pts') }} </th>
						        </tr>
						    </thead>
						    <tbody>
						    	@foreach($data as $d)
		                            <tr>
		                                <td class="table-standing-index">{{$loop->index+1}}</td>
		                                <td>
											<input class="form-control" type="number" step="1" name="order_{{$d['team']['id']}}" min="0" max="100" value="{{$d['order']}}">
		                                </td>
		                                <td>{{$d['team']['name']}}</td>
		                                <td>{{$d['match_total']}}</td>
		                                <td>{{$d['won']}}</td>
		                                <td>{{$d['drawn']}}</td>
		                                <td>{{$d['lost']}}</td>
		                                <td>{{$d['goal_for']}}</td>
		                                <td>{{$d['goal_against']}}</td>
		                                <td>{{ ($d['goal_for'] - $d['goal_against']) }}</td>
		                                <td>{{$d['point']}}</td>
		                            </tr>
				                @endforeach
						    </tbody>
						</table>

						<div class="row" style="padding: 2rem 0 0 0; border-top: 1px solid #EBEDF3;">
                            <div class="col-lg-3"></div>
                            <div class="col-lg-6">
                                <button type="submit" class="btn btn-success mr-2">{{ __('words.save') }}</button>
                                <button type="button" class="btn btn-secondary" onclick="history.back()">{{ __('words.cancel') }}</button>
                            </div>
                        </div>

					</form>

					<!--end: Datatable-->
				</div>
			</div>
			<!--end::Card-->
		
		<!--end::Dashboard-->

		<!--[html-partial:end:{"id":"demo1/dist/inc/view/partials/content/dashboards/demo1","page":"index"}]/-->

		<!--[html-partial:end:{"id":"demo1/dist/inc/view/demos/pages/index","page":"index"}]/-->
	</div>

	<!--end::Container-->
</div>

<!--end::Entry-->