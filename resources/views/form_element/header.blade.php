<div class="form-group row mb-1">
    <div class="col-lg-3"></div>
    <div class="col-lg-6">
        <{{ data_get($value, 'type') }} class="font-size-lg text-dark font-weight-bold mb-6 mt-10">{{ __(data_get($value, 'title')) }}</{{ data_get($value, 'type') }}>    
    </div>
</div>