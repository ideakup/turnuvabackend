@php
    /*
        dump($model_data);
        dump($value);
        dump($key);
    */
@endphp
@php
    if(!empty(data_get($value, 'renamecol'))){
        $key_r = data_get($value, 'renamecol');
    }else{
        $key_r = $key;
    }
@endphp
<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">{{ __(data_get($value, 'title')) }} : </label>
    <div class="col-lg-6 col-sm-12">
        <div class="input-group input-group-solid date" id="timepicker_{{$key}}" data-target-input="nearest">
            <input type="text" class="form-control form-control-solid datetimepicker-input" 

            name="{{ $key }}"
            placeholder="{{ __(data_get($value, 'placeholder')) }}" 
            data-target="#timepicker_{{$key}}"

            @if(!empty(data_get($value, 'relationship')))
                value="{{ (!is_null($model_data[data_get($value, 'relationship')])) ? Carbon\Carbon::parse($model_data[data_get($value, 'relationship')][data_get($value, 'relation_col')])->format('d.m.Y') : '' }}"
            @else
                value="{{ (!is_null($model_data)) ? Carbon\Carbon::parse($model_data->$key_r)->format('H:i') : '' }}"
            @endif

            @if(Request::segment(2) == 'view' || Request::segment(2) == 'delete') disabled="disabled" @endif

            />
            <div class="input-group-append" data-target="#timepicker_{{$key}}" data-toggle="datetimepicker">
                <span class="input-group-text">
                    <i class="fas fa-clock"></i>
                </span>
            </div>
        </div>
        @if(!empty(data_get($value, 'description')))
            <span class="form-text text-muted">{{ data_get($value, 'description') }}</span>
        @endif
    </div>
</div>
