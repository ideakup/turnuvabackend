@if(!empty(env('FS_API_KEY')) && Request::segment(2) == 'edit' && $model_data->season->fs_api)
	@if( !empty(data_get($value, 'addvisible')) && data_get($value, 'addvisible') == 'no' && Request::segment(2) == 'add')

	@else
		
		@if($key != 'fsdatasave' || ($key == 'fsdatasave' && $model_data->match_action->count() == 0))

			@php
				$formaStar = new App\Classes\FormaStarClass();
				$leagueId = $model_data->season->league_id;
				$matchId = $model_data->id;
				$result = $formaStar->getMatchActionData($leagueId, $matchId);
			@endphp

			@if($result['result']['status'] == 200)
	            @php // dump(json_decode($result['result']['data'])); @endphp
	            <div class="form-group row">
					<label class="col-form-label text-right col-lg-3 col-sm-12">{{ __(data_get($value, 'title')) }} :</label>
					<div class="col-lg-9 col-md-9 col-sm-12">
						<input name="fs_switch" data-switch="true" type="checkbox" data-handle-width="100"

							@if(!empty($model_data->$key) && $model_data->$key == true)
								checked="checked"
							@endif

							data-on-text="{{ __(data_get($value, 'data')['on']['name']) }}" data-on-color="primary" 
							data-off-text="{{ __(data_get($value, 'data')['off']['name']) }}" data-off-color="warning"

							 @if(Request::segment(2) == 'view' || Request::segment(2) == 'delete'/* || (Request::segment(2) == 'edit' && (!$team1_last_match_control || !$team2_last_match_control))*/) disabled="disabled" @endif

						/>
						@if(!empty(data_get($value, 'description')))
					    	<span class="form-text text-muted">{{ data_get($value, 'description') }}</span>
					    @endif
					</div>
				</div>
	        @else
	            @php // dump($result); @endphp
	        @endif

		@endif
	@endif
@endif
