@php
	//dump($model_data->team1->team_teamplayer);
	//dump($model_data->team2->team_teamplayer);
	//dump($model_data->match_press->press_conferences_players);
	//dump($model_data->match_press);
	//dump($key);

	//dump($value);
	//dump(App\Season::find(request()->query('tree_id'))->league_id);
	//dump('league_id: '.$model_data->team_teamseason->season->league->name);
    //dump('season_id: '.$model_data->team_teamseason->season->id);
    //dump('season_name: '.$model_data->team_teamseason->season->name);
	//dump($model_data->eliminaton_tree_teams);
	
	//dump($team1_last_match_control);
	//dump($team2_last_match_control);
	/*
	dump($model_data);
	if(!empty($model_data->group_teams)){
		dump($model_data->group_teams);

		foreach($model_data->group_teams as $etTeam){
			dump($etTeam->team);
		}
	}
	*/
	//dump(data_get($value, 'dataSource')::find($model_data[$key]));
	//dump($model_data->league_eliminationseason->base_season);
	//dump($model_data->match_press);
	//dd($model_data);
@endphp

@if( !empty(data_get($value, 'addvisible')) && data_get($value, 'addvisible') == 'no' && Request::segment(2) == 'add' )

@else

	<div class="form-group row">
		<label class="col-form-label text-right col-lg-3 col-sm-12">{{ __(data_get($value, 'title')) }} : </label>
		<div class="col-lg-6 col-sm-12">

			@if(!empty(request()->query('tree_id')) && $key == 'league_id')
				@php 
					$ss = App\Season::find(request()->query('tree_id'));
				@endphp
				<input type="hidden" name="league_id" id="select2_league_id" value="{{ $ss->league->id }}">
				<div class="alert alert-secondary" role="alert">{{ $ss->league->name }}</div>
				
			@elseif(!empty(request()->query('tree_id')) && $key == 'season_id')
				@php 
					$ss = App\Season::find(request()->query('tree_id'));
				@endphp
				<input type="hidden" name="season_id" id="select2_season_id" value="{{ $ss->id }}">
				<div class="alert alert-secondary" role="alert">{{ $ss->year }} {{ $ss->league->name }} {{ $ss->name }}</div>
				
			@else
			
				<select class="form-control select2" id="select2_{{$key}}" 
					
					@if(!empty(data_get($value, 'multiple')))
						name="{{ $key }}[]"
						multiple="multiple"
					@else
						name="{{ $key }}"
					@endif 

					@if(Request::segment(2) == 'view' || Request::segment(2) == 'delete') disabled="disabled" @endif


					@if(Request::segment(2) == 'edit' && $key == 'completed_status' && ($model_data->completed || (!$team1_last_match_control || !$team2_last_match_control))) disabled="disabled" @endif

				>
					@if(empty(data_get($value, 'trigger')))
						<option></option>

						@if(empty(data_get($value, 'search')))

							@if(empty(data_get($value, 'dataSource')))

								@if(data_get($value, 'seperator') == 'match_team_player')
									
									@php
										$__team1 = $model_data->team1;
							        @endphp
							        @if(!empty($__team1))
								        <option value="guest1">---{{ '('.$__team1->name.'). '. __('words.guest_player') }} ---</option>
								        @foreach($__team1->player as $__team1_player)
											<option value="{{ $__team1_player->id }}">
												{{ '('.$__team1->name.') '.$__team1_player->user->first_name.' '.$__team1_player->user->last_name }}
											</option>
								        @endforeach
							        @endif
							        @php
										$__team2 = $model_data->team2;
							        @endphp
							        @if(!empty($__team2))
								        <option value="guest2">---{{ '('.$__team2->name.') '. __('words.guest_player') }}---</option>
								        @foreach($__team2->player as $__team2_player)
											<option value="{{ $__team2_player->id }}">
												{{ '('.$__team2->name.') '.$__team2_player->user->first_name.' '.$__team2_player->user->last_name}}
											</option>
								        @endforeach
							   		@endif
							   	@elseif(data_get($value, 'seperator') == 'match_team_player_all')

							   		@if(!empty($model_data->team1))
							   			<option value="team1"> {{ $model_data->team1->name }} </option>
							   		@endif
							   		@if(!empty($model_data->team2))
										<option value="team2"> {{ $model_data->team2->name }} </option>
									@endif

									@php
										$match_team_player_all = $model_data->match_player;
							        @endphp
							        @foreach($match_team_player_all as $match_team_player)
										<option value="{{ $match_team_player->id }}">

											@if(empty($match_team_player->player_id))
												({{ $match_team_player->team->name }}) {{ $match_team_player->guest_name }}
											@else
												({{ $match_team_player->team->name }}) {{ $match_team_player->player->user->first_name }} {{ $match_team_player->player->user->last_name }}
											@endif

										</option>
							        @endforeach
							   	
							   	@elseif(data_get($value, 'seperator') == 'match_team_player_press')
							   		@php
						        		//print_r($model_data->match_press->press_conferences_players);
						        		//die;
						        	@endphp
							        @foreach($model_data->match_player as $m_player)

										<option value="{{ $m_player->id }}"
												@if(!empty($model_data->match_press))
													@foreach($model_data->match_press->press_conferences_players as $m_pcp)

														@if($m_player->id == $m_pcp->matchplayer_id)
															selected="selected"
														@endif 

													@endforeach
												@endif 
											>
											@if(empty($m_player->player_id))
												{{ '('.$m_player->team->name.') '.$m_player->guest_name }}
											@else
												{{ '('.$m_player->team->name.') '.$m_player->player->user->first_name.' '.$m_player->player->user->last_name }}
											@endif
										</option>
							        @endforeach

							    @elseif(data_get($value, 'seperator') == 'match_teams')

							    	@if(!empty($model_data->team1))
										<option value="{{ $model_data->team1->id }}"
											@if(!empty($model_data->match_press))
												@if($model_data->match_press->where('team_id', $model_data->team1->id)->count() > 0)
													selected="selected"
												@endif 
											@endif 
											>
											{{ $model_data->team1->name }}
										</option>
									@endif
									@if(!empty($model_data->team2))
										<option value="{{ $model_data->team2->id }}"
											@if(!empty($model_data->match_press))
												@if($model_data->match_press->where('team_id', $model_data->team2->id)->count() > 0)
													selected="selected"
												@endif 
											@endif 
											>
											{{ $model_data->team2->name }}
										</option>
									@endif

								@elseif(data_get($value, 'seperator') == 'match_teams_press')

							    	@if(!empty($model_data->team1))
										<option value="{{ $model_data->team1->id }}">
											{{ $model_data->team1->name }}
										</option>
									@endif
									@if(!empty($model_data->team2))
										<option value="{{ $model_data->team2->id }}">
											{{ $model_data->team2->name }}
										</option>
									@endif
									
							    @endif

							@else

								@php
									if( data_get($value, 'dataSource') == 'json_data'){
										$dataSource = data_get($value, 'json_data');
									}else{

										if($key == 'province_id'){
											//RoleProvince
											$roleProvince = collect(Auth::user()->roleprovince)->pluck('province_id');
									        if($roleProvince->count() > 0){
									            $dataSource = data_get($value, 'dataSource')::whereIn('id', $roleProvince)->get();
									        }else{
									            $dataSource = data_get($value, 'dataSource')::all();
									        }
										}else if($key == 'league_id'){
											//RoleProvince
											$roleProvince = collect(Auth::user()->roleprovince)->pluck('province_id');
									        if($roleProvince->count() > 0){
									            $dataSource = App\League::
										        select( 'leagues.id',
										                'leagues.name',
										                'leagues.active',
										                DB::raw("count(leagues_provinces.province_id) as count")
										        )->
										        leftJoin('leagues_provinces', 'leagues.id', '=', 'leagues_provinces.league_id')->
										        whereIn('leagues_provinces.province_id', $roleProvince)->where('leagues.active', 'true')->groupBy('leagues.id')->get();
									        }else{
									            $dataSource = App\League::where('leagues.active', 'true')->get();
									        }
										}else{
											$dataSource = data_get($value, 'dataSource')::all();
										}

									}
								@endphp

								@foreach ($dataSource as $valuee)
									
									<option value="{{ $valuee['id'] }}"

										@if(empty(data_get($value, 'add_only')))
											
											@if(!empty($model_data))

												@if(!empty($value_tabs) && !empty(data_get($value_tabs, 'relationship')))
													
													@php
														if(!empty(data_get($value, 'renamecol'))){
															$key_r = data_get($value, 'renamecol');
														}else{
															$key_r = $key;
														}
													@endphp

													@if( !empty($model_data[data_get($value_tabs, 'relationship')][$key_r]) )
				                						@if( $model_data[data_get($value_tabs, 'relationship')][$key_r] ==  $valuee['id'] )
															selected="selected"
														@endif
													@endif

												@elseif(!empty(data_get($value, 'relationship')))
												
													@if(empty(data_get($value, 'multiple')))

														@if( $model_data[data_get($value, 'relationship')][data_get($value, 'relation_col')] ==  $valuee['id'] )
															selected="selected"
														@endif

													@else

														@foreach ($model_data[data_get($value, 'relationship')] as $selectedVal)
															@if( $selectedVal[data_get($value, 'relation_col')] ==  $valuee['id'] )
																selected="selected"
															@endif
														@endforeach

													@endif

												@else

													@if(data_get($value, 'seperator') == 'user_role_id')
														@if(!empty($model_data->getRoleNames()))
															@if(in_array($valuee['name'], $model_data->getRoleNames()->toArray()))
																selected="selected"
															@endif
														@endif
													@elseif(data_get($value, 'seperator') == 'reservation_league_id')
												        @if($model_data->team_teamseason->season->league_id == $valuee['id'])
															selected="selected"
														@endif
													@elseif(data_get($value, 'seperator') == 'elimination_tree_league_id')
												        @if($model_data->league_eliminationseason->season_ptr->league_id == $valuee['id'])
															selected="selected"
														@endif
													@elseif(data_get($value, 'seperator') == 'fixture_group_league_id')
												        @if($model_data->league_fixtureseason->season_ptr->league_id == $valuee['id'])
															selected="selected"
														@endif
													@elseif($model_data[$key] == $valuee['id'])
														selected="selected"
													@endif

												@endif
											
											@endif
										@endif
									>
										{{ __($valuee['name']) }}
									</option>

								@endforeach

							@endif

						@else

							@if(data_get($value, 'relationship') == 'user')

								@php
									$__user = data_get($value, 'dataSource')::find($model_data[$key])[data_get($value, 'relationship')];
								@endphp
								@if(!is_null($__user))
									<option value="{{ $model_data[$key] }}" selected="selected">
										{{ '(TC:'.$__user->profile->identity_number.') '.$__user->first_name.' '.$__user->last_name }}
									</option>
								@endif
							@elseif(data_get($value, 'seperator') == 'active_season')

								@php
									$__season = data_get($value, 'dataSource')::find($model_data[$key]);
						        @endphp

						        @if(!is_null($__season))
									<option value="{{ $model_data[$key] }}" selected="selected">
										{{ $__season->id.' '.$__season->league->name.' '.$__season->year.' '.$__season->name }}
									</option>
								@endif
							@elseif(data_get($value, 'seperator') == 'base_season')

								@php
									// gogo
									$__season = $model_data;
						        @endphp

						        @if(!is_null($__season))
						        	@php
										$__baseseason = $model_data->league_eliminationseason->base_season;
							        @endphp
							        @if(!is_null($__baseseason))
										<option value="{{$__baseseason->id }}" selected="selected">
											{{ $__baseseason->league->name.' '.$__baseseason->year.' '.$__baseseason->name }}
										</option>
									@endif
								@endif
							@elseif(data_get($value, 'seperator') == 'match_team')

								@php
									$__team = App\Team::find($model_data[$key]);
						        @endphp
						        
						        @if(!is_null($__team))
									<option value="{{ $model_data[$key] }}" selected="selected">
										{{ $__team->name }}
									</option>
								@endif
							@elseif(data_get($value, 'seperator') == 'match_ground')

								@php
									$__ground = App\Ground::find($model_data[$key]);
						        @endphp
						        
						        @if(!is_null($__ground))
									<option value="{{ $model_data[$key] }}" selected="selected">
										{{ $__ground->name }}
									</option>
								@endif
							@elseif(data_get($value, 'seperator') == 'match_referee')

								@php
									$__user = App\Referee::find($model_data[$key]);
						        @endphp
						        
						        @if(!is_null($__user))
									<option value="{{ $model_data[$key] }}" selected="selected">
										{{ $__user->user->first_name.' '.$__user->user->last_name }}
									</option>
								@endif
							@elseif(data_get($value, 'seperator') == 'match_coordinator')

								@php
									$__user = App\Coordinator::find($model_data[$key]);
						        @endphp
						        
						        @if(!is_null($__user))
									<option value="{{ $model_data[$key] }}" selected="selected">
										{{ $__user->user->first_name.' '.$__user->user->last_name }}
									</option>
								@endif
							@elseif(data_get($value, 'seperator') == 'reservation_season_id' && !empty($model_data))

								@php
									$__season = App\Season::find($model_data->team_teamseason->season->id);
						        @endphp
						        
						        @if(!is_null($__season))
									<option value="{{ $__season->id }}" selected="selected">
										{{ $__season->league->name.' '.$__season->year.' '.$__season->name }}
									</option>
								@endif
							@elseif(data_get($value, 'seperator') == 'reservation_ground_id' && !empty($model_data))

								@php
									$__grounds = App\Ground::find($model_data->reservation_groundtable->ground->id);
						        @endphp
						        
						        @if(!is_null($__grounds))
									<option value="{{ $__grounds->id }}" selected="selected">
										{{ $__grounds->name }}
									</option>
								@endif
							@elseif(data_get($value, 'seperator') == 'reservation_team_id' && !empty($model_data))

								@php
									$__team = App\Team::find($model_data->team_teamseason->team->id);
						        @endphp
						        
						        @if(!is_null($__team))
									<option value="{{ $__team->id }}" selected="selected">
										{{ $__team->name }}
									</option>
								@endif
							@elseif(data_get($value, 'seperator') == 'match_time' && !empty($model_data))
						        
						        @if(!is_null($model_data->time))
									<option value="{{ $model_data->time }}" selected="selected">
										{{ Carbon\Carbon::parse($model_data->time)->format('H:i') }}
									</option>
								@endif
							@elseif(data_get($value, 'seperator') == 'reservation_time' && !empty($model_data))
						        
						        @if(!is_null($model_data->reservation_groundtable))
									<option value="{{ $model_data->reservation_groundtable->id }}" selected="selected">
										{{ Carbon\Carbon::parse($model_data->reservation_groundtable->time)->format('H:i') }}
									</option>
								@endif
							@elseif(data_get($value, 'seperator') == 'elimination_tree_season_id' && !empty($model_data))
						        
						        @if(!is_null($model_data->league_eliminationseason->season_ptr))
									<option value="{{ $model_data->league_eliminationseason->season_ptr->id }}" selected="selected">
										{{ $model_data->league_eliminationseason->season_ptr->year.' '.$model_data->league_eliminationseason->season_ptr->league->name.' '.$model_data->league_eliminationseason->season_ptr->name }}
									</option>
								@endif
							@elseif(data_get($value, 'seperator') == 'fixture_group_season_id' && !empty($model_data))
						        
						        @if(!is_null($model_data->league_fixtureseason->season_ptr))
									<option value="{{ $model_data->league_fixtureseason->season_ptr->id }}" selected="selected">
										{{ $model_data->league_fixtureseason->season_ptr->year.' '.$model_data->league_fixtureseason->season_ptr->league->name.' '.$model_data->league_fixtureseason->season_ptr->name }}
									</option>
								@endif
							@elseif(data_get($value, 'seperator') == 'elimination_tree_teams')

								@if(!empty($model_data->eliminaton_tree_teams))
							    	@foreach($model_data->eliminaton_tree_teams as $etTeam)
										<option value="{{ $etTeam->team_id }}" selected="selected">
											{{ $etTeam->team->name }}
										</option>
									@endforeach
								@endif
							@elseif(data_get($value, 'seperator') == 'fixture_group_teams')

								@if(!empty($model_data->group_teams))
							    	@foreach($model_data->group_teams as $etTeam)
										<option value="{{ $etTeam->team_id }}" selected="selected">
											{{ $etTeam->team->name }}
										</option>
									@endforeach
								@endif

							@elseif(data_get($value, 'seperator') == 'player_penalties_season_id')
								
								@if(!empty($model_data))
									@php
										$__season = App\Season::find($model_data->season_id);
							        @endphp
							        
							        @if(!is_null($__season))
										<option value="{{ $__season->id }}" selected="selected">
											{{ $__season->league->name.' '.$__season->year.' '.$__season->name }}
										</option>
									@endif
								@endif

							@elseif(data_get($value, 'seperator') == 'player_penalties_match_id')
								
								@if(!empty($model_data))
									@php
										$__match = App\Match::find($model_data->match_id);
							        @endphp
							        
							        @if(!is_null($__match))
										<option value="{{ $__match->id }}" selected="selected">
											{{ $__match->team1->name.' - '.$__match->team2->name }}
										</option>
									@endif
								@endif

							@elseif(data_get($value, 'seperator') == 'ppenalties_type_id')
								
								@if(!empty($model_data))
									@php
										$__playerPenaltyType = App\PlayerPenalty::find($model_data->id)->penalty_type;
							        @endphp
							        
							        @if(!is_null($__playerPenaltyType))
										<option value="{{ $__playerPenaltyType->id }}" selected="selected">
											{{ $__playerPenaltyType->name }}
										</option>
									@endif
								@endif

							@elseif(data_get($value, 'seperator') == 'player_penalties_match_player_id')
								
								@if(!empty($model_data))
									@php
										$__playerPenaltyMP = App\PlayerPenalty::find($model_data->id)->player;
							        @endphp
							        
							        @if(!is_null($__playerPenaltyMP))
										<option value="{{ $__playerPenaltyMP->id }}" selected="selected">
											@if(!empty($__playerPenaltyMP->team))

												{{ '('.$__playerPenaltyMP->team->name.') ' }}
											@endif
											{{ $__playerPenaltyMP->user->first_name .' '. $__playerPenaltyMP->user->last_name }}
										</option>
									@endif
								@endif

							@elseif(data_get($value, 'seperator') == 'team_penalties_season_id')
								
								@if(!empty($model_data))
									@php
										$__season = App\Season::find($model_data->season_id);
							        @endphp
							        
							        @if(!is_null($__season))
										<option value="{{ $__season->id }}" selected="selected">
											{{ $__season->league->name.' '.$__season->year.' '.$__season->name }}
										</option>
									@endif
								@endif

							@elseif(data_get($value, 'seperator') == 'team_penalties_match_id')
								
								@if(!empty($model_data))
									@php
										$__match = App\Match::find($model_data->match_id);
							        @endphp
							        
							        @if(!is_null($__match))
										<option value="{{ $__match->id }}" selected="selected">
											{{ $__match->team1->name.' - '.$__match->team2->name }}
										</option>
									@endif
								@endif

							@elseif(data_get($value, 'seperator') == 'tpenalties_type_id')
								
								@if(!empty($model_data))
									@php
										$__teamPenaltyType = App\TeamPenalty::find($model_data->id)->penalty_type;
							        @endphp
							        
							        @if(!is_null($__teamPenaltyType))
										<option value="{{ $__teamPenaltyType->id }}" selected="selected">
											{{ $__teamPenaltyType->name }}
										</option>
									@endif
								@endif

							@elseif(data_get($value, 'seperator') == 'team_penalties_match_team_id')
								
								@if(!empty($model_data))
									@php
										$__teamPenaltyMP = App\TeamPenalty::find($model_data->id)->team;
							        @endphp
							        
							        @if(!is_null($__teamPenaltyMP))
										<option value="{{ $__teamPenaltyMP->id }}" selected="selected">
											{{ $__teamPenaltyMP->name }}
										</option>
									@endif
								@endif

							@endif

						@endif

					@else

						@php
								if(empty($formConfig[data_get($value, 'trigger')]['relation_col'])){
									$dataSource = data_get($value, 'dataSource')::where(data_get($value, 'trigger'), $model_data[data_get($value, 'trigger')])->get();
								}else{
									$dataSource = data_get($value, 'dataSource')::where($formConfig[data_get($value, 'trigger')]['relation_col'], $model_data[$formConfig[data_get($value, 'trigger')]['relationship']][$formConfig[data_get($value, 'trigger')]['relation_col']])->get();
							}
						@endphp

						@foreach ($dataSource as $valuee)

							<option value="{{ $valuee->id }}"
								
								@if(empty(data_get($value, 'multiple')))
								
									@if(!empty($model_data))
										@if(!empty(data_get($value, 'relationship')))
											@if( $model_data[data_get($value, 'relationship')][data_get($value, 'relation_col')] ==  $valuee->id )
												selected="selected"
											@endif
										@else
											@if($model_data[$key] == $valuee->id)
												selected="selected"
											@endif
										@endif
									@endif
										
								@else
									
									@foreach ($model_data[data_get($value, 'relationship')] as $selectedVal)
										@if( $selectedVal->district_id == $valuee->id )
											selected="selected"
										@endif
									@endforeach

								@endif

							>

							{{ $valuee->name }}
							
							</option>
						@endforeach
						

					@endif

				</select>

			@endif


		</div>
	</div>

@endif