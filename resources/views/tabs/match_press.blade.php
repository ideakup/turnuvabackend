@php
	//dump($model_data[data_get($value_tabs, 'relationship')]);
    //dump($model_data[data_get($value_tabs, 'relationship')]);
    $value_rel = $model_data[data_get($value_tabs, 'relationship')];
@endphp
<!--begin: Datatable-->
<table class="table table-separate table-head-custom table-hover tab-table" id="kt_datatable_match_press">
    <thead>
        <tr>
            <th>ID</th>
            <th>{{ __('words.team') }}</th>
            <th>{{ __('words.player') }}</th>
            <th>{{ __('words.video_source') }}</th>
            <th>{{ __('words.video_embed_code') }}</th>
            <th>{{ __('words.action') }}</th>
        </tr>
    </thead>
    <tbody>
        
   		@if(!empty($model_data[data_get($value_tabs, 'relationship')]))

   			
                <tr role="row">
                    <td>{{ $value_rel->id }}</td>  <!-- ID -->
                    <td>@if(!empty($value_rel->team)) {{ $value_rel->team->name }} @endif</td>
                    <td>
                        @foreach($value_rel->press_conferences_players as $pcp)
                            ({{ $pcp->match_player->player->team->name }}) {{ $pcp->match_player->player->user->first_name }} {{ $pcp->match_player->player->user->last_name }} <br>
                        @endforeach
                    </td>
                    <td>{{ $value_rel->embed_source }}</td>
                    <td>{{ $value_rel->embed_code }}</td>
                    <td style="width: 110px;">
                        @if(Request::segment(2) != 'delete')
                            <a href="#" id="{{$loop->index}}" class="btn btn-sm btn-clean btn-icon btn-hover-success match_press_edit_button" title="{{ __('words.edit_press') }}" role="button" data-toggle="tooltip" data-html="true" data-content="">
                                <i class="fas fa-edit"></i>
                            </a>
                            <a href="#" class="btn btn-sm btn-clean btn-icon btn-hover-danger" role="button" data-toggle="popvstt" data-html="true" 

                            title="{{ __('words.delete_press') }}" 

                            data-content="
                                <p>
                                    
                                </p>
                                <p>
                                    Bu aksiyonu kaldırmak istiyor musunuz?
                                </p>
                                <a href='{{ url('match_press/delete/'.$model_data->id.'/'.$value_rel->id) }}' class='btn btn-sm btn-light-success font-weight-bold mr-2' title='{{ __('words.approve') }}'>{{ __('words.approve') }}</a>
                                <a href='#' class='btn btn-sm btn-light-danger font-weight-bold mr-2' title='{{ __('words.cancel') }}'>{{ __('words.cancel') }}</a>
                            ">
                                <i class="fas fa-times"></i>
                            </a>
                        @endif
                    </td>
                </tr>
            

        @endif
        
    </tbody>
</table>
<!--end: Datatable-->
