<!--begin: Datatable-->
<table class="table table-separate table-head-custom table-hover tab-table" id="kt_datatable_match_players">
    <thead>
        <tr>
            <th>ID</th>
            <th>{{ __('words.player') }}</th>
            <th>{{ __('words.guest_name') }}</th>
            <th>{{ __('words.position') }}</th>
            <!--<th>{{ __('words.rating') }}</th>-->
            <th>{{ __('words.saving') }}</th>
            <th>{{ __('words.action') }}</th>
        </tr>
    </thead>
    <tbody>
        
    	@if(!empty(data_get($value_tabs, 'relationship')))

            @foreach ($model_data[data_get($value_tabs, 'relationship')] as $key_rel => $value_rel)

                <tr role="row">
                    <td>{{ $value_rel->id }}</td>
                    <td>
                    	@if(is_null($value_rel->player_id))
                    		@if($value_rel->team_id == $value_rel->match->team1_id)
                    			({{ $value_rel->match->team1->name }})
							@elseif($value_rel->team_id == $value_rel->match->team2_id)
								({{ $value_rel->match->team2->name }})
							@endif
                    	@else
                            @if(!empty($value_rel->player->team))
                    		  ({{ $value_rel->player->team->name }}) {{ $value_rel->player->user->first_name.' '.$value_rel->player->user->last_name }}
                            @endif
                    	@endif
	                </td>
                    <td>{{ $value_rel->guest_name }}</td>
                    <td>{{ $value_rel->player_position->name }}</td>
                    <!--<td>{{ $value_rel->rating }}</td>-->
                    <td>{{ $value_rel->saving }}</td>
                    <td style="width: 110px;">
                        @if(!$model_data->completed && Request::segment(2) != 'delete')
                            <a href="#" id="{{$loop->index}}" class="btn btn-sm btn-clean btn-icon btn-hover-success match_player_edit_button" title="{{ __('words.edit_player') }}" role="button" data-toggle="tooltip" data-html="true" data-content="">
                                <i class="fas fa-edit"></i>
                            </a>
                            <a href="#" class="btn btn-sm btn-clean btn-icon btn-hover-danger" role="button" data-toggle="popvstt" data-html="true" 

                            title="{{ __('words.delete_player') }}"
                             
                            data-content="
                                <p>
                                    @if(is_null($value_rel->player_id))
                                        @if($value_rel->team_id == $value_rel->match->team1_id)
                                            ({{ $value_rel->match->team1->name }})
                                        @elseif($value_rel->team_id == $value_rel->match->team2_id)
                                            ({{ $value_rel->match->team2->name }})
                                        @endif
                                    @else
                                        @if(!empty($value_rel->player->team)) ({{ $value_rel->player->team->name }}) @else '' @endif {{ $value_rel->player->user->first_name.' '.$value_rel->player->user->last_name }}
                                    @endif
                                </p>
                                <p>
                                    {{ __('words.do_you_want_to_remove_this_player_from_the_match_roster') }}
                                </p>
                                <a href='{{ url('match_player/delete/'.$model_data->id.'/'.$value_rel->id) }}' class='btn btn-sm btn-light-success font-weight-bold mr-2' title='{{ __('words.approve') }}'>{{ __('words.approve') }}</a>
                                <a href='#' class='btn btn-sm btn-light-danger font-weight-bold mr-2' title='{{ __('words.cancel') }}'>{{ __('words.cancel') }}</a>
                            ">
                                <i class="fas fa-times"></i>
                            </a>
                        @endif
                    </td>
                </tr>
            @endforeach

        @endif
        
    </tbody>
</table>
<!--end: Datatable-->
