@php
    $activeSeasonId = array();
@endphp

@foreach ($model_data->team_teamseason as $valid)
    @if($valid->season->active == true && $valid->season->start_date <= Carbon\Carbon::now()->format('Y-m-d') && $valid->season->end_date >= Carbon\Carbon::now()->format('Y-m-d') && $valid->season->polymorphic_ctype_id != 32)
        @php
            $activeSeasonId[] = $valid->season->id;
        @endphp
    @endif
@endforeach

<!--begin: Datatable-->
<table class="table table-separate table-head-custom table-hover tab-table" id="kt_datatable_teams_seasons">
    <thead>
        <tr>
            <th>ID</th>
            <th>{{ __('words.league') }}</th>
            <th>{{ __('words.year') }}</th>
            <th>{{ __('words.season') }}</th>
            <th>{{ __('words.squad_lock_date') }}</th>
            <th>{{ __('words.total_matches') }}</th>
            <th>{{ __('words.point') }}</th>
            <th>{{ __('words.action') }}</th>
        </tr>
    </thead>
    <tbody>

        @if(!empty(data_get($value_tabs, 'relationship')))

            @foreach ($model_data[data_get($value_tabs, 'relationship')] as $key_rel => $value_rel)

                <tr role="row">
                    <td>{{ $value_rel->season->id }}</td>
                    <td>{{ $value_rel->season->league->name }}</td>
                    <td>{{ $value_rel->season->year }}</td>
                    <td>{{ $value_rel->season->name }}</td>
                    <td>{{ (!empty($value_rel->squad_locked_date)) ? Carbon\Carbon::parse($value_rel->squad_locked_date)->format('d.m.Y') : '-' }}</td>
                    <td>{{ $value_rel->match_total }}</td>
                    <td>{{ $value_rel->point }}</td>
                    <td style="width: 110px;">
                        @if(in_array($value_rel->season->id, $activeSeasonId) && Auth::user()->hasPermissionTo('change_teamseason'))

                            <a href="javascript:void(0);" 
                                @if($value_rel->playoff_member)
                                    class="btn btn-sm btn-success btn-icon" title="{{ __('words.team_playoff_member') }}"
                                @else
                                    class="btn btn-sm btn-clean btn-icon" title="{{ __('words.not_a_team_playoff_member') }}"
                                @endif

                                role="button" data-toggle="popvstt" data-html="true" 
                                data-content="
                                    <p>
                                        {{ __('words.do_you_want_to_change_playoff_membership_for_this_season') }}
                                    </p>
                                    <a href='{{ url('teams/setPlayOff/'.$model_data->id.'/'.$value_rel->id) }}' class='btn btn-sm btn-light-success font-weight-bold mr-2' title='{{ __('words.approve') }}'>{{ __('words.approve') }}</a>
                                    <a href='javascript:void(0);' class='btn btn-sm btn-light-danger font-weight-bold mr-2' title='{{ __('words.cancel') }}'>{{ __('words.cancel') }}</a>
                                "
                            >
                                <i class="fas fa-trophy"></i>
                            </a>

                            <a href="javascript:void(0);" 
                                @if(!$value_rel->squad_locked)
                                    class="btn btn-sm btn-success btn-icon" title="{{ __('words.squad_locked_status_unlocked') }}"
                                @else
                                    class="btn btn-sm btn-clean btn-icon" title="{{ __('words.squad_lock_status_locked') }}"
                                @endif

                                role="button" data-toggle="popvstt" data-html="true" 
                                data-content="
                                    <p>
                                        {{ __('words.do_you_want_to_change_the_squad_lock_status_for_this_season') }}
                                    </p>
                                    <a href='{{ url('teams/setSquadLocked/'.$model_data->id.'/'.$value_rel->id) }}' class='btn btn-sm btn-light-success font-weight-bold mr-2' title='{{ __('words.approve') }}'>{{ __('words.approve') }}</a>
                                    <a href='javascript:void(0);' class='btn btn-sm btn-light-danger font-weight-bold mr-2' title='{{ __('words.cancel') }}'>{{ __('words.cancel') }}</a>
                                "
                            >   
                                @if(!$value_rel->squad_locked)
                                    <i class="fas fa-unlock"></i>
                                @else
                                    <i class="fas fa-lock"></i>
                                @endif
                                
                            </a>


                            <button type="button" class="btn btn-sm btn-primary btn-icon" title="{{ __('words.add_points') }}" data-toggle="modal" data-target="#addpointModel{{ $value_rel->id }}">
                                <i class="fas fa-arrow-up"></i>
                            </button>

                            <div class="modal fade" id="addpointModel{{ $value_rel->id }}" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">{{ __('words.add_points') }}</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <i aria-hidden="true" class="ki ki-close"></i>
                                            </button>
                                        </div>
                                        <form class='form' method='POST' action='{{ url('teams/addpoint') }}'>
                                            {{ csrf_field() }}
                                            <input type='hidden' name='team_id' value='{{ $model_data->id }}'>
                                            <input type='hidden' name='tseason_id' value='{{ $value_rel->id }}'>

                                            <div class="modal-body">
                                                <div class="form-group row">
                                                    <label class="col-2 col-form-label">{{ __('words.point') }}</label>
                                                    <div class="col-10">
                                                        <input class="form-control" name="point" type="number" step="1" value="0">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-success mr-2"> {{ __('words.save') }} </button>
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('words.cancel') }}</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                        @endif

                    </td>
                </tr>

            @endforeach

        @endif
        
    </tbody>
</table>
<!--end: Datatable-->
