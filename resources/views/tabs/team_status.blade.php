<div class="form-group mb-8">
    <div class="alert alert-custom 
        @if($model_data->status == 'active')
            alert-outline-success
        @elseif($model_data->status == 'passive')
            alert-outline-danger
        @elseif($model_data->status == 'termination')
            alert-outline-warning
        @endif
        " role="alert">
        
        <div class="alert-icon">
            <i class="flaticon-warning"></i>
        </div>
        <div class="alert-text">
            <div class="row">
                <div class="col-form-label col-lg-12 col-sm-12 font-weight-bold">{{ __('words.termination_status') }} : 
                    @if($model_data->status == 'active')
                        {{ __('words.active') }}
                    @elseif($model_data->status == 'passive')
                        Feshedildi
                    @elseif($model_data->status == 'termination')
                        Fesih İsteği
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@if($model_data->status != 'passive')
    <div class="alert mb-5 p-5" role="alert">
        <h4 class="alert-heading">{{ __('words.termination_process') }}</h4>
        <ul>
            <li>{{ __('words.the_captain_requests_the_team_termination_and_waits_for_the_termination_to_be_approved_by_the_manager') }}</li>
            <li>{{ __('words.the_team_that_enters_the_termination_process_continues_all_its_operations_like_a_normal_team_until_the_transaction_is_approved') }}</li>
            <li>{{ __('words.after_the_termination_process_is_confirmed') }}</li>
            <li>{{ __('words.the_team_that_has_never_played_a_match_is_completely_removed_from_the_system') }}</li>
            <li>{{ __('words.the_team_with_the_previously_played_match_becomes_inactive_and_the_statistics_information_is_kept') }}</li>
            <li>{{ __('words.all_matches_to_be_played_will_be_cancelled') }}</li>
            <li>{{ __('words.all_transfer_offers_are_cancelled') }}</li>
            <li>{{ __('words.the_released_players_and_the_captain_can_form_a_new_team') }}</li>
        </ul>
    </div>

    <form class="form" method="POST" action="{{ url('teams/request_termination') }}" id="terminationForm">
        
        {{ csrf_field() }}
        <input type="hidden" name="operation" id="operation">
        <input type="hidden" name="team_id" id="team_id" value="{{ Request::segment(3) }}">

        @if($model_data->status == 'active')
            <button type="button" class="btn btn-danger mr-2" id="terminationBtnPassive" onclick="terminationPassive()">
                {{ __('words.termination_team') }}
            </button>
        @elseif($model_data->status == 'termination')
            <button type="button" class="btn btn-secondary mr-2" id="terminationBtnCancel" onclick="terminationCancel()">
                {{ __('words.cancel') }}
            </button>
            <button type="button" class="btn btn-danger mr-2" id="terminationBtnAccept" onclick="terminationAccept()">
                {{ __('words.confirm_termination') }}
            </button>
        @endif

        </form>
@endif


