<div class="form-group row">
	<label class="col-form-label col-lg-3 col-sm-12 text-lg-right"> {{ __('words.team_logo') }} </label>
	<div class="col-lg-6 col-sm-12">
		<div class="card card-custom overlay" style="width: 200px; margin: auto;">
		    <div class="card-body p-0">
		        <div class="overlay-wrapper" id="imgTeamLogo">
		          <img src="{{ env('MEDIA_END').$model_data->image }}" alt="" class="rounded" style="width: 200px; height: auto;" />
		        </div>
		        <div class="overlay-layer align-items-start justify-content-end pt-5 pr-5">
		            <a href="{{ url('teams/logo_delete/'.$model_data->id) }}" class="btn btn-clean btn-icon" title="{{ __('words.delete_photo') }}" ><i class="fas fa-trash-alt"></i></a>
		        </div>
		    </div>
		</div>
	</div>
	<div class="col-lg-3 col-sm-12">
		<div class="dropzone dropzone-default dropzone-primary" id="kt_dropzone_teamlogo">
			<div class="dropzone-msg dz-message needsclick">
				<h3 class="dropzone-msg-title">{{ __('words.drag_and_drop_the_team_logo_here_or_click_to_upload') }}</h3>
				<span class="dropzone-msg-desc">{{ __('words.you_can_only_upload_1_file') }}</span>
			</div>
		</div>
	</div>
</div>

<div class="form-group row">
	<label class="col-form-label col-lg-3 col-sm-12 text-lg-right" style="vertical-align: "> {{ __('words.team_profile_photo') }} </label>
	<div class="col-lg-6 col-sm-12">
		<div class="card card-custom overlay">
		    <div class="card-body p-0">
		        <div class="overlay-wrapper" id="imgTeamCover">
		          <img src="{{ env('MEDIA_END').$model_data->cover_image}}" alt="" class="w-100 rounded" />
		        </div>
		        <div class="overlay-layer align-items-start justify-content-end pt-5 pr-5">
		            <a href="{{ url('teams/cover_delete/'.$model_data->id) }}" class="btn btn-clean btn-icon" title="{{ __('words.delete_photo') }}" ><i class="fas fa-trash-alt"></i></a>
		        </div>
		    </div>
		</div>
	</div>
	<div class="col-lg-3 col-sm-12">
		<div class="dropzone dropzone-default dropzone-primary" id="kt_dropzone_teamcover">
			<div class="dropzone-msg dz-message needsclick">
				<h3 class="dropzone-msg-title">{{ __('words.drag_and_drop_team_profile_photo_here_or_click_to_upload') }}</h3>
				<span class="dropzone-msg-desc">{{ __('words.you_can_only_upload_1_file') }}</span>
			</div>
		</div>
	</div>
</div>