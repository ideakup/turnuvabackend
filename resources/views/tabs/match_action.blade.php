@php
   // dump($model_data->completed);
@endphp
<!--begin: Datatable-->
<table class="table table-separate table-head-custom table-hover tab-table" id="kt_datatable_match_actions">
    <thead>
        <tr>
            <th>ID</th>
            <th>{{ __('words.video_source') }}</th>
            <th>{{ __('words.video_embed_code') }}</th>
            <th>{{ __('words.team') }}</th>
            <th>{{ __('words.player') }}</th>
            <th>{{ __('words.action_type') }}</th>
            <th>{{ __('words.minute') }}</th>
            <th>{{ __('words.action') }}</th>
        </tr>
    </thead>
    <tbody>
        
   		@if(!empty(data_get($value_tabs, 'relationship')))

            @foreach ($model_data[data_get($value_tabs, 'relationship')] as $key_rel => $value_rel)
                <tr role="row">
                    <td>{{ $value_rel->id }}</td>  <!-- ID -->
                    <td>{{ $value_rel->embed_source }}</td>  <!-- {{ __('words.video_source') }} -->
                    <td>{{ $value_rel->embed_code }}</td>  <!-- {{ __('words.video_embed_code') }} -->
                    <td>{{ $value_rel->match_team->name }}</td>  <!-- Team -->
                    <td>
                    	@if(!empty($value_rel->match_player_id))
                    		@if(empty($value_rel->match_player->player_id))
                    			{{ $value_rel->match_player->guest_name }}
                    		@else
                    			{{ $value_rel->match_player->player->user->first_name.' '.$value_rel->match_player->player->user->last_name }}
                    		@endif
                    	@endif
                    </td>  <!-- Oyuncu -->
                    <td>{{ $value_rel->type }}</td>  <!-- {{ __('words.action_type') }} -->
                    <td>{{ $value_rel->minute }}</td>  <!-- Dakika -->
                    <td style="width: 110px;">
                        @if(!$model_data->completed && Request::segment(2) != 'delete')
                            <a href="#" id="{{$loop->index}}" class="btn btn-sm btn-clean btn-icon btn-hover-success match_action_edit_button" title="{{ __('words.edit_action') }}" role="button" data-toggle="tooltip" data-html="true" data-content="">
                                <i class="fas fa-edit"></i>
                            </a>
                            <a href="#" class="btn btn-sm btn-clean btn-icon btn-hover-danger" role="button" data-toggle="popvstt" data-html="true" 

                            title="{{ __('words.delete_action') }}" 

                            data-content="
                                <p>
                                    ({{ $value_rel->match_team->name }})
                                    @if(!empty($value_rel->match_player_id))
                                        @if(empty($value_rel->match_player->player_id))
                                            {{ $value_rel->match_player->guest_name }}
                                        @else
                                            {{ $value_rel->match_player->player->user->first_name.' '.$value_rel->match_player->player->user->last_name }}
                                        @endif
                                    @endif
                                </p>
                                <p>
                                    Bu aksiyonu kaldırmak istiyor musunuz?
                                </p>
                                <a href='{{ url('match_action/delete/'.$model_data->id.'/'.$value_rel->id) }}' class='btn btn-sm btn-light-success font-weight-bold mr-2' title='{{ __('words.approve') }}'>{{ __('words.approve') }}</a>
                                <a href='#' class='btn btn-sm btn-light-danger font-weight-bold mr-2' title='{{ __('words.cancel') }}'>{{ __('words.cancel') }}</a>
                            ">
                                <i class="fas fa-times"></i>
                            </a>
                        @endif
                    </td>
                </tr>
            @endforeach

        @endif
        
    </tbody>
</table>
<!--end: Datatable-->
