"use strict";
var KTDatatablesDataSourceAjaxServer = function() {

	var initTableUsers = function() {
		
		var table = $('#kt_datatable_users').DataTable({

			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,

			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[0, 'desc']],
            language: {
                "lengthMenu": wordsjs["lengthMenu"],
                "info": wordsjs["info"],
                "infoEmpty": wordsjs["infoEmpty"],
                "zeroRecords": wordsjs["zeroRecords"],
                "infoFiltered": wordsjs["infoFiltered"],
                "processing": wordsjs["processing"],
                "search": wordsjs["search"],
            },

			ajax: {
				url: HOST_URL + '/getUsers',
				type: 'POST',
				headers: { 'X-CSRF-TOKEN': $('#token').val() },
				data: function(data){
					data.columnsDef = ['id', 'identity', 'first_name', 'email', 'province_district', 'user_detail', 'phone',  'date_joined', 'actions'];
					data.searchRole = $('#select2_staff_search').val();
					data.searchUserType = $('#select2_user_type_search').val();
					data.searchTeam = $('#select2_team_search').val();
					data.searchActive = $('#select2_active_search').val();
					data.searchInfoShare = $('#select2_infoshare_search').val();
			    },
			},
			columns: [
				{name: 'id'},
				{name: 'identity'},
				{name: 'first_name'},
				{name: 'email'},
				{name: 'province_district'},
				{name: 'user_detail'},
				{name: 'phone'},
				{name: 'date_joined'},
				{name: 'actions', responsivePriority: -1},
			],
			columnDefs: [
				{targets: 0, title: 'ID', visible: false},
				{targets: 1, title: 'TC', visible: false},
				{
					targets: 2, 
					title: wordsjs['name_surname'],
					/*
					render: function(data, type, full, meta) {
						//console.log(data);
						var sdata = full[8].split("::");

						var fullname = '';
						if(sdata[1] == 1){
							fullname = data;
						}else{
							fullname = '<del>'+data+'</del>';
						}

						return fullname;
					},*/
				},
				{targets: 3, title: wordsjs['email']},
				{targets: 4, title: wordsjs['location']},
				{
					targets: 5, 
					title: wordsjs['status'], 
					orderable: false,

					render: function(data, type, full, meta) {

						var returnValue = '';
						var sdata = data.split("::");
						//console.log(sdata);
						var user_type = {
							'coordinator': {'state': 'fa-user-tie', 'color': 'text-warning'},
							'referee': {'state': 'fa-child', 'color': 'text-warning'},
							'player': {'state': 'fa-users', 'color': 'text-success'},
							'only_user': {'state': 'fa-user', 'color': ''},
						};

						if(sdata[0] == 'coordinator'){
							returnValue += '<i data-trigger="hover" data-toggle="tooltip" title="' + wordsjs['coordinator'] + '" class="fas ' + user_type[sdata[0]].state + ' ' + user_type[sdata[0]].color + '"></i> ';
						}

						if(sdata[1] == 'referee'){
							returnValue += '<i data-trigger="hover" data-toggle="tooltip" title="' + wordsjs['referee'] + '" class="fas ' + user_type[sdata[1]].state + ' ' + user_type[sdata[1]].color + '"></i> ';
						}

						if(sdata[2].split(":")[0] == 'player'){
							returnValue += '<i data-trigger="hover" data-toggle="tooltip" title="' + wordsjs['player'] + '" class="fas ' + user_type[sdata[2].split(":")[0]].state + ' ' + user_type[sdata[2].split(":")[0]].color + '"></i> '+sdata[2].split(":")[1];
						}

						if(sdata[3] == 'only_user'){
							returnValue += '<i data-trigger="hover" data-toggle="tooltip" title="' + wordsjs['user'] + '" class="fas ' + user_type[sdata[3]].state + ' ' + user_type[sdata[3]].color + '"></i> ';
						}

						return returnValue;
					},
				},
				{targets: 6, title: wordsjs['phone']},
				{targets: 7, title: wordsjs['join_date']},
				{
					targets: -1,
					title: wordsjs['action'],
					orderable: false,
					render: function(data, type, full, meta) {
						
						var sdata = full[8].split("::");

						var deleteBtn = '';
						if(sdata[1] == 1){
							deleteBtn = '\
							<a href="users/delete/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_delete_user" title="' + wordsjs['delete'] + '">\
								<i class="fas fa-trash-alt"></i>\
							</a>\
							';
						}

						return '\
						<a href="users/view/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_view_user" title="' + wordsjs['view'] + '">\
							<i class="fas fa-eye"></i>\
						</a>\
						<a href="users/edit/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_change_user" title="' + wordsjs['edit'] + '">\
							<i class="fas fa-edit"></i>\
						</a>\
						';
						/*
						'<a href="users/password/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_password_user" title="' + wordsjs['change_password'] + '">\
							<i class="fas fa-key"></i>\
						</a>'+deleteBtn;
						*/
					},
				},
			],
			drawCallback: function (settings) {
		      	$('[data-toggle="tooltip"]').tooltip();
		      	viButUser();
		    }

		});


		$('#kt_search').on('click', function(e) {
			e.preventDefault();
			
			var params = {};
			$('.datatable-input').each(function() {
				//console.log($(this));
				var i = $(this).data('col-index');
				if (params[i]) {
					params[i] += '|' + $(this).val();
				}
				else {
					params[i] = $(this).val();
				}
			});
			$.each(params, function(i, val) {
				// apply search params to datatable
				table.column(i).search(val ? val : '', false, false);
			});

			table.table().draw();
		});

		$('#kt_reset').on('click', function(e) {
			e.preventDefault();
			$('.datatable-input').each(function() {
				if($(this).prop("tagName") == 'INPUT'){
					$(this).val('');
				}else if($(this).prop("tagName") == 'SELECT'){
					$(this).val(null).change();
				}
				
				table.column($(this).data('col-index')).search('', false, false);
			});

			table.table().draw();
		});
	};

	var initTablePlayerList = function() {
		
		var table = $('#kt_datatable_player_list').DataTable({

			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,

			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[0, 'desc']],
            language: {
                "lengthMenu": wordsjs["lengthMenu"],
                "info": wordsjs["info"],
                "infoEmpty": wordsjs["infoEmpty"],
                "zeroRecords": wordsjs["zeroRecords"],
                "infoFiltered": wordsjs["infoFiltered"],
                "processing": wordsjs["processing"],
                "search": wordsjs["search"],
            },

			ajax: {
				url: HOST_URL + '/getPlayers',
				type: 'POST',
				headers: { 'X-CSRF-TOKEN': $('#token').val() },
				data: function(data){
					data.columnsDef = ['id', 'first_name', 'email', 'province_district', 'user_detail', 'phone',  'date_joined', 'icons', 'actions'];
					data.team_id = $('#team_id').val();
					data.searchRole = $('#select2_staff_search').val();
					data.searchUserType = $('#select2_user_type_search').val();
					data.searchTeam = $('#select2_team_search').val();
					data.searchActive = $('#select2_active_search').val();
			    },
			},
			columns: [
				{name: 'id'},
				{name: 'first_name'},
				{name: 'email'},
				{name: 'province_district'},
				{name: 'user_detail'},
				{name: 'phone'},
				{name: 'date_joined'},
				{name: 'icons'},
				{name: 'actions', responsivePriority: -1},
			],
			columnDefs: [
				{targets: 0, title: wordsjs['player']+' ID', visible: false},
				{
					targets: 1, 
					title: wordsjs['name_surname'],
					render: function(data, type, full, meta) {
						console.log(full);
						var sdata = full[7].split("::");
						console.log(sdata);
						var fullname = '';
						if(sdata[1] == 1){
							fullname = data;
						}else{
							fullname = '<del>'+data+'</del>';
						}
						return fullname;
					},
				},
				{targets: 2, title: wordsjs['email']},
				{targets: 3, title: wordsjs['location']},
				{
					targets: 4, 
					title: wordsjs['team'], 
					orderable: false
				},
				{targets: 5, title: wordsjs['phone']},
				{targets: 6, title: wordsjs['join_date']},
				{
					targets: 7,
					visible: false,
					title: '',
					
					render: function(data, type, full, meta) {
						
						var sdata = data.split("::");
						//console.log(sdata);
						//return;

						var staff_status = {
							'2': {'state': 'fa-user-shield', 'color': 'text-warning'},
							'1': {'state': 'fa-user-shield', 'color': 'text-success'},
							'': {'state': 'fa-user-shield', 'color': ''},
						};

						var active_status = {
							'1': {'state': 'fa-check-circle', 'color': 'text-success'},
							'': {'state': 'fa-times-circle', 'color': 'text-danger'},
						};

						var email_confirmed_status = {
							'1': {'state': 'fa-envelope-open', 'color': 'text-success'},
							'': {'state': 'fa-envelope-open', 'color': 'text-danger'},
						};

						var phone_confirmed_status = {
							'1': {'state': 'fa-phone-square', 'color': 'text-success'},
							'': {'state': 'fa-phone-square', 'color': 'text-danger'},
						};
						
						if (typeof staff_status[sdata[0]] === 'undefined') {
							//console.log('bbb undefined');
							return data;
						}

						//console.log(phone_confirmed_status);
						//console.log(sdata[3]);

						return '<i data-trigger="hover" data-toggle="tooltip" title="'+((sdata[0] == '2') ? wordsjs["super_user"] : ((sdata[0] == '1') ? wordsjs["admin"] : wordsjs["not_an_admin"]))+'" class="fas ' + staff_status[sdata[0]].state + ' ' + staff_status[sdata[0]].color + ' "></i> \
						<i data-trigger="hover" data-toggle="tooltip" title="'+((sdata[1] == '1') ? wordsjs["active"] : wordsjs["passive"])+'" class="fas ' + active_status[sdata[1]].state + ' ' + active_status[sdata[1]].color + ' "></i> \
						<i data-trigger="hover" data-toggle="tooltip" title="'+((sdata[2] == '1') ? wordsjs["email_confirmed"] : wordsjs["email_not_confirmed"])+'" class="fas ' + email_confirmed_status[sdata[2]].state + ' ' + email_confirmed_status[sdata[2]].color + ' "></i> \
						<i data-trigger="hover" data-toggle="tooltip" title="'+((sdata[3] == '1') ? wordsjs["phone_approved"] : wordsjs["phone_not_certified"])+'" class="fas ' + phone_confirmed_status[sdata[3]].state + ' ' + phone_confirmed_status[sdata[3]].color + ' "></i>';
					},
				},
				{
					targets: -1,
					title: wordsjs['action'],
					orderable: false,
					render: function(data, type, full, meta) {
						
						var sdata = full[6].split("::");
						return '\
						<a href="/teams/addinsideplayer_save/'+$('#team_id').val()+"/"+full[0]+'" class="btn btn-sm btn-success" title="' + wordsjs['add_player_to_team'] + '">\
							<i class="fas fa-plus"></i> ' + wordsjs['add'] +'\
						</a>';
					},
				},
			],
			drawCallback: function (settings) {
		      	$('[data-toggle="tooltip"]').tooltip();
		      	viButUser();
		    }

		});


		$('#kt_search').on('click', function(e) {
			e.preventDefault();
			
			var params = {};
			$('.datatable-input').each(function() {
				//console.log($(this));
				var i = $(this).data('col-index');
				if (params[i]) {
					params[i] += '|' + $(this).val();
				}
				else {
					params[i] = $(this).val();
				}
			});
			$.each(params, function(i, val) {
				// apply search params to datatable
				table.column(i).search(val ? val : '', false, false);
			});

			table.table().draw();
		});

		$('#kt_reset').on('click', function(e) {
			e.preventDefault();
			$('.datatable-input').each(function() {
				if($(this).prop("tagName") == 'INPUT'){
					$(this).val('');
				}else if($(this).prop("tagName") == 'SELECT'){
					$(this).val(null).change();
				}
				
				table.column($(this).data('col-index')).search('', false, false);
			});

			table.table().draw();
		});
	};

	var initTableRoles = function() {
		
		var table = $('#kt_datatable_roles').DataTable({

			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,

			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[0, 'desc']],
            language: {
                "lengthMenu": wordsjs["lengthMenu"],
                "info": wordsjs["info"],
                "infoEmpty": wordsjs["infoEmpty"],
                "zeroRecords": wordsjs["zeroRecords"],
                "infoFiltered": wordsjs["infoFiltered"],
                "processing": wordsjs["processing"],
                "search": wordsjs["search"],
            },
			ajax: {
				url: HOST_URL + '/getRoles',
				type: 'POST',
				headers: { 'X-CSRF-TOKEN': $('#token').val() },
				data: function(data){
					data.columnsDef = ['id', 'name', 'actions'];
			    },
			},
			columns: [
				{name: 'id'},
				{name: 'name'},
				{name: 'actions', responsivePriority: -1},
			],
			columnDefs: [
				{targets: 0, title: 'ID', visible: false},
				{targets: 1, title: wordsjs["first_name"]},
				{
					targets: -1,
					title: wordsjs['action'],
					orderable: false,
					width: '150px', //btn btn-text-primary btn-hover-light-primary font-weight-bold 
					render: function(data, type, full, meta) {
						return '\
						<a href="roles/view/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_view_permission" title="' + wordsjs['view'] + '">\
							<i class="fas fa-eye"></i>\
						</a>\
						<a href="roles/edit/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_change_permission" title="' + wordsjs['edit'] + '">\
							<i class="fas fa-edit"></i>\
						</a>\
						<a href="roles/delete/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_delete_permission" title="' + wordsjs['delete'] + '">\
							<i class="fas fa-trash-alt"></i>\
						</a>\
						<a href="roles/permission/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_change_permission" title="' + wordsjs['edit_authorization'] + '">\
							<i class="fas fa-book-medical"></i></i>\
						</a>\
						';
					},
				},
			],
			drawCallback: function (settings) {
				viButRole();
		    }

		});
	};

	var initTableConferences = function() {

		// begin first table
		var table = $('#kt_datatable_conferences').DataTable({

			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,

			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[0, 'desc']],
            language: {
                "lengthMenu": wordsjs["lengthMenu"],
                "info": wordsjs["info"],
                "infoEmpty": wordsjs["infoEmpty"],
                "zeroRecords": wordsjs["zeroRecords"],
                "infoFiltered": wordsjs["infoFiltered"],
                "processing": wordsjs["processing"],
                "search": wordsjs["search"],
            },

			ajax: {
				url: HOST_URL + '/getConferences',
				type: 'POST',
				headers: { 'X-CSRF-TOKEN': $('#token').val() },
				data: {
					// parameters for custom backend script demo
					// columnsDef: ['id', 'first_name', 'username', 'email', 'actions'],
					columnsDef: ['id', 'province_id', 'name', 'actions'],
				},
			},
			columns: [
				{name: 'id'},
				{name: 'name'},
				{name: 'province_id'},
				{name: 'actions', responsivePriority: -1},
			],
			columnDefs: [

				{targets: 0, title: 'ID', visible: false},
				{targets: 1, title: wordsjs["name"]},
				{targets: 2, title: wordsjs["city"]},

				{
					targets: -1,
					title: wordsjs['action'],
					orderable: false,
					width: '150px',
					render: function(data, type, full, meta) {
						return '\
						<a href="conferences/view/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_view_conference" title="' + wordsjs['view'] + '">\
							<i class="fas fa-eye"></i>\
						</a>\
						<a href="conferences/edit/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_change_conference" title="' + wordsjs['edit'] + '">\
							<i class="fas fa-edit"></i>\
						</a>\
						<a href="conferences/delete/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_delete_conference" title="' + wordsjs['delete'] + '">\
							<i class="fas fa-trash-alt"></i>\
						</a>\
						';
					},
				},

			],
			drawCallback: function (settings) {
				viButConference();
		    }
		});

		$('#kt_search').on('click', function(e) {
			e.preventDefault();
			
			var params = {};
			$('.datatable-input').each(function() {
				//console.log($(this));
				var i = $(this).data('col-index');
				if (params[i]) {
					params[i] += '|' + $(this).val();
				}
				else {
					params[i] = $(this).val();
				}
			});
			$.each(params, function(i, val) {
				// apply search params to datatable
				table.column(i).search(val ? val : '', false, false);
			});

			table.table().draw();
		});

		$('#kt_reset').on('click', function(e) {
			e.preventDefault();
			$('.datatable-input').each(function() {
				if($(this).prop("tagName") == 'INPUT'){
					$(this).val('');
				}else if($(this).prop("tagName") == 'SELECT'){
					$(this).val(null).change();
				}
				
				table.column($(this).data('col-index')).search('', false, false);
			});

			table.table().draw();
		});
	};

	var initTableProvinces = function() {

		var table = $('#kt_datatable_provinces');

		// begin first table
		table.DataTable({

			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,

			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[0, 'desc']],
            language: {
                "lengthMenu": wordsjs["lengthMenu"],
                "info": wordsjs["info"],
                "infoEmpty": wordsjs["infoEmpty"],
                "zeroRecords": wordsjs["zeroRecords"],
                "infoFiltered": wordsjs["infoFiltered"],
                "processing": wordsjs["processing"],
                "search": wordsjs["search"],
            },

			ajax: {
				url: HOST_URL + '/getProvinces',
				type: 'POST',
				headers: { 'X-CSRF-TOKEN': $('#token').val() },
				data: {
					// parameters for custom backend script demo
					// columnsDef: ['id', 'first_name', 'username', 'email', 'actions'],
					columnsDef: ['id', 'name', 'plate_code', 'actions'],
				},
			},
			columns: [
				{name: 'id'},
				{name: 'name'},
				{name: 'plate_code'},
				{name: 'actions', responsivePriority: -1},
			],
			columnDefs: [

				{targets: 0, title: 'ID', visible: false},
				{targets: 1, title: wordsjs["name"]},
				{targets: 2, title: wordsjs["plate"]},

				{
					targets: -1,
					title: wordsjs['action'],
					orderable: false,
					width: '150px',
					render: function(data, type, full, meta) {
						return '\
						<a href="provinces/view/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_view_province" title="' + wordsjs['view'] + '">\
							<i class="fas fa-eye"></i>\
						</a>\
						<a href="provinces/edit/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_change_province" title="' + wordsjs['edit'] + '">\
							<i class="fas fa-edit"></i>\
						</a>\
						<a href="provinces/delete/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_delete_province" title="' + wordsjs['delete'] + '">\
							<i class="fas fa-trash-alt"></i>\
						</a>\
						';
					},
				},

			],
			drawCallback: function (settings) {
				viButProvince();
		    }
		});
	};

	var initTableDistricts = function() {

		// begin first table
		var table = $('#kt_datatable_districts').DataTable({

			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,

			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[2, 'asc']],
            language: {
                "lengthMenu": wordsjs["lengthMenu"],
                "info": wordsjs["info"],
                "infoEmpty": wordsjs["infoEmpty"],
                "zeroRecords": wordsjs["zeroRecords"],
                "infoFiltered": wordsjs["infoFiltered"],
                "processing": wordsjs["processing"],
                "search": wordsjs["search"],
            },

			ajax: {
				url: HOST_URL + '/getDistricts',
				type: 'POST',
				headers: { 'X-CSRF-TOKEN': $('#token').val() },
				data: {
					columnsDef: ['id', 'name', 'province_id', 'actions'],
				},
			},
			columns: [
				{name: 'id'},
				{name: 'name'},
				{name: 'province_id'},
				{name: 'actions', responsivePriority: -1},
			],
			columnDefs: [

				{targets: 0, title: 'ID', visible: false},
				{targets: 1, title: wordsjs["name"]},
				{targets: 2, title: wordsjs["city"]},

				{
					targets: -1,
					title: wordsjs['action'],
					orderable: false,
					width: '150px',
					render: function(data, type, full, meta) {
						return '\
						<a href="districts/view/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_view_district" title="' + wordsjs['view'] + '">\
							<i class="fas fa-eye"></i>\
						</a>\
						<a href="districts/edit/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_change_district" title="' + wordsjs['edit'] + '">\
							<i class="fas fa-edit"></i>\
						</a>\
						<a href="districts/delete/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_delete_district" title="' + wordsjs['delete'] + '">\
							<i class="fas fa-trash-alt"></i>\
						</a>\
						';
					},
				},

			],
			drawCallback: function (settings) {
				viButDistrict();
		    }
		});

		$('#kt_search').on('click', function(e) {
			e.preventDefault();
			
			var params = {};
			$('.datatable-input').each(function() {
				//console.log($(this));
				var i = $(this).data('col-index');
				if (params[i]) {
					params[i] += '|' + $(this).val();
				}
				else {
					params[i] = $(this).val();
				}
			});
			$.each(params, function(i, val) {
				// apply search params to datatable
				table.column(i).search(val ? val : '', false, false);
			});

			table.table().draw();
		});

		$('#kt_reset').on('click', function(e) {
			e.preventDefault();
			$('.datatable-input').each(function() {
				if($(this).prop("tagName") == 'INPUT'){
					$(this).val('');
				}else if($(this).prop("tagName") == 'SELECT'){
					$(this).val(null).change();
				}
				
				table.column($(this).data('col-index')).search('', false, false);
			});

			table.table().draw();
		});
	};

	var initTableGrounds = function() {

		// begin first table
		var table = $('#kt_datatable_grounds').DataTable({

			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,

			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[1, 'asc']],
            language: {
                "lengthMenu": wordsjs["lengthMenu"],
                "info": wordsjs["info"],
                "infoEmpty": wordsjs["infoEmpty"],
                "zeroRecords": wordsjs["zeroRecords"],
                "infoFiltered": wordsjs["infoFiltered"],
                "processing": wordsjs["processing"],
                "search": wordsjs["search"],
            },

			ajax: {
				url: HOST_URL + '/getGrounds',
				type: 'POST',
				headers: { 'X-CSRF-TOKEN': $('#token').val() },
				data: {
					columnsDef: ['id', 'name', 'actions'],
				},
			},
			columns: [
				{name: 'id'},
				{name: 'name'},
				{name: 'actions', responsivePriority: -1},
			],
			columnDefs: [

				{targets: 0, title: 'ID', visible: false},
				{targets: 1, title: wordsjs["name"]},

				{
					targets: -1,
					title: wordsjs['action'],
					orderable: false,
					width: '150px',
					render: function(data, type, full, meta) {
						return '\
						<a href="grounds/view/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_view_ground" title="' + wordsjs['view'] + '">\
							<i class="fas fa-eye"></i>\
						</a>\
						<a href="grounds/edit/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_change_ground" title="' + wordsjs['edit'] + '">\
							<i class="fas fa-edit"></i>\
						</a>\
						<a href="grounds/delete/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_delete_ground" title="' + wordsjs['delete'] + '">\
							<i class="fas fa-trash-alt"></i>\
						</a>\
						';
					},
				},

			],
			drawCallback: function (settings) {
				viButGround();
		    }
		});

		$('#kt_search').on('click', function(e) {
			e.preventDefault();
			
			var params = {};
			$('.datatable-input').each(function() {
				//console.log($(this));
				var i = $(this).data('col-index');
				if (params[i]) {
					params[i] += '|' + $(this).val();
				}
				else {
					params[i] = $(this).val();
				}
			});
			$.each(params, function(i, val) {
				// apply search params to datatable
				table.column(i).search(val ? val : '', false, false);
			});

			table.table().draw();
		});

		$('#kt_reset').on('click', function(e) {
			e.preventDefault();
			$('.datatable-input').each(function() {
				if($(this).prop("tagName") == 'INPUT'){
					$(this).val('');
				}else if($(this).prop("tagName") == 'SELECT'){
					$(this).val(null).change();
				}
				
				table.column($(this).data('col-index')).search('', false, false);
			});

			table.table().draw();
		});
	};

	var initTableGroundProperties = function() {

		// begin first table
		var table = $('#kt_datatable_ground_properties').DataTable({

			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,

			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[0, 'desc']],
            language: {
                "lengthMenu": wordsjs["lengthMenu"],
                "info": wordsjs["info"],
                "infoEmpty": wordsjs["infoEmpty"],
                "zeroRecords": wordsjs["zeroRecords"],
                "infoFiltered": wordsjs["infoFiltered"],
                "processing": wordsjs["processing"],
                "search": wordsjs["search"],
            },

			ajax: {
				url: HOST_URL + '/getGroundProperties',
				type: 'POST',
				headers: { 'X-CSRF-TOKEN': $('#token').val() },
				data: {
					columnsDef: ['id', 'name', 'actions'],
				},
			},
			columns: [
				{name: 'id'},
				{name: 'name'},
				{name: 'actions', responsivePriority: -1},
			],
			columnDefs: [

				{targets: 0, title: 'ID', visible: false},
				{targets: 1, title: wordsjs["name"]},

				{
					targets: -1,
					title: wordsjs['action'],
					orderable: false,
					width: '150px',
					render: function(data, type, full, meta) {
						return '\
						<a href="ground_properties/view/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_view_ground btn_view_groundproperty" title="' + wordsjs['view'] + '">\
							<i class="fas fa-eye"></i>\
						</a>\
						<a href="ground_properties/edit/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_change_ground btn_change_groundproperty" title="' + wordsjs['edit'] + '">\
							<i class="fas fa-edit"></i>\
						</a>\
						<a href="ground_properties/delete/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_delete_ground btn_delete_groundproperty" title="' + wordsjs['delete'] + '">\
							<i class="fas fa-trash-alt"></i>\
						</a>\
						';
					},
				},

			],
			drawCallback: function (settings) {
				viButGroundproperty();
		    }
		});

		$('#kt_search').on('click', function(e) {
			e.preventDefault();
			
			var params = {};
			$('.datatable-input').each(function() {
				//console.log($(this));
				var i = $(this).data('col-index');
				if (params[i]) {
					params[i] += '|' + $(this).val();
				}
				else {
					params[i] = $(this).val();
				}
			});
			$.each(params, function(i, val) {
				// apply search params to datatable
				table.column(i).search(val ? val : '', false, false);
			});

			table.table().draw();
		});

		$('#kt_reset').on('click', function(e) {
			e.preventDefault();
			$('.datatable-input').each(function() {
				if($(this).prop("tagName") == 'INPUT'){
					$(this).val('');
				}else if($(this).prop("tagName") == 'SELECT'){
					$(this).val(null).change();
				}
				
				table.column($(this).data('col-index')).search('', false, false);
			});

			table.table().draw();
		});
	};

	var initTableGroundHour = function() {

		var gunler = ['Pazartesi', 'Salı', 'Çarşamba', 'Perşembe', 'Cuma', 'Cumartesi', 'Pazar'];
		var checkGen = function(check_si, check_i, datas){
			//console.log(check_si+' - '+check_i+' - '+datas);

			var _gtsi = "";
			var _gtsis = "";
			var _gti = "";

			if(check_si == 'checked'){
				_gti = 'style="display:none;"';
			}else{
				_gtsis = 'style="display:none;"';
			}
			
			return '\
				<div class="checkbox-inline">\
					<label class="checkbox checkbox-outline checkbox-primary checkbox-lg">\
					<input type="checkbox" '+check_si+' class="ground_hour_cb btn_change_groundhour" data-type="ground-table-subscription-item" data-day="'+datas[0]+'" data-time="'+datas[1]+'" data-d="'+datas[2]+'" >\
					<span></span><div class="subsLabel" '+_gtsis+'>Abone</div></label>\
				</div>\
				<span class="switch switch-sm switch-outline switch-primary" '+_gti+'>\
				    <label>\
					    <input type="checkbox" '+check_i+' class="ground_hour_cb btn_change_groundhour" data-type="ground-table-item" data-day="'+datas[0]+'" data-time="'+datas[1]+'" data-d="'+datas[2]+'"/>\
					    <span></span>\
				    </label>\
				</span>';
		}

		var setGroundHourAjaxData = function(__type, __day, __time, __d, __val){
			//console.log(__type+' - '+__day+' - '+__time+' - '+__d+' - '+__val);
			
			$.ajax({
                method: 'POST',
                url : '/setGroundHour',
                dataType: 'json',
                headers: { 'X-CSRF-TOKEN': $('#token').val() },
				data: {
					week_start_date: $('#select2_week_start_date').val(),
					ground_id: $('#ground_id').val(),
					type: __type,
					day: __day,
					time: __time,
					d: __d,
					val: __val
				},
                success: function(data, textStatus, jqXHR)
                {	
                	$(function() {
	                    if(data.status == "success"){
	                    	toastr.options = {"closeButton": true,"positionClass": "toast-bottom-right"}
	                    	toastr.success(data.text)	
	                    }
	                });
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
             
                }
            });
            
        }

		// begin first table
		var table = $('#kt_datatable_ground_hour').DataTable({
			responsive: false,
			searchDelay: 500,
			processing: true,
			serverSide: true,
			paging:   false,
			ordering: false,
			info: false,

			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			lengthMenu: ["All"],
            pageLength: 9999,
            //order: [[0, 'desc']],
            language: {
                "lengthMenu": wordsjs["lengthMenu"],
                "info": wordsjs["info"],
                "infoEmpty": wordsjs["infoEmpty"],
                "zeroRecords": wordsjs["zeroRecords"],
                "infoFiltered": wordsjs["infoFiltered"],
                "processing": wordsjs["processing"],
                "search": wordsjs["search"],
            },
			ajax: {
				url: HOST_URL + '/getGroundHour',
				type: 'POST',
				headers: { 'X-CSRF-TOKEN': $('#token').val() },
				data: function(data){
					data.columnsDef = ['sort_id', 'time', 'day1', 'day2', 'day3', 'day4', 'day5', 'day6', 'day7'];
					data.ground_id = $('#ground_id').val();
					data.select2_week_start_date = $('#select2_week_start_date').val();
			    }
			},
			columns: [
				{name: 'sort_id'},
				{name: 'time'},
				{name: 'day1'},
				{name: 'day2'},
				{name: 'day3'},
				{name: 'day4'},
				{name: 'day5'},
				{name: 'day6'},
				{name: 'day7'},
			],
			columnDefs: [
				{targets: 0, orderable: false, visible: false},
				{targets: 1, orderable: false, className: "dt-center"},
				{targets: 2, orderable: false, className: "dt-center",
					render: function(data, type, full, meta) {
						//console.log(full);
						var check_si_arr = '';
						var check_i_arr = '';
						var datas = new Array();

						var d = data.split('|');

						if(d[1] == 1){
							check_si_arr = 'checked';
						}

						if(d[2] == 1){
							check_i_arr = 'checked';
						}

						datas[0] = d[0];
						datas[1] = full[1];
						datas[2] = full[2];
						
						if (typeof data === 'undefined') {
							return data;
						}

						return checkGen(check_si_arr, check_i_arr, datas);
					}
				},
				{targets: 3, orderable: false, className: "dt-center",
					render: function(data, type, full, meta) {
						var check_si_arr = '';
						var check_i_arr = '';
						var datas = new Array();

						var d = data.split('|');

						if(d[1] == 1){
							check_si_arr = 'checked';
						}

						if(d[2] == 1){
							check_i_arr = 'checked';
						}

						datas[0] = d[0];
						datas[1] = full[1];
						datas[2] = full[2];
						
						if (typeof data === 'undefined') {
							return data;
						}

						return checkGen(check_si_arr, check_i_arr, datas);
					}
				},
				{targets: 4, orderable: false, className: "dt-center",
					render: function(data, type, full, meta) {
						var check_si_arr = '';
						var check_i_arr = '';
						var datas = new Array();

						var d = data.split('|');

						if(d[1] == 1){
							check_si_arr = 'checked';
						}

						if(d[2] == 1){
							check_i_arr = 'checked';
						}

						datas[0] = d[0];
						datas[1] = full[1];
						datas[2] = full[2];
						
						if (typeof data === 'undefined') {
							return data;
						}

						return checkGen(check_si_arr, check_i_arr, datas);
					}
				},
				{targets: 5, orderable: false, className: "dt-center",
					render: function(data, type, full, meta) {
						var check_si_arr = '';
						var check_i_arr = '';
						var datas = new Array();

						var d = data.split('|');

						if(d[1] == 1){
							check_si_arr = 'checked';
						}

						if(d[2] == 1){
							check_i_arr = 'checked';
						}

						datas[0] = d[0];
						datas[1] = full[1];
						datas[2] = full[2];
						
						if (typeof data === 'undefined') {
							return data;
						}

						return checkGen(check_si_arr, check_i_arr, datas);
					}
				},
				{targets: 6, orderable: false, className: "dt-center",
					render: function(data, type, full, meta) {
						var check_si_arr = '';
						var check_i_arr = '';
						var datas = new Array();

						var d = data.split('|');

						if(d[1] == 1){
							check_si_arr = 'checked';
						}

						if(d[2] == 1){
							check_i_arr = 'checked';
						}

						datas[0] = d[0];
						datas[1] = full[1];
						datas[2] = full[2];
						
						if (typeof data === 'undefined') {
							return data;
						}

						return checkGen(check_si_arr, check_i_arr, datas);
					}
				},
				{targets: 7, orderable: false, className: "dt-center",
					render: function(data, type, full, meta) {
						var check_si_arr = '';
						var check_i_arr = '';
						var datas = new Array();

						var d = data.split('|');

						if(d[1] == 1){
							check_si_arr = 'checked';
						}

						if(d[2] == 1){
							check_i_arr = 'checked';
						}

						datas[0] = d[0];
						datas[1] = full[1];
						datas[2] = full[2];
						
						if (typeof data === 'undefined') {
							return data;
						}

						return checkGen(check_si_arr, check_i_arr, datas);
					}
				},
				{targets: 8, orderable: false, className: "dt-center",
					render: function(data, type, full, meta) {
						var check_si_arr = '';
						var check_i_arr = '';
						var datas = new Array();

						var d = data.split('|');

						if(d[1] == 1){
							check_si_arr = 'checked';
						}

						if(d[2] == 1){
							check_i_arr = 'checked';
						}

						datas[0] = d[0];
						datas[1] = full[1];
						datas[2] = full[2];
						
						if (typeof data === 'undefined') {
							return data;
						}

						return checkGen(check_si_arr, check_i_arr, datas);
					}
				}
			],
			drawCallback: function (settings) {
				viButGroundhour();
		    }
		}).on('xhr.dt', function ( e, settings, json, xhr ) {
	        for (var i = 0; i < json.colName.length; i++) {
	        	$(table.column(i+2).header()).html(json.colName[i]+'</br>'+gunler[i]);
	        }
	    });


		$('#kt_datatable_ground_hour').on( 'change', 'input.ground_hour_cb', function (e) {
			
			e.preventDefault();
			var __type = $( this ).attr('data-type');
			var __day = $( this ).attr('data-day');
			var __time = $( this ).attr('data-time');
			var __d = $( this ).attr('data-d');

			if($(this).attr('data-type') == 'ground-table-subscription-item'){
				if($(this).prop("checked") == true){
					//console.log('check');
					$( "input[data-type='ground-table-subscription-item'][data-day='"+$( this ).attr('data-day')+"'][data-time='"+$( this ).attr('data-time')+"']" ).parent().find('.subsLabel').show();
					$( "input[data-type='ground-table-item'][data-day='"+$( this ).attr('data-day')+"'][data-time='"+$( this ).attr('data-time')+"']" ).prop('checked', false);
					$( "input[data-type='ground-table-item'][data-day='"+$( this ).attr('data-day')+"'][data-time='"+$( this ).attr('data-time')+"']" ).parent().parent().hide();
				}else{
					//console.log('un check');
					$( "input[data-type='ground-table-subscription-item'][data-day='"+$( this ).attr('data-day')+"'][data-time='"+$( this ).attr('data-time')+"']" ).parent().find('.subsLabel').hide();
					$( "input[data-type='ground-table-item'][data-day='"+$( this ).attr('data-day')+"'][data-time='"+$( this ).attr('data-time')+"']" ).parent().parent().show();
				}
			}else if($(this).attr('data-type') == 'ground-table-item'){

			}

			var __val = $( "input[data-type='"+__type+"'][data-day='"+__day+"'][data-time='"+__time+"']" ).prop('checked');
			setGroundHourAjaxData(__type, __day, __time, __d, __val);
			
		});

		$('#kt_search').on('click', function(e) {
			e.preventDefault();
			
			var params = {};
			$('.datatable-input').each(function() {
				//console.log($(this));
				var i = $(this).data('col-index');
				if (params[i]) {
					params[i] += '|' + $(this).val();
				}
				else {
					params[i] = $(this).val();
				}
			});
			$.each(params, function(i, val) {
				// apply search params to datatable
				table.column(i).search(val ? val : '', false, false);
			});

			table.table().draw();
		});

		$('#kt_reset').on('click', function(e) {
			e.preventDefault();
			$('.datatable-input').each(function() {
				if($(this).prop("tagName") == 'INPUT'){
					$(this).val('');
				}else if($(this).prop("tagName") == 'SELECT'){
					$(this).val(null).change();
				}
				
				table.column($(this).data('col-index')).search('', false, false);
			});

			table.table().draw();
		});
	};

	var initTableLeagues = function() {

		var table = $('#kt_datatable_leagues').DataTable({

			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,

			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[0, 'desc']],
            language: {
                "lengthMenu": wordsjs["lengthMenu"],
                "info": wordsjs["info"],
                "infoEmpty": wordsjs["infoEmpty"],
                "zeroRecords": wordsjs["zeroRecords"],
                "infoFiltered": wordsjs["infoFiltered"],
                "processing": wordsjs["processing"],
                "search": wordsjs["search"],
            },

			ajax: {
				url: HOST_URL + '/getLeagues',
				type: 'POST',
				headers: { 'X-CSRF-TOKEN': $('#token').val() },
				data: {
					columnsDef: ['id', 'name', 'active', 'actions'],
				},
			},
			columns: [
				{name: 'id'},
				{name: 'name'},
				{name: 'active'},
				{name: 'actions', responsivePriority: -1},
			],
			columnDefs: [

				{targets: 0, title: 'ID', visible: false},
				{targets: 1, title: wordsjs["name"]},
				{
					targets: 2, 
					title: wordsjs["active"],
					render: function(data, type, full, meta) {

						var active_status = {
							'true': {'state': 'fa-check-circle', 'color': 'text-success'},
							'false': {'state': 'fa-times-circle', 'color': 'text-danger'},
						};
						
						if (typeof data === 'undefined') {
							return data;
						}

						return '<i class="fas ' + active_status[data].state + ' ' + active_status[data].color + ' "></i>';
					},
				},
				{
					targets: -1,
					title: wordsjs['action'],
					orderable: false,
					width: '150px',
					render: function(data, type, full, meta) {
						return '\
						<a href="leagues/view/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_view_league" title="' + wordsjs['view'] + '">\
							<i class="fas fa-eye"></i>\
						</a>\
						<a href="leagues/edit/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_change_league" title="' + wordsjs['edit'] + '">\
							<i class="fas fa-edit"></i>\
						</a>\
						<a href="leagues/delete/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_delete_league" title="' + wordsjs['delete'] + '">\
							<i class="fas fa-trash-alt"></i>\
						</a>\
						';
					},
				},

			],
			drawCallback: function (settings) {
				viButLeague();
		    }
		});

		$('#kt_search').on('click', function(e) {
			e.preventDefault();
			
			var params = {};
			$('.datatable-input').each(function() {
				//console.log($(this));
				var i = $(this).data('col-index');
				if (params[i]) {
					params[i] += '|' + $(this).val();
				}
				else {
					params[i] = $(this).val();
				}
			});
			$.each(params, function(i, val) {
				// apply search params to datatable
				table.column(i).search(val ? val : '', false, false);
			});

			table.table().draw();
		});

		$('#kt_reset').on('click', function(e) {
			e.preventDefault();
			$('.datatable-input').each(function() {
				if($(this).prop("tagName") == 'INPUT'){
					$(this).val('');
				}else if($(this).prop("tagName") == 'SELECT'){
					$(this).val(null).change();
				}
				
				table.column($(this).data('col-index')).search('', false, false);
			});

			table.table().draw();
		});
	};

	var initTableSeasons = function() {

		// begin first table
		var table = $('#kt_datatable_seasons').DataTable({

			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,

			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[0, 'desc']],
            language: {
                "lengthMenu": wordsjs["lengthMenu"],
                "info": wordsjs["info"],
                "infoEmpty": wordsjs["infoEmpty"],
                "zeroRecords": wordsjs["zeroRecords"],
                "infoFiltered": wordsjs["infoFiltered"],
                "processing": wordsjs["processing"],
                "search": wordsjs["search"],
            },

			ajax: {
				url: HOST_URL + '/getSeasons',
				type: 'POST',
				headers: { 'X-CSRF-TOKEN': $('#token').val() },
				data: {
					columnsDef: ['id', 'league', 'name', 'polymorphic_ctype_id', 'year', 'start_date', 'end_date', 'actions'],
				},
			},
			columns: [
				{name: 'id'},
				{name: 'league'},
				{name: 'name'},
				{name: 'polymorphic_ctype_id'},
				{name: 'year'},
				{name: 'start_date'},
				{name: 'end_date'},
				{name: 'actions', responsivePriority: -1},
			],
			columnDefs: [

				{targets: 0, title: 'ID', visible: false},
				{targets: 1, title: wordsjs["league"]},
				{targets: 2, title: wordsjs["name"]},
				{targets: 3, title: wordsjs["type"]},
				{targets: 4, title: wordsjs["year"]},
				{targets: 5, title: wordsjs["starting"]},
				{targets: 6, title: wordsjs["end"]},
				{
					targets: -1,
					title: wordsjs['action'],
					orderable: false,
					width: '150px',
					render: function(data, type, full, meta) {

						var ss = '';
						var etMenuItem = '';

						if(full[3] == 'pointseason'){
							ss = 'p';
						}else if(full[3] == 'fixtureseason'){
							ss = 'f';
							etMenuItem = '\
							<a href="fixture_group/?tree_id='+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_view_fixturegroup" title="' + wordsjs['groups'] + '">\
								<i class="fas fa-layer-group"></i>\
							</a>\
							';
						}else if(full[3] == 'eliminationseason'){
							ss = 'e';
							etMenuItem = '\
							<a href="elimination_tree/?tree_id='+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_view_eliminationtree" title="' + wordsjs['trees_of_elimination'] + '">\
								<i class="fas fa-sitemap"></i>\
							</a>\
							';
						}

						return '\
						<a href="seasons'+ss+'/view/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_view_season btn_view_season_'+ss+'" title="' + wordsjs['view'] + '">\
							<i class="fas fa-eye"></i>\
						</a>\
						<a href="seasons'+ss+'/edit/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_change_season btn_change_season_'+ss+'" title="' + wordsjs['edit'] + '">\
							<i class="fas fa-edit"></i>\
						</a>\
						<a href="seasons'+ss+'/delete/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_delete_season btn_delete_season_'+ss+'" title="' + wordsjs['delete'] + '">\
							<i class="fas fa-trash-alt"></i>\
						</a>'+etMenuItem;
						
					},
				},
			],
			drawCallback: function (settings) {
				viButSeason();
		    }
		});

		$('#kt_search').on('click', function(e) {
			e.preventDefault();
			
			var params = {};
			$('.datatable-input').each(function() {
				//console.log($(this));
				var i = $(this).data('col-index');
				if (params[i]) {
					params[i] += '|' + $(this).val();
				}
				else {
					params[i] = $(this).val();
				}
			});
			$.each(params, function(i, val) {
				// apply search params to datatable
				table.column(i).search(val ? val : '', false, false);
			});

			table.table().draw();
		});

		$('#kt_reset').on('click', function(e) {
			e.preventDefault();
			$('.datatable-input').each(function() {
				if($(this).prop("tagName") == 'INPUT'){
					$(this).val('');
				}else if($(this).prop("tagName") == 'SELECT'){
					$(this).val(null).change();
				}
				
				table.column($(this).data('col-index')).search('', false, false);
			});

			table.table().draw();
		});
	};

	var initTableSeasonsAddBonus = function() {

		// begin first table
		var table = $('#kt_datatable_seasons_addbonus').DataTable({

			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,

			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			paging: false,
			info: false,
            order: [[5, 'desc']],
            language: {
                "lengthMenu": wordsjs["lengthMenu"],
                "info": wordsjs["info"],
                "infoEmpty": wordsjs["infoEmpty"],
                "zeroRecords": wordsjs["zeroRecords"],
                "infoFiltered": wordsjs["infoFiltered"],
                "processing": wordsjs["processing"],
                "search": wordsjs["search"],
            },

			ajax: {
				url: HOST_URL + '/getSeasonsAddBonus',
				type: 'POST',
				headers: { 'X-CSRF-TOKEN': $('#token').val() },
				data: {
					columnsDef: ['id', 'league', 'name', 'year', 'start_date', 'end_date', 'sumadded', 'month'],
				},
			},
			columns: [
				{name: 'id'},
				{name: 'league'},
				{name: 'name'},
				{name: 'year'},
				{name: 'start_date'},
				{name: 'end_date'},
				{name: 'sumadded'},
				{name: 'month'}
			],
			columnDefs: [

				{
					targets: 0, 
					title: '',
					width: '10px',
					orderable: false,
					render: function(data, type, full, meta) {
						console.log(full);
						return '<div class="form-group" style="margin: 0.5rem;">\
								    <div class="checkbox-list">\
								        <label class="checkbox">\
								            <input type="checkbox" name="seasonid[]" value="'+data+'">\
								            <span></span>\
								        </label>\
								    </div>\
								</div>';
					},

				},
				{targets: 1, title: wordsjs["league"]},
				{targets: 2, title: wordsjs["name"]},
				{targets: 3, title: wordsjs["year"]},
				{targets: 4, title: wordsjs["starting"]},
				{targets: 5, title: wordsjs["end"]},
				{targets: 6, title: wordsjs["sum_added"]},
				{targets: 7, title: wordsjs["month"]}

			],
		});
	};

	var initTableTeams = function() {

        function setTeamsAjaxData(_selector, _id, _url){
			//console.log($(_selector).val());
			$.ajax({
                method: 'GET',
                url : _url+'/'+_id+'/'+$(_selector).val(),
                dataType: 'json',
                success: function(data, textStatus, jqXHR)
                {	
                	$(function() {
	                    if(data.status == "success"){
	                    	toastr.options = {"closeButton": true,"positionClass": "toast-bottom-right"}
	                    	toastr.success(data.text)	
	                    }
	                });
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
             
                }
            });
        }

		var select2Optioneee = { placeholder: "Seçiniz", language: "tr", width: 150 };
		// begin first table
		var table = $('#kt_datatable_teams').DataTable({
			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,

			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[0, 'desc']],
            language: {
                "lengthMenu": wordsjs["lengthMenu"],
                "info": wordsjs["info"],
                "infoEmpty": wordsjs["infoEmpty"],
                "zeroRecords": wordsjs["zeroRecords"],
                "infoFiltered": wordsjs["infoFiltered"],
                "processing": wordsjs["processing"],
                "search": wordsjs["search"],
            },
			ajax: {
				url: HOST_URL + '/getTeams',
				type: 'POST',
				headers: { 'X-CSRF-TOKEN': $('#token').val() },
				data: {
					//columnsDef: ['id', 'name', 'province_id', 'district_id', 'captain_id', 'phone', 'created_at', 'potential', 'status', 'ms', 'ks', 'actions'],
					columnsDef: ['id', 'name', 'country_id', 'province_id', 'captain_id', 'created_at', 'ms', 'ks', 'actions'],
				},
			},
			columns: [
				{name: 'id'},
				{name: 'name'},
				{name: 'country_id'},
				{name: 'province_id'},
				{name: 'captain_id'},
				{name: 'created_at'},
				{name: 'ms'},
				{name: 'ks'},
				{name: 'actions', responsivePriority: -1},
			],
			columnDefs: [
				{targets: 0, title: 'ID', visible: false},
				{targets: 1, title: wordsjs["name"]},
				{targets: 2, title: wordsjs["country"]},
				{targets: 3, title: wordsjs["province"]},
				{targets: 4, title: wordsjs["captain"]},
				{targets: 5, title: wordsjs["creation"]},
				{targets: 6, title: wordsjs["num_of_matches"], orderable: false},
				{targets: 7, title: wordsjs["num_of_staff"], orderable: false},
				{
					targets: -1,
					title: wordsjs['action'],
					orderable: false,
					width: '150px',
					render: function(data, type, full, meta) {
						return '\
						<a href="teams/view/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_view_team" title="' + wordsjs['view'] + '">\
							<i class="fas fa-eye"></i>\
						</a>\
						<a href="teams/edit/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_change_team" title="' + wordsjs['edit'] + '">\
							<i class="fas fa-edit"></i>\
						</a>\
						<a href="teams/delete/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_delete_team" title="' + wordsjs['delete'] + '">\
							<i class="fas fa-trash-alt"></i>\
						</a>\
						';
					},
				},
			],
			drawCallback: function (settings) {
				viButTeam();
		      	$('[data-toggle="tooltip"]').tooltip();
		    }
		}).on('draw', function () {
		    $.each(table.column(0).data(), function(i, val) {
				$('#select2_potential_dt_'+val).select2(select2Optioneee).on('change', function (e) {
	            	//console.log('#select2_potential_dt_'+val);
	            	setTeamsAjaxData('#select2_potential_dt_'+val, val, 'setTeamsP');
	            });

				$('#select2_satisfaction_dt_'+val).select2(select2Optioneee).on('change', function (e) {
	            	//console.log('#select2_satisfaction_dt_'+val);
	            	setTeamsAjaxData('#select2_satisfaction_dt_'+val, val, 'setTeamsS');
	            });
			});
		});

		$('#kt_search').on('click', function(e) {
			e.preventDefault();

			$('#potential').val($('#select2_potential_search').val()+'|'+$('#select2_satisfaction_search').val());
			$('#ms').val($('#msmin').val()+'|'+$('#msmax').val());
			$('#ks').val($('#ksmin').val()+'|'+$('#ksmax').val());

			var params = {};
			$('.datatable-input').each(function() {
				//console.log($(this));
				var i = $(this).data('col-index');
				if (params[i]) {
					params[i] += '|' + $(this).val();
				}
				else {
					params[i] = $(this).val();
				}
			});
			$.each(params, function(i, val) {
				// apply search params to datatable
				table.column(i).search(val ? val : '', false, false);
			});

			table.table().draw();
		});

		$('#kt_reset').on('click', function(e) {
			e.preventDefault();
			$('.datatable-input').each(function() {
				if($(this).prop("tagName") == 'INPUT'){
					$(this).val('');
				}else if($(this).prop("tagName") == 'SELECT'){
					$(this).val(null).change();
				}
				
				table.column($(this).data('col-index')).search('', false, false);
			});

			table.table().draw();
		});
	};

	var initTableTeams_Seasons = function() {

		// begin first table
		var table = $('#kt_datatable_teams_seasons').DataTable({
			responsive: true,
			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[0, 'desc']],
            language: {
                "lengthMenu": wordsjs["lengthMenu"],
                "info": wordsjs["info"],
                "infoEmpty": wordsjs["infoEmpty"],
                "zeroRecords": wordsjs["zeroRecords"],
                "infoFiltered": wordsjs["infoFiltered"],
                "processing": wordsjs["processing"],
                "search": wordsjs["search"],
            },
			columnDefs: [
				{targets: 0, visible: false},
				{targets: -1, orderable: false},
			],
			
		});
	};

	var initTableTeams_Players = function() {

		// begin first table
		var table = $('#kt_datatable_teams_players').DataTable({
			responsive: true,
			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[0, 'desc']],
            language: {
                "lengthMenu": wordsjs["lengthMenu"],
                "info": wordsjs["info"],
                "infoEmpty": wordsjs["infoEmpty"],
                "zeroRecords": wordsjs["zeroRecords"],
                "infoFiltered": wordsjs["infoFiltered"],
                "processing": wordsjs["processing"],
                "search": wordsjs["search"],
            },
			columnDefs: [
				{targets: 0, visible: false},
				{targets: -1, orderable: false},
			],
			
		});
	};

	var initTableMatches = function() {

		var select2Optioneee = { placeholder: "Seçiniz", language: "tr", width: 150 };
		// begin first table
		var table = $('#kt_datatable_matches').DataTable({
			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,

			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[0, 'desc']],
            language: {
                "lengthMenu": wordsjs["lengthMenu"],
                "info": wordsjs["info"],
                "infoEmpty": wordsjs["infoEmpty"],
                "zeroRecords": wordsjs["zeroRecords"],
                "infoFiltered": wordsjs["infoFiltered"],
                "processing": wordsjs["processing"],
                "search": wordsjs["search"],
            },
			ajax: {
				url: HOST_URL + '/getMatches',
				type: 'POST',
				headers: { 'X-CSRF-TOKEN': $('#token').val() },
				data: {
					columnsDef: ['id', 'season', 'team1_id', 'team2_id', 'date', 'time', 'videodurumu', 'completed', 'actions']
				}
			},
			columns: [
				{name: 'id'},
				{name: 'season'},
				{name: 'team1_id'},
				{name: 'team2_id'},
				{name: 'date'},
				{name: 'time'},
				{name: 'videodurumu'},
				{name: 'completed'},
				{name: 'actions', responsivePriority: -1}
			],
			columnDefs: [
				{targets: 0, title: 'ID', visible: false},
				{targets: 1, title: wordsjs["season"]},
				{targets: 2, title: wordsjs["first_team"]},
				{targets: 3, title: wordsjs["second_team"]},
				{targets: 4, title: wordsjs["date"]},
				{targets: 5, title: wordsjs["hour"]},
				{targets: 6, title: wordsjs["video_status"], orderable: false},
				{
					targets: 7, 
					title: '',
					render: function(data, type, full, meta) {

						var active_status = {
							'true': {'state': 'fa-check-circle', 'color': 'text-success', 'text': 'Tamamlandı'},
							'false': {'state': 'fa-times-circle', 'color': 'text-danger', 'text': 'Tamamlanmadı'},
						};
						if (typeof data === 'undefined') {
							return data;
						}
						return '<i data-trigger="hover" data-toggle="tooltip" title="' + active_status[data].text + '" class="fas ' + active_status[data].state + ' ' + active_status[data].color + ' "></i>';
					},
				},
				{
					targets: -1,
					title: wordsjs['action'],
					orderable: false,
					width: '150px',
					render: function(data, type, full, meta) {
						return '\
						<a href="matches/view/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_view_match" title="' + wordsjs['view'] + '">\
							<i class="fas fa-eye"></i>\
						</a>\
						<a href="matches/edit/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_change_match" title="' + wordsjs['edit'] + '">\
							<i class="fas fa-edit"></i>\
						</a>\
						<a href="matches/delete/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_delete_match" title="' + wordsjs['delete'] + '">\
							<i class="fas fa-trash-alt"></i>\
						</a>\
						';
					},
				}
			],
			drawCallback: function (settings) {
				viButMatch();
		      	$('[data-toggle="tooltip"]').tooltip();
		    }
		});

		$('#kt_search').on('click', function(e) {
			
			e.preventDefault();
            $('#daterange_dates_search').val($('#daterange_start_search > input').val()+'::'+$('#daterange_end_search > input').val());

			var params = {};
			$('.datatable-input').each(function() {
				//console.log($(this));
				var i = $(this).data('col-index');
				if (params[i]) {
					params[i] += '|' + $(this).val();
				}
				else {
					params[i] = $(this).val();
				}
			});
			$.each(params, function(i, val) {
				// apply search params to datatable
				table.column(i).search(val ? val : '', false, false);
			});

			table.table().draw();
			
		});

		$('#kt_reset').on('click', function(e) {
			
			e.preventDefault();
			$('.datatable-input').each(function() {
				if($(this).prop("tagName") == 'INPUT'){
					$(this).val('');
				}else if($(this).prop("tagName") == 'SELECT'){
					$(this).val(null).change();
				}
				
				table.column($(this).data('col-index')).search('', false, false);
			});

			table.table().draw();
			
		});
	};

	var initTableReservations = function() {

		var select2Optioneee = { placeholder: "Seçiniz", language: "tr", width: 150 };
		// begin first table
		var table = $('#kt_datatable_reservations').DataTable({
			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,

			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[0, 'desc']],
            language: {
                "lengthMenu": wordsjs["lengthMenu"],
                "info": wordsjs["info"],
                "infoEmpty": wordsjs["infoEmpty"],
                "zeroRecords": wordsjs["zeroRecords"],
                "infoFiltered": wordsjs["infoFiltered"],
                "processing": wordsjs["processing"],
                "search": wordsjs["search"],
            },
			ajax: {
				url: HOST_URL + '/getReservations',
				type: 'POST',
				headers: { 'X-CSRF-TOKEN': $('#token').val() },
				data: {
					columnsDef: ['reservation_id', 'province_name', 'ground_name', 'reservation_groundtable_date', 'reservation_groundtable_time', 'team_id', 'reservation_offer_count', 'status', 'actions'],
				},
			},
			columns: [
				{name: 'reservation_id'},
				{name: 'province_name'},
				{name: 'ground_name'},
				{name: 'reservation_groundtable_date'},
				{name: 'reservation_groundtable_time'},
				{name: 'team_id'},
				{name: 'reservation_offer_count'},
				{name: 'status'},
				{name: 'actions', responsivePriority: -1},
			],
			columnDefs: [
				{targets: 0, title: 'ID', visible: false},
				{targets: 1, title: wordsjs["city"]},
				{targets: 2, title: wordsjs["ground"]},
				{targets: 3, title: wordsjs["date"]},
				{targets: 4, title: wordsjs["hour"]},
				{targets: 5, title: 'T.B.Takım'},
				{targets: 6, title: 'Teklif S.'},
				{targets: 7, title: wordsjs["status"]},
				{
					targets: -1,
					title: wordsjs['action'],
					orderable: false,
					width: '150px',
					render: function(data, type, full, meta) {
						return '\
						<a href="reservations/view/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_view_reservation" title="' + wordsjs['view'] + '">\
							<i class="fas fa-eye"></i>\
						</a>\
						<a href="reservations/edit/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_change_reservation" title="' + wordsjs['edit'] + '">\
							<i class="fas fa-edit"></i>\
						</a>\
						<a href="reservations/delete/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_delete_reservation" title="' + wordsjs['delete'] + '">\
							<i class="fas fa-trash-alt"></i>\
						</a>\
						';
					},
				},
			],
			drawCallback: function (settings) {
				viButReservation();
		      	$('[data-toggle="tooltip"]').tooltip();
		    }
		}).on('draw', function () {
			/*
			    $.each(table.column(0).data(), function(i, val) {
					$('#select2_potential_dt_'+val).select2(select2Optioneee).on('change', function (e) {
		            	//console.log('#select2_potential_dt_'+val);
		            	setTeamsAjaxData('#select2_potential_dt_'+val, val, 'setTeamsP');
		            });

					$('#select2_satisfaction_dt_'+val).select2(select2Optioneee).on('change', function (e) {
		            	//console.log('#select2_satisfaction_dt_'+val);
		            	setTeamsAjaxData('#select2_satisfaction_dt_'+val, val, 'setTeamsS');
		            });
				});
			*/
		});

		$('#kt_search').on('click', function(e) {
			e.preventDefault();

			$('#potential').val($('#select2_potential_search').val()+'|'+$('#select2_satisfaction_search').val());
			$('#ms').val($('#msmin').val()+'|'+$('#msmax').val());
			$('#ks').val($('#ksmin').val()+'|'+$('#ksmax').val());

			var params = {};
			$('.datatable-input').each(function() {
				//console.log($(this));
				var i = $(this).data('col-index');
				if (params[i]) {
					params[i] += '|' + $(this).val();
				}
				else {
					params[i] = $(this).val();
				}
			});
			$.each(params, function(i, val) {
				// apply search params to datatable
				table.column(i).search(val ? val : '', false, false);
			});

			table.table().draw();
		});

		$('#kt_reset').on('click', function(e) {
			e.preventDefault();
			$('.datatable-input').each(function() {
				if($(this).prop("tagName") == 'INPUT'){
					$(this).val('');
				}else if($(this).prop("tagName") == 'SELECT'){
					$(this).val(null).change();
				}
				
				table.column($(this).data('col-index')).search('', false, false);
			});

			table.table().draw();
		});
	};

	var initTableMatchPlayers = function() {

		// begin first table
		var table = $('#kt_datatable_match_players').DataTable({
			responsive: true,
			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[0, 'desc']],
            language: {
                "lengthMenu": wordsjs["lengthMenu"],
                "info": wordsjs["info"],
                "infoEmpty": wordsjs["infoEmpty"],
                "zeroRecords": wordsjs["zeroRecords"],
                "infoFiltered": wordsjs["infoFiltered"],
                "processing": wordsjs["processing"],
                "search": wordsjs["search"],
            },
			columnDefs: [
				{targets: 0, visible: false},
				{targets: -1, orderable: false},
			],
			
		});

		$('.match_player_edit_button').on('click', function(e) {
			e.preventDefault();
		    var target = $(this).attr('href');
		    $('html, body').animate({
		    	scrollTop: ($('#match_playerForm').offset().top)
		    }, 1000);

			$.ajax({
                method: 'GET',
                url : '/getMatchPlayerF/'+table.row(this.id).data()[0],
                dataType: 'json',
                success: function(data, textStatus, jqXHR)
                {
					$("#match_playerForm input[name=match_player_id]").val(data.items['id']);
					$("#match_playerForm select[name=player_id]").val(data.items['player_id']).trigger('change');
					$("#match_playerForm select[name=player_id]").prop("disabled", true);
					$("#match_playerForm input[name=guest_name]").val(data.items['guest_name']);
					$("#match_playerForm select[name=position_id]").val(data.items['position_id']).trigger('change');
					$("#match_playerForm input[name=rating]").val(data.items['rating']);
					$("#match_playerForm input[name=saving]").val(data.items['saving']);
                },
                error: function (jqXHR, textStatus, errorThrown)
                {

                }
            });
		});
	};

	var initTableMatchActions = function() {

		// begin first table
		var table = $('#kt_datatable_match_actions').DataTable({
			responsive: true,
			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[0, 'desc']],
            language: {
                "lengthMenu": wordsjs["lengthMenu"],
                "info": wordsjs["info"],
                "infoEmpty": wordsjs["infoEmpty"],
                "zeroRecords": wordsjs["zeroRecords"],
                "infoFiltered": wordsjs["infoFiltered"],
                "processing": wordsjs["processing"],
                "search": wordsjs["search"],
            },
			columnDefs: [
				//{targets: 0, visible: false},
				{targets: -1, orderable: false},
			],
			
		});

		$('.match_action_edit_button').on('click', function(e) {
			e.preventDefault();
		    var target = $(this).attr('href');
		    $('html, body').animate({
		    	scrollTop: ($('#match_actionForm').offset().top)
		    }, 1000);

			$.ajax({
                method: 'GET',
                url : '/getMatchActionF/'+table.row(this.id).data()[0],
                dataType: 'json',
                success: function(data, textStatus, jqXHR)
                {	

                	$("#match_actionForm input[name=match_action_id]").val(data.items['id']);

                	$("#match_actionForm select[name=embed_source]").val(data.items['embed_source']).trigger('change');
                	$("#match_actionForm input[name=embed_code]").val(data.items['embed_code']);
                	$("#match_actionForm select[name=team_id]").val(data.items['team_id']).trigger('change');
					//$("#match_actionForm select[name=team_id]").prop("disabled", true);
                	$("#match_actionForm select[name=match_player_id]").val(data.items['match_player_id']).trigger('change');
					//$("#match_actionForm select[name=match_player_id]").prop("disabled", true);
					$("#match_actionForm select[name=type]").val(data.items['type']).trigger('change');
					$("#match_actionForm input[name=minute]").val(data.items['minute']);

                },
                error: function (jqXHR, textStatus, errorThrown)
                {

                }
            });
		});
	};

	var initTableMatchPanoramas = function() {

		// begin first table
		var table = $('#kt_datatable_match_panoramas').DataTable({
			responsive: true,
			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[0, 'desc']],
            language: {
                "lengthMenu": wordsjs["lengthMenu"],
                "info": wordsjs["info"],
                "infoEmpty": wordsjs["infoEmpty"],
                "zeroRecords": wordsjs["zeroRecords"],
                "infoFiltered": wordsjs["infoFiltered"],
                "processing": wordsjs["processing"],
                "search": wordsjs["search"],
            },
			columnDefs: [
				//{targets: 0, visible: false},
				{targets: -1, orderable: false},
			],
			
		});

		$('.match_panorama_edit_button').on('click', function(e) {
			e.preventDefault();
		    var target = $(this).attr('href');
		    $('html, body').animate({
		    	scrollTop: ($('#match_panoramaForm').offset().top)
		    }, 1000);

			$.ajax({
                method: 'GET',
                url : '/getMatchPanoramaF/'+table.row(this.id).data()[0],
                dataType: 'json',
                success: function(data, textStatus, jqXHR)
                {	

                	$("#match_panoramaForm input[name=match_panorama_id]").val(data.items['id']);

                	$("#match_panoramaForm select[name=panorama_id]").val(data.items['panorama_id']).trigger('change');
                	$("#match_panoramaForm select[name=match_player_id_mpa]").val(data.items['match_player_id']).trigger('change');

                	$("#match_panoramaForm select[name=embed_source_mpa]").val(data.items['embed_source']).trigger('change');
                	$("#match_panoramaForm input[name=embed_code_mpa]").val(data.items['embed_code']);

                },
                error: function (jqXHR, textStatus, errorThrown)
                {

                }
            });
		});
	};

	var initTableMatchPress = function() {

		// begin first table
		var table = $('#kt_datatable_match_press').DataTable({
			responsive: true,
			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[0, 'desc']],
            language: {
                "lengthMenu": wordsjs["lengthMenu"],
                "info": wordsjs["info"],
                "infoEmpty": wordsjs["infoEmpty"],
                "zeroRecords": wordsjs["zeroRecords"],
                "infoFiltered": wordsjs["infoFiltered"],
                "processing": wordsjs["processing"],
                "search": wordsjs["search"],
            },
			columnDefs: [
				//{targets: 0, visible: false},
				{targets: -1, orderable: false},
			],
			
		});

		$('.match_press_edit_button').on('click', function(e) {
			e.preventDefault();
			console.log('qwe');
		    var target = $(this).attr('href');
		    $('html, body').animate({
		    	scrollTop: ($('#match_pressForm').offset().top)
		    }, 1000);

			$.ajax({
                method: 'GET',
                url : '/getMatchPressF/'+table.row(this.id).data()[0],
                dataType: 'json',
                success: function(data, textStatus, jqXHR)
                {	
                	$("#match_pressForm input[name=match_press_id]").val(data.items['id']);
                	$("#match_pressForm select[name=team_id_mprs]").val(data.items['team_id']).trigger('change');

                	$("#match_pressForm select[name=embed_source_mprs]").val(data.items['embed_source']).trigger('change');
                	$("#match_pressForm input[name=embed_code_mprs]").val(data.items['embed_code']);

                	$("#select2_players_id_mprs").val(data.items['press_conferences_players']).trigger('change');

                	//$("#match_pressForm select[name=players_id_mprs]").val(['1829390','1829389']).trigger('change');
                	//$("#match_pressForm select[name=players_id_mprs]").select2('val', ["1829390", "1829389"]);
                	//$('#match_pressForm select[name=players_id_mprs]').select2().select2('val', ['1829390', '1829389']);
                	//$('#match_pressForm select[name=players_id_mprs]').select2().val(['1829390', '1829389']).trigger('change');

                	//$('#match_pressForm select[name=players_id_mprs]').select2({}).select2('val', ['1829390', '1829389']);
                },
                error: function (jqXHR, textStatus, errorThrown)
                {

                }
            });
		});
	};

	var initTableEliminationTree = function() {
		
		var queryString = '';
		if($('#tree_id').val() != ""){
			queryString = '?tree_id='+$('#tree_id').val();
		}

		// begin first table
		var table = $('#kt_datatable_elimination_tree').DataTable({

			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,

			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[1, 'asc']],
            language: {
                "lengthMenu": wordsjs["lengthMenu"],
                "info": wordsjs["info"],
                "infoEmpty": wordsjs["infoEmpty"],
                "zeroRecords": wordsjs["zeroRecords"],
                "infoFiltered": wordsjs["infoFiltered"],
                "processing": wordsjs["processing"],
                "search": wordsjs["search"],
            },

			ajax: {
				url: HOST_URL + '/getEliminationTrees',
				type: 'POST',
				headers: { 'X-CSRF-TOKEN': $('#token').val() },
				data: function(data){
					data.columnsDef = ['id', 'name', 'season', 'type', 'actions'];
					data.treeId = $('#tree_id').val();
			    },
			},
			columns: [
				{name: 'id'},
				{name: 'name'},
				{name: 'season'},
				{name: 'type'},
				{name: 'actions', responsivePriority: -1},
			],
			columnDefs: [

				{targets: 0, title: 'ID', visible: false},
				{targets: 1, title: wordsjs["name"]},
				{targets: 2, title: wordsjs["season"]},
				{targets: 3, title: wordsjs["type"]},
				{
					targets: -1,
					title: wordsjs['action'],
					orderable: false,
					width: '150px',
					render: function(data, type, full, meta) {
						return '\
						<a href="/elimination_tree/view/' + full[0] + queryString + '" class="btn btn-sm btn-clean btn-icon btn_view_eliminationtree" title="' + wordsjs['view'] + '">\
							<i class="fas fa-eye"></i>\
						</a>\
						<a href="/elimination_tree/edit/' + full[0] + queryString + '" class="btn btn-sm btn-clean btn-icon btn_change_eliminationtree" title="' + wordsjs['edit'] + '">\
							<i class="fas fa-edit"></i>\
						</a>\
						<a href="/elimination_tree/delete/' + full[0] + queryString + '" class="btn btn-sm btn-clean btn-icon btn_delete_eliminationtree" title="' + wordsjs['delete'] + '">\
							<i class="fas fa-trash-alt"></i>\
						</a>\
						<a href="/elimination_tree/tree/' + full[0] + queryString + '" class="btn btn-sm btn-clean btn-icon" title="Eleme Ağacı">\
							<i class="fas fa-sitemap"></i>\
						</a>\
						';
					},
				},

			],
			drawCallback: function (settings) {
				viButEliminationtree();
		    }
		});

		$('#kt_search').on('click', function(e) {
			e.preventDefault();
			
			var params = {};
			$('.datatable-input').each(function() {
				//console.log($(this));
				var i = $(this).data('col-index');
				if (params[i]) {
					params[i] += '|' + $(this).val();
				}
				else {
					params[i] = $(this).val();
				}
			});
			$.each(params, function(i, val) {
				// apply search params to datatable
				table.column(i).search(val ? val : '', false, false);
			});

			table.table().draw();
		});

		$('#kt_reset').on('click', function(e) {
			e.preventDefault();
			$('.datatable-input').each(function() {
				if($(this).prop("tagName") == 'INPUT'){
					$(this).val('');
				}else if($(this).prop("tagName") == 'SELECT'){
					$(this).val(null).change();
				}
				
				table.column($(this).data('col-index')).search('', false, false);
			});

			table.table().draw();
		});
	};

	var initTableFixtureGroup = function() {
		
		var queryString = '';
		if($('#tree_id').val() != ""){
			queryString = '?tree_id='+$('#tree_id').val();
		}

		// begin first table
		var table = $('#kt_datatable_fixture_group').DataTable({

			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,

			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[1, 'asc']],
            language: {
                "lengthMenu": wordsjs["lengthMenu"],
                "info": wordsjs["info"],
                "infoEmpty": wordsjs["infoEmpty"],
                "zeroRecords": wordsjs["zeroRecords"],
                "infoFiltered": wordsjs["infoFiltered"],
                "processing": wordsjs["processing"],
                "search": wordsjs["search"],
            },

			ajax: {
				url: HOST_URL + '/getFixtureGroup',
				type: 'POST',
				headers: { 'X-CSRF-TOKEN': $('#token').val() },
				data: function(data){
					data.columnsDef = ['id', 'name', 'season', 'actions'];
					data.treeId = $('#tree_id').val();
			    },
			},
			columns: [
				{name: 'id'},
				{name: 'name'},
				{name: 'season'},
				{name: 'actions', responsivePriority: -1},
			],
			columnDefs: [

				{targets: 0, title: 'ID', visible: false},
				{targets: 1, title: wordsjs["name"]},
				{targets: 2, title: wordsjs["season"]},
				{
					targets: -1,
					title: wordsjs['action'],
					orderable: false,
					width: '150px',
					render: function(data, type, full, meta) {
						return '\
						<a href="/fixture_group/view/' + full[0] + queryString + '" class="btn btn-sm btn-clean btn-icon btn_view_fixturegroup" title="' + wordsjs['view'] + '">\
							<i class="fas fa-eye"></i>\
						</a>\
						<a href="/fixture_group/edit/' + full[0] + queryString + '" class="btn btn-sm btn-clean btn-icon btn_change_fixturegroup" title="' + wordsjs['edit'] + '">\
							<i class="fas fa-edit"></i>\
						</a>\
						<a href="/fixture_group/delete/' + full[0] + queryString + '" class="btn btn-sm btn-clean btn-icon btn_delete_fixturegroup" title="' + wordsjs['delete'] + '">\
							<i class="fas fa-trash-alt"></i>\
						</a>\
						<a href="/fixture_group/edit_order/' + full[0] + queryString + '" class="btn btn-sm btn-clean btn-icon btn_editorder_fixturegroup" title="' + wordsjs['editorder'] + '">\
							<i class="fas fa-list"></i>\
						</a>\
						';
					},
				},

			],
			drawCallback: function (settings) {
				viButEliminationtree();
		    }
		});

		$('#kt_search').on('click', function(e) {
			e.preventDefault();
			
			var params = {};
			$('.datatable-input').each(function() {
				//console.log($(this));
				var i = $(this).data('col-index');
				if (params[i]) {
					params[i] += '|' + $(this).val();
				}
				else {
					params[i] = $(this).val();
				}
			});
			$.each(params, function(i, val) {
				// apply search params to datatable
				table.column(i).search(val ? val : '', false, false);
			});

			table.table().draw();
		});

		$('#kt_reset').on('click', function(e) {
			e.preventDefault();
			$('.datatable-input').each(function() {
				if($(this).prop("tagName") == 'INPUT'){
					$(this).val('');
				}else if($(this).prop("tagName") == 'SELECT'){
					$(this).val(null).change();
				}
				
				table.column($(this).data('col-index')).search('', false, false);
			});

			table.table().draw();
		});
	};

	var initTablePlayerPositions = function() {

		// begin first table
		var table = $('#kt_datatable_player_positions').DataTable({

			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,

			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[3, 'asc']],
            language: {
                "lengthMenu": wordsjs["lengthMenu"],
                "info": wordsjs["info"],
                "infoEmpty": wordsjs["infoEmpty"],
                "zeroRecords": wordsjs["zeroRecords"],
                "infoFiltered": wordsjs["infoFiltered"],
                "processing": wordsjs["processing"],
                "search": wordsjs["search"],
            },

			ajax: {
				url: HOST_URL + '/getPlayerPositions',
				type: 'POST',
				headers: { 'X-CSRF-TOKEN': $('#token').val() },
				data: {
					columnsDef: ['id', 'name', 'code', 'actions'],
				},
			},
			columns: [
				{name: 'id'},
				{name: 'name'},
				{name: 'code'},
				{name: 'order'},
				{name: 'actions', responsivePriority: -1},
			],
			columnDefs: [

				{targets: 0, title: 'ID', visible: false},
				{targets: 1, title: wordsjs["name"]},
				{targets: 2, title: wordsjs["code"]},
				{targets: 3, title: wordsjs["rder"]},
				{
					targets: -1,
					title: wordsjs['action'],
					orderable: false,
					width: '150px',
					render: function(data, type, full, meta) {
						return '\
						<a href="player_positions/view/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_view_position" title="' + wordsjs['view'] + '">\
							<i class="fas fa-eye"></i>\
						</a>\
						<a href="player_positions/edit/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_change_position" title="' + wordsjs['edit'] + '">\
							<i class="fas fa-edit"></i>\
						</a>\
						<a href="player_positions/delete/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_delete_position" title="' + wordsjs['delete'] + '">\
							<i class="fas fa-trash-alt"></i>\
						</a>\
						';
					},
				},

			],
			drawCallback: function (settings) {
				viButPosition();
		    }
		});

		$('#kt_search').on('click', function(e) {
			e.preventDefault();
			
			var params = {};
			$('.datatable-input').each(function() {
				//console.log($(this));
				var i = $(this).data('col-index');
				if (params[i]) {
					params[i] += '|' + $(this).val();
				}
				else {
					params[i] = $(this).val();
				}
			});
			$.each(params, function(i, val) {
				// apply search params to datatable
				table.column(i).search(val ? val : '', false, false);
			});

			table.table().draw();
		});

		$('#kt_reset').on('click', function(e) {
			e.preventDefault();
			$('.datatable-input').each(function() {
				if($(this).prop("tagName") == 'INPUT'){
					$(this).val('');
				}else if($(this).prop("tagName") == 'SELECT'){
					$(this).val(null).change();
				}
				
				table.column($(this).data('col-index')).search('', false, false);
			});

			table.table().draw();
		});
	};

	var initTablePointTypes = function() {

		// begin first table
		var table = $('#kt_datatable_point_types').DataTable({

			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,

			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[0, 'desc']],
            language: {
                "lengthMenu": wordsjs["lengthMenu"],
                "info": wordsjs["info"],
                "infoEmpty": wordsjs["infoEmpty"],
                "zeroRecords": wordsjs["zeroRecords"],
                "infoFiltered": wordsjs["infoFiltered"],
                "processing": wordsjs["processing"],
                "search": wordsjs["search"],
            },

			ajax: {
				url: HOST_URL + '/getPointTypes',
				type: 'POST',
				headers: { 'X-CSRF-TOKEN': $('#token').val() },
				data: {
					columnsDef: ['id', 'name', 'min_point', 'max_point', 'win_score', 'defeat_score', 'draw_score', 'actions'],
				},
			},
			columns: [
				{name: 'id'},
				{name: 'name'},
				{name: 'min_point'},
				{name: 'max_point'},
				{name: 'win_score'},
				{name: 'defeat_score'},
				{name: 'draw_score'},
				{name: 'actions', responsivePriority: -1},
			],
			columnDefs: [

				{targets: 0, title: 'ID', visible: false},
				{targets: 1, title: wordsjs["name"]},
				{targets: 2, title: wordsjs["min_points"]},
				{targets: 3, title: wordsjs["max_points"]},
				{targets: 4, title: wordsjs["win_points"]},
				{targets: 5, title: wordsjs["defeat_score"]},
				{targets: 6, title: wordsjs["draw_score"]},
				{
					targets: -1,
					title: wordsjs['action'],
					orderable: false,
					width: '150px',
					render: function(data, type, full, meta) {
						return '\
						<a href="point_types/view/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_view_pointtype" title="' + wordsjs['view'] + '">\
							<i class="fas fa-eye"></i>\
						</a>\
						<a href="point_types/edit/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_change_pointtype" title="' + wordsjs['edit'] + '">\
							<i class="fas fa-edit"></i>\
						</a>\
						<a href="point_types/delete/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_delete_pointtype" title="' + wordsjs['delete'] + '">\
							<i class="fas fa-trash-alt"></i>\
						</a>\
						';
					},
				},

			],
			drawCallback: function (settings) {
				viButPointType();
		    }
		});

		$('#kt_search').on('click', function(e) {
			e.preventDefault();
			
			var params = {};
			$('.datatable-input').each(function() {
				//console.log($(this));
				var i = $(this).data('col-index');
				if (params[i]) {
					params[i] += '|' + $(this).val();
				}
				else {
					params[i] = $(this).val();
				}
			});
			$.each(params, function(i, val) {
				// apply search params to datatable
				table.column(i).search(val ? val : '', false, false);
			});

			table.table().draw();
		});

		$('#kt_reset').on('click', function(e) {
			e.preventDefault();
			$('.datatable-input').each(function() {
				if($(this).prop("tagName") == 'INPUT'){
					$(this).val('');
				}else if($(this).prop("tagName") == 'SELECT'){
					$(this).val(null).change();
				}
				
				table.column($(this).data('col-index')).search('', false, false);
			});

			table.table().draw();
		});
	};

	var initTableTactics = function() {

		// begin first table
		var table = $('#kt_datatable_tactics').DataTable({

			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,

			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[0, 'desc']],
            language: {
                "lengthMenu": wordsjs["lengthMenu"],
                "info": wordsjs["info"],
                "infoEmpty": wordsjs["infoEmpty"],
                "zeroRecords": wordsjs["zeroRecords"],
                "infoFiltered": wordsjs["infoFiltered"],
                "processing": wordsjs["processing"],
                "search": wordsjs["search"],
            },

			ajax: {
				url: HOST_URL + '/getTactics',
				type: 'POST',
				headers: { 'X-CSRF-TOKEN': $('#token').val() },
				data: {
					columnsDef: ['id', 'name', 'actions'],
				},
			},
			columns: [
				{name: 'id'},
				{name: 'name'},
				{name: 'actions', responsivePriority: -1},
			],
			columnDefs: [

				{targets: 0, title: 'ID', visible: false},
				{targets: 1, title: wordsjs["name"]},
				{
					targets: -1,
					title: wordsjs['action'],
					orderable: false,
					width: '150px',
					render: function(data, type, full, meta) {
						return '\
						<a href="tactics/view/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_view_tactic" title="' + wordsjs['view'] + '">\
							<i class="fas fa-eye"></i>\
						</a>\
						<a href="tactics/edit/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_change_tactic" title="' + wordsjs['edit'] + '">\
							<i class="fas fa-edit"></i>\
						</a>\
						<a href="tactics/delete/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_delete_tactic" title="' + wordsjs['delete'] + '">\
							<i class="fas fa-trash-alt"></i>\
						</a>\
						';
					},
				},
			],
			drawCallback: function (settings) {
				viButTactic();
		    }
		});

		$('#kt_search').on('click', function(e) {
			e.preventDefault();
			
			var params = {};
			$('.datatable-input').each(function() {
				//console.log($(this));
				var i = $(this).data('col-index');
				if (params[i]) {
					params[i] += '|' + $(this).val();
				}
				else {
					params[i] = $(this).val();
				}
			});
			$.each(params, function(i, val) {
				// apply search params to datatable
				table.column(i).search(val ? val : '', false, false);
			});

			table.table().draw();
		});

		$('#kt_reset').on('click', function(e) {
			e.preventDefault();
			$('.datatable-input').each(function() {
				if($(this).prop("tagName") == 'INPUT'){
					$(this).val('');
				}else if($(this).prop("tagName") == 'SELECT'){
					$(this).val(null).change();
				}
				
				table.column($(this).data('col-index')).search('', false, false);
			});

			table.table().draw();
		});
	};

	var initTableTeamPotentials = function() {

		// begin first table
		var table = $('#kt_datatable_team_potentials').DataTable({

			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,

			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[0, 'desc']],
            language: {
                "lengthMenu": wordsjs["lengthMenu"],
                "info": wordsjs["info"],
                "infoEmpty": wordsjs["infoEmpty"],
                "zeroRecords": wordsjs["zeroRecords"],
                "infoFiltered": wordsjs["infoFiltered"],
                "processing": wordsjs["processing"],
                "search": wordsjs["search"],
            },

			ajax: {
				url: HOST_URL + '/getTeamPotentials',
				type: 'POST',
				headers: { 'X-CSRF-TOKEN': $('#token').val() },
				data: {
					columnsDef: ['id', 'name', 'actions'],
				},
			},
			columns: [
				{name: 'id'},
				{name: 'name'},
				{name: 'actions', responsivePriority: -1},
			],
			columnDefs: [

				{targets: 0, title: 'ID', visible: false},
				{targets: 1, title: wordsjs["name"]},
				{
					targets: -1,
					title: wordsjs['action'],
					orderable: false,
					width: '150px',
					render: function(data, type, full, meta) {
						return '\
						<a href="team_potentials/view/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_view_potential" title="' + wordsjs['view'] + '">\
							<i class="fas fa-eye"></i>\
						</a>\
						<a href="team_potentials/edit/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_change_potential" title="' + wordsjs['edit'] + '">\
							<i class="fas fa-edit"></i>\
						</a>\
						<a href="team_potentials/delete/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_delete_potential" title="' + wordsjs['delete'] + '">\
							<i class="fas fa-trash-alt"></i>\
						</a>\
						';
					},
				},

			],
			drawCallback: function (settings) {
				viButPotential();
		    }
		});

		$('#kt_search').on('click', function(e) {
			e.preventDefault();
			
			var params = {};
			$('.datatable-input').each(function() {
				//console.log($(this));
				var i = $(this).data('col-index');
				if (params[i]) {
					params[i] += '|' + $(this).val();
				}
				else {
					params[i] = $(this).val();
				}
			});
			$.each(params, function(i, val) {
				// apply search params to datatable
				table.column(i).search(val ? val : '', false, false);
			});

			table.table().draw();
		});

		$('#kt_reset').on('click', function(e) {
			e.preventDefault();
			$('.datatable-input').each(function() {
				if($(this).prop("tagName") == 'INPUT'){
					$(this).val('');
				}else if($(this).prop("tagName") == 'SELECT'){
					$(this).val(null).change();
				}
				
				table.column($(this).data('col-index')).search('', false, false);
			});

			table.table().draw();
		});
	};

	var initTableTeamSatisfactions = function() {

		// begin first table
		var table = $('#kt_datatable_team_satisfactions').DataTable({

			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,

			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[0, 'desc']],
            language: {
                "lengthMenu": wordsjs["lengthMenu"],
                "info": wordsjs["info"],
                "infoEmpty": wordsjs["infoEmpty"],
                "zeroRecords": wordsjs["zeroRecords"],
                "infoFiltered": wordsjs["infoFiltered"],
                "processing": wordsjs["processing"],
                "search": wordsjs["search"],
            },

			ajax: {
				url: HOST_URL + '/getTeamSatisfactions',
				type: 'POST',
				headers: { 'X-CSRF-TOKEN': $('#token').val() },
				data: {
					columnsDef: ['id', 'name', 'actions'],
				},
			},
			columns: [
				{name: 'id'},
				{name: 'name'},
				{name: 'actions', responsivePriority: -1},
			],
			columnDefs: [

				{targets: 0, title: 'ID', visible: false},
				{targets: 1, title: wordsjs["name"]},
				{
					targets: -1,
					title: wordsjs['action'],
					orderable: false,
					width: '150px',
					render: function(data, type, full, meta) {
						return '\
						<a href="team_satisfactions/view/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_view_satisfaction" title="' + wordsjs['view'] + '">\
							<i class="fas fa-eye"></i>\
						</a>\
						<a href="team_satisfactions/edit/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_change_satisfaction" title="' + wordsjs['edit'] + '">\
							<i class="fas fa-edit"></i>\
						</a>\
						<a href="team_satisfactions/delete/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_delete_satisfaction" title="' + wordsjs['delete'] + '">\
							<i class="fas fa-trash-alt"></i>\
						</a>\
						';
					},
				},
			],
			drawCallback: function (settings) {
				viButSatisfaction();
		    }
		});

		$('#kt_search').on('click', function(e) {
			e.preventDefault();
			
			var params = {};
			$('.datatable-input').each(function() {
				//console.log($(this));
				var i = $(this).data('col-index');
				if (params[i]) {
					params[i] += '|' + $(this).val();
				}
				else {
					params[i] = $(this).val();
				}
			});
			$.each(params, function(i, val) {
				// apply search params to datatable
				table.column(i).search(val ? val : '', false, false);
			});

			table.table().draw();
		});

		$('#kt_reset').on('click', function(e) {
			e.preventDefault();
			$('.datatable-input').each(function() {
				if($(this).prop("tagName") == 'INPUT'){
					$(this).val('');
				}else if($(this).prop("tagName") == 'SELECT'){
					$(this).val(null).change();
				}
				
				table.column($(this).data('col-index')).search('', false, false);
			});

			table.table().draw();
		});
	};

	var initTablePenaltyTypes = function() {

		// begin first table
		var table = $('#kt_datatable_penalty_types').DataTable({

			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,

			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[0, 'desc']],
            language: {
                "lengthMenu": wordsjs["lengthMenu"],
                "info": wordsjs["info"],
                "infoEmpty": wordsjs["infoEmpty"],
                "zeroRecords": wordsjs["zeroRecords"],
                "infoFiltered": wordsjs["infoFiltered"],
                "processing": wordsjs["processing"],
                "search": wordsjs["search"],
            },

			ajax: {
				url: HOST_URL + '/getPenaltyTypes',
				type: 'POST',
				headers: { 'X-CSRF-TOKEN': $('#token').val() },
				data: {
					columnsDef: ['id', 'name', 'to_who', 'actions'],
				},
			},
			columns: [
				{name: 'id'},
				{name: 'name'},
				{name: 'to_who'},
				{name: 'actions', responsivePriority: -1},
			],
			columnDefs: [

				{targets: 0, title: 'ID', visible: false},
				{targets: 1, title: wordsjs["name"]},
				{targets: 2, title: wordsjs["to_who"]},
				{
					targets: -1,
					title: wordsjs['action'],
					orderable: false,
					width: '150px',
					render: function(data, type, full, meta) {
						return '\
						<a href="penalty_types/view/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_view_tactic" title="' + wordsjs['view'] + '">\
							<i class="fas fa-eye"></i>\
						</a>\
						<a href="penalty_types/edit/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_change_tactic" title="' + wordsjs['edit'] + '">\
							<i class="fas fa-edit"></i>\
						</a>\
						<a href="penalty_types/delete/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_delete_tactic" title="' + wordsjs['delete'] + '">\
							<i class="fas fa-trash-alt"></i>\
						</a>\
						';
					},
				},
			],
			drawCallback: function (settings) {
				viButTactic();
		    }
		});

		$('#kt_search').on('click', function(e) {
			e.preventDefault();
			
			var params = {};
			$('.datatable-input').each(function() {
				//console.log($(this));
				var i = $(this).data('col-index');
				if (params[i]) {
					params[i] += '|' + $(this).val();
				}
				else {
					params[i] = $(this).val();
				}
			});
			$.each(params, function(i, val) {
				// apply search params to datatable
				table.column(i).search(val ? val : '', false, false);
			});

			table.table().draw();
		});

		$('#kt_reset').on('click', function(e) {
			e.preventDefault();
			$('.datatable-input').each(function() {
				if($(this).prop("tagName") == 'INPUT'){
					$(this).val('');
				}else if($(this).prop("tagName") == 'SELECT'){
					$(this).val(null).change();
				}
				
				table.column($(this).data('col-index')).search('', false, false);
			});

			table.table().draw();
		});
	};

	var initTablePenalties = function() {

		// begin first table
		var table = $('#kt_datatable_penalties').DataTable({

			responsive: true,
			searchDelay: 500,
			processing: true,
			serverSide: true,

			dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			lengthMenu: [5, 10, 25, 50, 100],
            pageLength: 50,
            order: [[0, 'desc']],
            language: {
                "lengthMenu": wordsjs["lengthMenu"],
                "info": wordsjs["info"],
                "infoEmpty": wordsjs["infoEmpty"],
                "zeroRecords": wordsjs["zeroRecords"],
                "infoFiltered": wordsjs["infoFiltered"],
                "processing": wordsjs["processing"],
                "search": wordsjs["search"],
            },

			ajax: {
				url: HOST_URL + '/getPenalties',
				type: 'POST',
				headers: { 'X-CSRF-TOKEN': $('#token').val() },
				data: {
					columnsDef: ['id', 'season', 'match', 'team', 'player', 'polymorphic_ctype_id', 'type', 'actions'],
				},
			},
			columns: [
				{name: 'id'},
				{name: 'season'},
				{name: 'match'},
				{name: 'team'},
				{name: 'player'},
				{name: 'polymorphic_ctype_id'},
				{name: 'type'},
				{name: 'actions', responsivePriority: -1},
			],
			columnDefs: [

				{targets: 0, title: 'ID', visible: false},
				{targets: 1, title: wordsjs["season"]},
				{targets: 2, title: wordsjs["match"]},
				{targets: 3, title: wordsjs["team"]},
				{targets: 4, title: wordsjs["player"]},
				{targets: 5, title: wordsjs["penalty_type"]},
				{targets: 6, visible: false},
				{
					targets: -1,
					title: wordsjs['action'],
					orderable: false,
					width: '150px',
					render: function(data, type, full, meta) {
						return '\
						<a href="'+full[6]+'_penalties/view/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_view_tactic" title="' + wordsjs['view'] + '">\
							<i class="fas fa-eye"></i>\
						</a>\
						<a href="'+full[6]+'_penalties/edit/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_change_tactic" title="' + wordsjs['edit'] + '">\
							<i class="fas fa-edit"></i>\
						</a>\
						<a href="'+full[6]+'_penalties/delete/'+full[0]+'" class="btn btn-sm btn-clean btn-icon btn_delete_tactic" title="' + wordsjs['delete'] + '">\
							<i class="fas fa-trash-alt"></i>\
						</a>\
						';
					},
				},
			],
			drawCallback: function (settings) {
				viButTactic();
		    }
		});

		$('#kt_search').on('click', function(e) {
			e.preventDefault();
			
			var params = {};
			$('.datatable-input').each(function() {
				//console.log($(this));
				var i = $(this).data('col-index');
				if (params[i]) {
					params[i] += '|' + $(this).val();
				}
				else {
					params[i] = $(this).val();
				}
			});
			$.each(params, function(i, val) {
				// apply search params to datatable
				table.column(i).search(val ? val : '', false, false);
			});

			table.table().draw();
		});

		$('#kt_reset').on('click', function(e) {
			e.preventDefault();
			$('.datatable-input').each(function() {
				if($(this).prop("tagName") == 'INPUT'){
					$(this).val('');
				}else if($(this).prop("tagName") == 'SELECT'){
					$(this).val(null).change();
				}
				
				table.column($(this).data('col-index')).search('', false, false);
			});

			table.table().draw();
		});
	};



	return {

		//main function to initiate the module
		init: function() {
			initTableUsers();
			initTablePlayerList();
			initTableRoles();
			initTableConferences();
			initTableProvinces();
			initTableDistricts();
			initTableGrounds();
			initTableGroundProperties();
			initTableGroundHour();
			initTableLeagues();
			initTableSeasons();
			initTableSeasonsAddBonus();
			initTableTeams();
			initTableTeams_Seasons();
			initTableTeams_Players();
			initTableMatches();
			initTableMatchPlayers();
			initTableMatchActions();
			initTableMatchPanoramas();
			initTableMatchPress();
			initTableReservations();
			initTableEliminationTree();
			initTableFixtureGroup();
			initTablePlayerPositions();
			initTablePointTypes();
			initTableTactics();
			initTableTeamPotentials();
			initTableTeamSatisfactions();
			initTablePenaltyTypes();
			initTablePenalties();
		},

	};

}();



jQuery(document).ready(function() {
	KTDatatablesDataSourceAjaxServer.init();
});
