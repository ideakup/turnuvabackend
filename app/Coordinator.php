<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Coordinator extends Model
{
    protected $table = 'coordinators';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    //public.coordinators_leagues.coordinators_leagues_coordinator_id_b717a3b6_fk_coordinators_id	


    //public.matches.matches_coordinator_id_427875e1_fk_coordinators_id	
    
}