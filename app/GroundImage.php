<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class GroundImage extends Model
{
    protected $table = 'ground_images';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;

    public function ground()
    {
        return $this->hasOne('App\Ground', 'id', 'ground_id');
    }

}