<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class TransferSeason extends Model
{
    protected $table = 'transfers_seasons';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;

    //public.transfers_seasons.transfers_seasons_season_id_b905c643_fk_seasons_id
    public function season()
    {
        return $this->hasOne('App\Season', 'id', 'season_id');
    }

	//public.transfers_seasons.transfers_seasons_transfer_id_8dc1c694_fk_transfers_id
    public function transfer()
    {
        return $this->hasOne('App\Transfer', 'id', 'transfer_id');
    }

}