<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class GalleriesLeagues extends Model
{
    protected $table = 'galleries_leagues';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;

	//public.galleries_leagues.galleries_leagues_gallery_id_f0a9d9b1_fk_galleries_id
    public function galleries()
    {
        return $this->hasMany('App\Galleries', 'gallery_id', 'id');
    }
	
	//public.galleries_leagues.galleries_leagues_league_id_ab8949c2_fk_leagues_id
    public function leagues()
    {
        return $this->hasMany('App\League', 'league_id', 'id');
    }
	
}
