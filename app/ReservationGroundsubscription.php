<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class ReservationGroundsubscription extends Model
{
    protected $table = 'reservation_groundsubscription';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;

    //	public.reservation_groundsubscription.reservation_groundsubscription_ground_id_1b098520_fk_grounds_id	auto
    public function ground()
    {
        return $this->hasOne('App\Ground', 'id', 'ground_id');
    }

}