<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $table = 'tags';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;
    
}