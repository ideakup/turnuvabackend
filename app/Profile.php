<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Profile extends Model
{
    protected $table = 'profiles';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;
    
    public function district()
    {
        return $this->hasOne('App\District', 'id', 'district_id');
    }

    public function province()
    {
        return $this->hasOne('App\Province', 'id', 'province_id');
    }

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

}