<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class GroupTeam extends Model
{
    protected $table = 'groups_teams';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;

    public function team()
    {
        return $this->hasOne('App\Team', 'id', 'team_id');
    }

}