<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class PointType extends Model
{
    protected $table = 'point_types';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;

}