<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Referee extends Model
{
    protected $table = 'referees';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    //public.matches.matches_referee_id_4baa2316_fk_referees_id	

    //public.referees_leagues.referees_leagues_referee_id_56d06b51_fk_referees_id

}