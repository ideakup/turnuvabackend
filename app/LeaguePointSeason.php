<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class LeaguePointSeason extends Model
{
    protected $table = 'league_pointseason';
    protected $primaryKey = 'season_ptr_id';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;

}