<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Tactic extends Model
{
    protected $table = 'tactics';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;

    //public.teams.teams_tactic_id_f36b6586_fk_tactics_id
    public function team()
    {
        return $this->hasMany('App\Team', 'tactic_id', 'id');
    }

}