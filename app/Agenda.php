<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Agenda extends Model
{
    protected $table = 'agenda';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;

}