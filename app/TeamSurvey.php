<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class TeamSurvey extends Model
{
    protected $table = 'team_surveys';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;

	public function team_potential()
    {
        return $this->hasOne('App\TeamPotential', 'id', 'potential_id');
    }

	public function team_satisfaction()
    {
        return $this->hasOne('App\TeamSatisfaction', 'id', 'satisfaction_id');
    }

	public function team()
    {
        return $this->hasOne('App\Team', 'id', 'team_id');
    }

}