<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class VideoGallery extends Model
{
    protected $table = 'video_galleries';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;

	//public.video_galleries.video_galleries_gallery_ptr_id_3e5932a9_fk_galleries_id
	public function gallery()
    {
        return $this->hasOne('App\Galleries', 'id', 'gallery_ptr_id');
    }

	//public.news.news_video_gallery_id_8cdad97b_fk_video_gal
	public function news()
    {
        return $this->belongsTo('App\News', 'gallery_ptr_id', 'video_gallery_id');
    }

	//public.video_gallery_items.video_gallery_items_gallery_id_ff22d6eb_fk_video_gal
    public function items()
    {
        return $this->hasMany('App\VideoGalleryItem', 'gallery_id', 'gallery_ptr_id');
    }

}