<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class TeamSatisfaction extends Model
{
    protected $table = 'team_satisfactions';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;

    public function team_survey()
    {
        return $this->belongsTo('App\TeamSurvey', 'id', 'satisfaction_id');
    }

}