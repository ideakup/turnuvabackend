<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class PasswordReset extends Model
{
    protected $table = 'password_resets';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;

    protected $fillable = [
        'email', 'token'
    ];
}
