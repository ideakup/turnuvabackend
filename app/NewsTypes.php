<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class NewsTypes extends Model
{
    protected $table = 'news_types';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;

	public function news_type()
    {
        return $this->belongsTo('App\News', 'type_id', 'id');
    }

}
