<?php
 
namespace App\Classes;

use Auth;
use Carbon\Carbon;

class FormaStarClass {
	
    protected $apiUrl;

    public function __construct() {
        $this->apiUrl = env('FS_API_URL');
    }

    public function addLeague($leagueId, $name, $minPlayer, $maxPlayer, $cityId, $profileUrl, $redCardBlockTime) {
        
        $data = array(
            "leagueId" => $leagueId,
            "name" => $name,
            "minPlayer" => $minPlayer,
            "maxPlayer" => $maxPlayer,
            "cityId" => $cityId,
            "profileUrl" => $profileUrl,
            "redCardBlockTime" => $redCardBlockTime
        );

        $result = $this->sendRequest('league', json_encode($data), 'POST');
        if($result['status'] == 200){
            return array('status' => 'success', 'result' => $result);
        }else{
            return array('status' => 'error', 'result' => $result);
        }
    }

    public function updateLeague($leagueId, $name, $minPlayer, $maxPlayer, $cityId, $profileUrl, $redCardBlockTime) {

        $data = array(
            "leagueId" => $leagueId,
            "name" => $name,
            "minPlayer" => $minPlayer,
            "maxPlayer" => $maxPlayer,
            "cityId" => $cityId,
            "profileUrl" => $profileUrl,
            "redCardBlockTime" => $redCardBlockTime
        );

        $result = $this->sendRequest('league', json_encode($data), 'PUT');
        if($result['status'] == 200){
            return array('status' => 'success', 'result' => $result);
        }else{
            return array('status' => 'error', 'result' => $result);
        }
    }

    public function addSeason($leagueId, $cityId, $name, $year, $type, $seasonId) {
        
        $data = array(
            "leagueId" => $leagueId,
            "cityId" => $cityId,
            "name" => $name,
            "year" => $year,
            "type" => $type,
            "seasonId" => $seasonId
        );

        $result = $this->sendRequest('season', json_encode($data), 'POST');
        if($result['status'] == 200){
            return array('status' => 'success', 'result' => $result);
        }else{
            return array('status' => 'error', 'result' => $result);
        }
    }

    public function updateSeason($leagueId, $cityId, $name, $year, $type, $seasonId) {

        $data = array(
            "leagueId" => $leagueId,
            "cityId" => $cityId,
            "name" => $name,
            "year" => $year,
            "type" => $type,
            "seasonId" => $seasonId
        );

        $result = $this->sendRequest('season', json_encode($data), 'PUT');
        if($result['status'] == 200){
            return array('status' => 'success', 'result' => $result);
        }else{
            return array('status' => 'error', 'result' => $result);
        }
    }

    public function addGroup($leagueId, $seasonId, $name, $groupId) {
        
        $data = array(
            "leagueId" => $leagueId,
            "seasonId" => $seasonId,
            "name" => $name,
            "groupId" => $groupId
        );

        $result = $this->sendRequest('group', json_encode($data), 'POST');
        if($result['status'] == 200){
            return array('status' => 'success', 'result' => $result);
        }else{
            return array('status' => 'error', 'result' => $result);
        }
    }

    public function updateGroup($leagueId, $seasonId, $name, $groupId) {

        $data = array(
            "leagueId" => $leagueId,
            "seasonId" => $seasonId,
            "name" => $name,
            "groupId" => $groupId
        );

        $result = $this->sendRequest('group', json_encode($data), 'PUT');
        if($result['status'] == 200){
            return array('status' => 'success', 'result' => $result);
        }else{
            return array('status' => 'error', 'result' => $result);
        }
    }

    public function addTeam($leagueId, $groupId, $seasonId, $name, $cityId, $teamId, $profileUrl, $userId, $email, $mobile) {
        
        $data = array(
            "leagueId" => $leagueId,
            "groupId" => $groupId,
            "seasonId" => $seasonId,
            "name" => $name,
            "cityId" => $cityId,
            "teamId" => $teamId,
            //"profileUrl" => $profileUrl,
            "userId" => $userId,
            //"email" => $email,
            "mobile" => $mobile
        );

        $result = $this->sendRequest('team', json_encode($data), 'POST');
        if($result['status'] == 200){
            return array('status' => 'success', 'result' => $result);
        }else{
            return array('status' => 'error', 'result' => $result);
        }
    }

    public function updateTeam($leagueId, $groupId, $seasonId, $name, $cityId, $teamId, $profileUrl, $userId, $email, $mobile) {

        $data = array(
            "leagueId" => $leagueId,
            "groupId" => $groupId,
            "seasonId" => $seasonId,
            "name" => $name,
            "cityId" => $cityId,
            "teamId" => $teamId,
            //"profileUrl" => $profileUrl,
            "userId" => $userId,
            //"email" => $email,
            "mobile" => $mobile
        );

        $result = $this->sendRequest('team', json_encode($data), 'PUT');
        if($result['status'] == 200){
            return array('status' => 'success', 'result' => $result);
        }else{
            return array('status' => 'error', 'result' => $result);
        }
    }

    public function addPlayer($firstName, $lastName, $email, $mobile, $birthDate, $teamId, $profileUrl, $roleCode, $userId, $height, $weight, $formNumber) {
        
        $data = array(
            "firstName" => $firstName,
            "lastName" => $lastName,
            //"email" => $email,
            "mobile" => $mobile,
            "birthDate" => $birthDate,
            "teamId" => $teamId,
            //"profileUrl" => $profileUrl,
            "roleCode" => $roleCode,
            "userId" => $userId,
            "height" => $height,
            "weight" => $weight,
            "formNumber" => $formNumber
        );
        dump(json_encode($data));
        $result = $this->sendRequest('athlete', json_encode($data), 'POST');
        if($result['status'] == 200){
            return array('status' => 'success', 'result' => $result);
        }else{
            return array('status' => 'error', 'result' => $result);
        }
    }

    public function updatePlayer($firstName, $lastName, $email, $mobile, $birthDate, $teamId, $profileUrl, $roleCode, $userId, $height, $weight, $formNumber) {

        $data = array(
            "firstName" => $firstName,
            "lastName" => $lastName,
            //"email" => $email,
            "mobile" => $mobile,
            "birthDate" => $birthDate,
            "teamId" => $teamId,
            //"profileUrl" => $profileUrl,
            "roleCode" => $roleCode,
            "userId" => $userId,
            "height" => $height,
            "weight" => $weight,
            "formNumber" => $formNumber
        );

        $result = $this->sendRequest('athlete', json_encode($data), 'PUT');
        if($result['status'] == 200){
            return array('status' => 'success', 'result' => $result);
        }else{
            return array('status' => 'error', 'result' => $result);
        }
    }

    public function addFixture($leagueId, $seasonId, $groundId, $team1Id, $team2Id, $matchDate, $matchTime, $matchId) {
        
        $data = array(
            "leagueId" => $leagueId,
            "seasonId" => $seasonId,
            "groundId" => $groundId,
            "team1Id" => $team1Id,
            "team2Id" => $team2Id,
            "matchDate" => $matchDate,
            "matchTime" => $matchTime,
            "matchId" => $matchId
        );
        //dd(json_encode($data));
        $result = $this->sendRequest('fixture', json_encode($data), 'POST');
        if($result['status'] == 200){
            return array('status' => 'success', 'result' => $result);
        }else{
            return array('status' => 'error', 'result' => $result);
        }
    }

    public function updateFixture($leagueId, $seasonId, $groundId, $team1Id, $team2Id, $matchDate, $matchTime, $matchId) {

        $data = array(
            //"leagueId" => $leagueId,
            //"seasonId" => $seasonId,
            "groundId" => $groundId,
            "team1Id" => $team1Id,
            "team2Id" => $team2Id,
            "matchDate" => $matchDate,
            "matchTime" => $matchTime,
            "matchId" => $matchId
        );

        $result = $this->sendRequest('fixture', json_encode($data), 'PUT');
        if($result['status'] == 200){
            return array('status' => 'success', 'result' => $result);
        }else{
            return array('status' => 'error', 'result' => $result);
        }
    }

    public function getMatchData($leagueId, $matchId) {

        $data = array(
            "leagueId" => $leagueId,
            "matchId" => $matchId
        );

        $result = $this->sendRequest('leagues/'.$leagueId.'/match/'.$matchId, json_encode($data), 'GET');
        if($result['status'] == 200){
            return array('status' => 'success', 'result' => $result);
        }else{
            return array('status' => 'error', 'result' => $result);
        }
    }

    public function getMatchActionData($leagueId, $matchId) {

        $data = array(
            "leagueId" => $leagueId,
            "matchId" => $matchId
        );

        $result = $this->sendRequest('leagues/'.$leagueId.'/match/'.$matchId.'/actions', json_encode($data), 'GET');
        if($result['status'] == 200){
            return array('status' => 'success', 'result' => $result);
        }else{
            return array('status' => 'error', 'result' => $result);
        }
    }

    public function getMatchTeamData($leagueId, $matchId) {

        $data = array(
            "leagueId" => $leagueId,
            "matchId" => $matchId
        );

        $result = $this->sendRequest('leagues/'.$leagueId.'/match/'.$matchId.'/perf', json_encode($data), 'GET');
        if($result['status'] == 200){
            return array('status' => 'success', 'result' => $result);
        }else{
            return array('status' => 'error', 'result' => $result);
        }
    }






    public function sendRequest($param, $jsonData, $method) {
        //dump($jsonData);
        $curl = curl_init($this->apiUrl.$param);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type:application/json', 'x-api-key:ksah3twksh791powjd25sju3772jdjsh'
        ));
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $jsonData);

        $result = curl_exec($curl);
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        return array('status' => $httpcode, 'data' => $result);
    }


}
