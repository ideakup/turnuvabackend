<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Conference extends Model
{
    protected $table = 'conferences';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;

    public function province()
    {
        return $this->hasOne('App\Province', 'id', 'province_id');
    }

    public function conferences_district()
    {
        return $this->hasMany('App\ConferencesDistrict', 'conference_id', 'id');
    }

    //public.nostalgia.nostalgia_conference_id_0280bfcb_fk_conferences_id
    
}