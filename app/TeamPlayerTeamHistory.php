<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class TeamPlayerTeamHistory extends Model
{
    protected $table = 'team_playerteamhistory';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;

	//public.team_playerteamhistory.team_playerteamhistory_player_id_069e0282_fk_players_id
	public function player()
    {
        return $this->belongsTo('App\Player', 'player_id', 'id');
    }

	//public.team_playerteamhistory.team_playerteamhistory_season_id_ebf7dfff_fk_seasons_id
    public function season()
    {
        return $this->belongsTo('App\Season', 'season_id', 'id');
    }

	//public.team_playerteamhistory.team_playerteamhistory_team_id_278c78c5_fk_teams_id
	public function team()
    {
        return $this->belongsTo('App\Team', 'team_id', 'id');
    }

}