<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class LeagueFixtureSeason extends Model
{
    protected $table = 'league_fixtureseason';
    protected $primaryKey = 'season_ptr_id';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;


    public function season_ptr()
    {
        return $this->hasOne('App\Season', 'id', 'season_ptr_id');
    }

    public function groups()
    {
        return $this->hasMany('App\Group', 'season_id', 'season_ptr_id');
    }

}