<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class GroundProperty extends Model
{
    protected $table = 'ground_properties';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;
    
    public function grounds_properties()
    {
        return $this->belongsTo('App\GroundsProperty', 'id', 'property_id');
    }

}