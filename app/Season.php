<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Season extends Model
{
    protected $table = 'seasons';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;

    public function league()
    {
        return $this->belongsTo('App\League', 'league_id', 'id');
    }

    public function content_type()
    {
        return $this->hasOne('App\DjangoContentType', 'id', 'polymorphic_ctype_id');
    }

    public function championships()
    {
        return $this->hasMany('App\Championships', 'season_id', 'id');
    }

    public function league_elimination_baseseason()
    {
        return $this->hasOne('App\LeagueEliminationSeason', 'base_season_id', 'id');
    }

    public function league_eliminationseason()
    {
        return $this->hasOne('App\LeagueEliminationSeason', 'season_ptr_id', 'id');
    }

    public function league_eliminationseasonApi()
    {
        return $this->hasOne('App\LeagueEliminationSeason', 'base_season_id', 'id');
    }

    //public.league_fixtureseason.league_fixtureseason_season_ptr_id_727b11b0_fk_seasons_id
    public function league_fixtureseason()
    {
        return $this->hasOne('App\LeagueFixtureSeason', 'season_ptr_id', 'id');
    }

	//public.league_photo_gallery.league_photo_gallery_season_id_35b24c86_fk_seasons_id

    public function league_pointseason()
    {
        return $this->hasOne('App\LeaguePointSeason', 'season_ptr_id', 'id');
    }

    public function match()
    {
        return $this->hasMany('App\Match', 'season_id', 'id');
    }


	//public.nostalgia.nostalgia_season_id_85e7c0c1_fk_seasons_id


	//public.panorama_types.panorama_types_season_id_ec51f1a7_fk_seasons_id


	//public.penal_types.panorama_penaltype_season_id_789b312b_fk_seasons_id


	//public.penalties.panorama_penal_season_id_471b75c6_fk_seasons_id
    public function penalties()
    {
        return $this->hasMany('App\Penalty', 'season_id', 'id');
    }

	//public.player_values.player_values_season_id_44f1a140_fk_seasons_id
    public function player_value()
    {
        return $this->hasMany('App\PlayerValue', 'season_id', 'id');
    }

	//public.team_playerteamhistory.team_playerteamhistory_season_id_ebf7dfff_fk_seasons_id
    public function team_playerteamhistory()
    {
        return $this->hasMany('App\TeamPlayerTeamHistory', 'season_id', 'id');
    }

	//public.team_teamseason.team_teamseason_season_id_8269f079_fk_seasons_id
    public function team_teamseason()
    {
        return $this->hasMany('App\TeamTeamSeason', 'season_id', 'id');
    }

    public function team_teamseasonApi()
    {
        return $this->hasMany('App\TeamTeamSeason', 'season_id', 'id')->limit(10);
    }

	//public.transfers_seasons.transfers_seasons_season_id_b905c643_fk_seasons_id

}