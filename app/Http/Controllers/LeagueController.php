<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Validator;

use Carbon\Carbon;
use App\User;
use App\Classes\FormaStarClass;

use App\League;
use App\LeaguesProvince;

class LeagueController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	return view('dashboard');
    }

    public function crud($id = null)
    {
        $league = NULL;
        if(!is_null($id)){
            $league = League::find($id);
        }
        return view('dashboard', array('model_data' => $league));
    }

    public function save(Request $request)
    {
        //dd($request->input());
        $text = "";
        if ($request->crud == 'delete') {

            $model = League::find($request->id);

            foreach ($model->league_province as $value) {
                $value->delete();
            }

            $model->delete();
            $text = 'Başarıyla Silindi...';

        }else{

            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $model = new League();
            }else if($request->crud == 'edit'){
                $model = League::find($request->id);
            }

            $rules = array();
            $formConfig = config('forms.'.$request->segment);

            if(!is_null($formConfig)){
                foreach ($formConfig as $key => $value){
                    if(!empty(data_get($value, 'validation'))){
                        $rules[$key] = data_get($value, 'validation');
                    }
                }
            }
            
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
               return \Redirect::back()->withErrors($validator)->withInput();
            }

            $model->name = $request->name;
            $model->facebook = $request->facebook;
            $model->twitter = $request->twitter;
            $model->instagram = $request->instagram;
            $model->active = (!empty($request->active)) ? true : false;
            $model->save();

             /* EDIT */
            if($request->crud == 'edit'){
                foreach ($model->league_province as $value_lp) {
                    $value_lp->delete();
                }
            }

            if(!empty($request->province)){
                foreach ($request->province as $value_lp) {
                    if(!empty($value_lp)){
                        $lp = new LeaguesProvince();
                        $lp->league_id = $model->id;
                        $lp->province_id = $value_lp;
                        $lp->save();
                    }
                }
            }



            //dump($model);
            //dump($model->league_province);
            //dump($request->province);

            if(!empty(env('FS_API_KEY'))){
                $formaStar = new FormaStarClass();

                $leagueId = $model->id;
                $name = $model->name;
                $minPlayer = 7;
                $maxPlayer = 15;

                if(!empty($model->league_province->first())){
                    $cityId = $model->league_province->first()->province_id;
                }else{
                    $cityId = 34; //Default Şehir İstanbul
                }

                $activeSeason = 0;
                $redCardBlockTime = 300;
                $profileUrl = null;

                $fs_return = array();
                $fs_return = $formaStar->addLeague($leagueId, $name, $minPlayer, $maxPlayer, $cityId, $profileUrl, $redCardBlockTime);

                //dump('Lig > Ekle');
                //dump($fs_return);
                $text .= 'Lig > Ekle '.json_encode($fs_return).' --- ';

                if(!empty(json_decode($fs_return['result']['status']))){
                    if(json_decode($fs_return['result']['status']) != 200){
                        //dump(json_decode($fs_return['result']['data'])->errorCode);
                        if(json_decode($fs_return['result']['data'])->errorCode == 1001){ //Gönderilen kod ile kayıtlı bir takım mevcut
                            $fs_return = $formaStar->updateLeague($leagueId, $name, $minPlayer, $maxPlayer, $cityId, $profileUrl, $redCardBlockTime);
                            //dump('Lig > Güncelle');
                            //dump($fs_return);
                            $text .= 'Lig > Güncelle '.json_encode($fs_return).' --- ';
                        }
                    }
                }
            }

            //dd('fs');

            /* ADD - EDIT 
            if ($request->crud == 'add') {
                $text = 'Başarıyla Eklendi...';
            }else if($request->crud == 'edit'){
                $text = 'Başarıyla Güncellendi...';
            }*/
        }

        return redirect($request->segment)->with('message', array('text' => $text, 'status' => 'success'));
    }

}