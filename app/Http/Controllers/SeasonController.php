<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Validator;

use Carbon\Carbon;
use App\User;
use App\Classes\FormaStarClass;

use App\Season;
use App\LeaguePointSeason;
use App\LeagueFixtureSeason;
use App\LeagueEliminationSeason;

use App\TeamTeamSeasonAddPointLog;

class SeasonController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
    	return view('dashboard');
    }

    public function addBonus() {
        return view('dashboard');
    }

    public function addBonusSave(Request $request) {

        $seasonIds = $request->input('seasonid');
        $recordDate = Carbon::now()->subMonth();

        if(!empty($seasonIds)){

            foreach ($seasonIds as $seasonId) {
                
                $season = Season::find($seasonId);

                //dump($season);
                //dump($recordDate->startOfMonth()->format('Y-m-d').' - '.$recordDate->endOfMonth()->format('Y-m-d'));

                if(!empty($season->league_pointseason)){

                    $bonus_match_count = null;
                    $bonus_point = null;

                    if($season->league_pointseason->has_bonus){
                        $bonus_match_count = $season->league_pointseason->bonus_match_count;
                        $bonus_point = $season->league_pointseason->bonus_point;

                        //dump('Gerçek - bonus_match_count: '.$bonus_match_count);
                        //dump('Gerçek - bonus_point: '.$bonus_point);
                    }
                    
                    //Test için manuple satırı...
                    //$bonus_match_count = 1;
                    //$bonus_point = 10;
                    //dump('Temp - bonus_match_count: '.$bonus_match_count);
                    //dump('Temp - bonus_point: '.$bonus_point);
                    
                    foreach ($season->team_teamseason->where('match_total', '>=', $bonus_match_count) as $teamseason) {
                        $team = $teamseason->team;
                        //dump('Takım: '.$team->name);
                        //dump($teamseason);

                        $matches1 = collect($team->match1)->where('completed', true)->where('date', '>=', $recordDate->startOfMonth()->format('Y-m-d'))->where('date', '<=', $recordDate->endOfMonth()->format('Y-m-d'));
                        $matches2 = collect($team->match2)->where('completed', true)->where('date', '>=', $recordDate->startOfMonth()->format('Y-m-d'))->where('date', '<=', $recordDate->endOfMonth()->format('Y-m-d'));
                        $matches = $matches1->merge($matches2)->sortBy('completed_at');

                        if($matches->count() >= $bonus_match_count){
                            //dump('Maç Sayısı: '.$matches->count());
                            //dump('Puan Ekle: '.$bonus_point);

                            //dump(json_decode($teamseason->teamseason_add_point_log));
                            $dahaOnceEklenmis = false;
                            foreach ($teamseason->teamseason_add_point_log as $log) {

                                if($log->point_type == 'Monthly Bonus' && json_decode($log->meta)->year == $recordDate->format('Y') && json_decode($log->meta)->month == $recordDate->format('n')){
                                    //dump('daha önce eklenmiş');
                                    $dahaOnceEklenmis = true;
                                }

                            }

                            
                            if(!$dahaOnceEklenmis){
                                //dump('ekleeee');

                                $newPoint = $teamseason->point + $bonus_point;

                                $teamseason->point = $newPoint;
                                $teamseason->save();

                                $pointLog = new TeamTeamSeasonAddPointLog();
                                $pointLog->point_add = $bonus_point;
                                $pointLog->point_new = $newPoint;
                                $pointLog->teamseason_id = $teamseason->id;
                                $pointLog->user_id = Auth::user()->id;
                                $pointLog->meta = json_encode(array('year' => $recordDate->format('Y'), 'month' => $recordDate->format('n')));
                                $pointLog->point_type = 'Monthly Bonus';
                                $pointLog->save();
                            }

                        }

                    }

                }

            }

            $text = 'Bonus Puanlar Başarıyla Eklendi...';
            return redirect('seasons/addbonus')->with('message', array('text' => $text, 'status' => 'success'));

        }else{

            $text = 'Bir sezon seçmelisiniz...';
            return redirect('seasons/addbonus')->with('message', array('text' => $text, 'status' => 'warning'));

        }
    }

    public function crudp($id = null) {
        $season = NULL;
        if(!is_null($id)){
            $season = Season::find($id);
        }
        return view('dashboard', array('model_data' => $season));
    }

    public function savep(Request $request) {

        $text = "";
        
        if ($request->crud == 'delete') {

            $model = Season::find($request->id);
            $model->league_pointseason->delete();
            $model->delete();
            $text = 'Başarıyla Silindi...';

        }else{

            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $model = new Season();
            }else if($request->crud == 'edit'){
                $model = Season::find($request->id);
            }

            $rules = array();
            $formConfig = config('forms.'.$request->segment);
            
            if(!is_null($formConfig)){
                foreach ($formConfig as $key => $value){
                    if(!empty(data_get($value, 'validation'))){
                        $rules[$key] = data_get($value, 'validation');
                    }
                }
            }
            
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                return \Redirect::back()->withErrors($validator)->withInput();
            }
            
            $model->polymorphic_ctype_id = $request->polymorphic_ctype_id;
            $model->closed = ($request->closed == 'false') ? false : true;
            $model->name = $request->name;
            $model->league_id = $request->league_id;
            $model->year = $request->year;
            $model->start_date = Carbon::parse($request->start_date);
            $model->end_date = Carbon::parse($request->end_date);
            $model->transfer_start_date = Carbon::parse($request->transfer_start_date);
            $model->transfer_end_date = Carbon::parse($request->transfer_end_date);
            $model->allowed_transfer_count = $request->allowed_transfer_count;
            $model->locking_match_count = $request->locking_match_count;
            $model->active = (!empty($request->active)) ? true : false;
            $model->save();

            if ($request->crud == 'add') {
                $subModel = new LeaguePointSeason();
                $subModel->season_ptr_id = $model->id;
            }else if($request->crud == 'edit'){
                $subModel = $model->league_pointseason;
            }

            $subModel->min_team_point = $request->min_team_point;
            $subModel->has_bonus = (!empty($request->has_bonus)) ? true : false;
            $subModel->bonus_match_count = $request->bonus_match_count;
            $subModel->bonus_point = $request->bonus_point;
            $subModel->save();
            
            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $text = 'Başarıyla Eklendi...';
            }else if($request->crud == 'edit'){
                $text = 'Başarıyla Güncellendi...';
            }
        }

        return redirect('seasons')->with('message', array('text' => $text, 'status' => 'success'));
    }


    public function crudf($id = null) {
        $season = NULL;
        if(!is_null($id)){
            $season = Season::find($id);
        }
        return view('dashboard', array('model_data' => $season));
    }

    public function savef(Request $request) {

        $text = "";
        //dd($request->input());
        if ($request->crud == 'delete') {

            $model = Season::find($request->id);
            if(!empty($model->team_teamseason)){
                foreach ($model->team_teamseason as $teamseason) {
                    $teamseason->delete();
                }
            }

            if(!empty($model->league_fixtureseason)){
                foreach ($model->league_fixtureseason->groups as $group) {
                    foreach ($group->group_teams as $group_team) {
                        $group_team->delete();
                    }
                    $group->delete();
                }
                $model->league_fixtureseason->delete();
            }
            $model->delete();
            $text = 'Başarıyla Silindi...';

        }else{

            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $model = new Season();
            }else if($request->crud == 'edit'){
                $model = Season::find($request->id);
            }

            $rules = array();
            $formConfig = config('forms.'.$request->segment);
            if(!is_null($formConfig)){
                foreach ($formConfig as $key => $value){
                    if(!empty(data_get($value, 'validation'))){
                        $rules[$key] = data_get($value, 'validation');
                    }
                }
            }
            
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
               return \Redirect::back()->withErrors($validator)->withInput();
            }

            $model->polymorphic_ctype_id = $request->polymorphic_ctype_id;
            $model->closed = ($request->closed == 'false') ? false : true;
            $model->name = $request->name;
            $model->league_id = $request->league_id;
            $model->year = $request->year;
            $model->start_date = Carbon::parse($request->start_date);
            $model->end_date = Carbon::parse($request->end_date);
            //$model->transfer_start_date = Carbon::parse($request->transfer_start_date);
            //$model->transfer_end_date = Carbon::parse($request->transfer_end_date);

            if(!empty($request->allowed_transfer_count)){
                $model->allowed_transfer_count = $request->allowed_transfer_count;
            }else{
                $model->allowed_transfer_count = 0;
            }

            if(!empty($request->locking_match_count)){
                $model->locking_match_count = $request->locking_match_count;
            }else{
                $model->locking_match_count = 0;
            }

            $model->active = (!empty($request->active)) ? true : false;
            if(!empty(env('FS_API_KEY'))){
                $model->fs_api = (!empty($request->fs_api)) ? true : false;
            }
            $model->save();

            if ($request->crud == 'add') {
                $subModel = new LeagueFixtureSeason();
                $subModel->season_ptr_id = $model->id;
            }else if($request->crud == 'edit'){
                $subModel = $model->league_fixtureseason;
            }

            $subModel->min_team_point = 0;
            $subModel->save();

            $season_count = Season::where('year', $model->year)->where('league_id', $model->league_id)->where('fs_api', true)->count();

            //dump($season_count);
            //dump($model);
            //dump($subModel);

            if(!empty(env('FS_API_KEY'))){
                $formaStar = new FormaStarClass();

                $leagueId = $model->league->id; //103;

                if(!empty($model->league->league_province->first())){
                    $cityId = $model->league->league_province->first()->province_id;
                }else{
                    $cityId = 34; //Default Şehir İstanbul
                }

                $name = $model->name; //"Sezon Bahar";
                $year = $model->year; //2023;
                $type = $season_count;
                $seasonId = $model->id; //3001;

                $fs_return = array();
                $fs_return = $formaStar->addSeason($leagueId, $cityId, $name, $year, $type, $seasonId);

                // dump('Sezon > Ekle');
                // dump($fs_return);
                $text .= 'Sezon > Ekle '.json_encode($fs_return).' --- ';

                if(!empty(json_decode($fs_return['result']['status']))){
                    if(json_decode($fs_return['result']['status']) != 200){
                        // dump(json_decode($fs_return['result']['data'])->errorCode);
                        if(json_decode($fs_return['result']['data'])->errorCode == 1003){ //Gönderilen kod ile kayıtlı bir takım mevcut
                            $fs_return = $formaStar->updateSeason($leagueId, $cityId, $name, $year, $type, $seasonId);
                            // dump('Sezon > Güncelle');
                            // dump($fs_return);
                            $text .= 'Sezon > Güncelle '.json_encode($fs_return).' --- ';
                        }
                    }
                }

            }

            //dd('fs');

            /* ADD - EDIT 
            if ($request->crud == 'add') {
                $text = 'Başarıyla Eklendi... ('.json_encode($fs_return).')';
            }else if($request->crud == 'edit'){
                $text = 'Başarıyla Güncellendi... ('.json_encode($fs_return).')';
            }*/
        }

        return redirect('seasons')->with('message', array('text' => $text, 'status' => 'success'));
    }
    
    public function crude($id = null) {
        $season = NULL;
        if(!is_null($id)){
            $season = Season::find($id);
        }
        return view('dashboard', array('model_data' => $season));
    }

    public function savee(Request $request) {

        $text = "";
        // dd($request->input());
        if ($request->crud == 'delete') {

            $model = Season::find($request->id);
            //$model->league_fixtureseason->delete();
            $model->delete();
            $text = 'Başarıyla Silindi...';

        }else{

            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $model = new Season();
            }else if($request->crud == 'edit'){
                $model = Season::find($request->id);
            }

            $rules = array();
            $formConfig = config('forms.'.$request->segment);
            if(!is_null($formConfig)){
                foreach ($formConfig as $key => $value){
                    if(!empty(data_get($value, 'validation'))){
                        $rules[$key] = data_get($value, 'validation');
                    }
                }
            }
            
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
               return \Redirect::back()->withErrors($validator)->withInput();
            }



            if ($request->crud == 'add') {

                $contModel = LeagueEliminationSeason::where('base_season_id', $request->season_id)->count();
                if($contModel != 0){
                    return \Redirect::back()->with('message', array('text' => 'Seçilen Ana Sezon Bir Elemeli Sezona Sahiptir...', 'status' => 'error'))->withInput();
                }

            }else if($request->crud == 'edit'){

                $contModel = LeagueEliminationSeason::where('season_ptr_id', "!=", $request->id)->where('base_season_id', $request->season_id)->count();
                if($contModel != 0){
                    return \Redirect::back()->with('message', array('text' => 'Seçilen Ana Sezon Bir Elemeli Sezona Sahiptir...', 'status' => 'error'))->withInput();
                }
            }
            


            $model->polymorphic_ctype_id = $request->polymorphic_ctype_id;
            $model->closed = ($request->closed == 'false') ? false : true;
            $model->name = $request->name;
            $model->league_id = $request->league_id;
            $model->year = $request->year;
            $model->start_date = Carbon::parse($request->start_date);
            $model->end_date = Carbon::parse($request->end_date);
            $model->transfer_start_date = Carbon::parse($request->transfer_start_date);
            $model->transfer_end_date = Carbon::parse($request->transfer_end_date);
            $model->allowed_transfer_count = (!empty($request->allowed_transfer_count)) ? $request->allowed_transfer_count : 0;
            $model->locking_match_count = (!empty($request->locking_match_count)) ? $request->locking_match_count : 0;;
            $model->active = (!empty($request->active)) ? true : false;
            if(!empty(env('FS_API_KEY'))){
                $model->fs_api = (!empty($request->fs_api)) ? true : false;
            }
            $model->save();

            if ($request->crud == 'add') {
                $subModel = new LeagueEliminationSeason();
                $subModel->season_ptr_id = $model->id;
                $subModel->base_season_id = $request->season_id;
            }else if($request->crud == 'edit'){
                $subModel = $model->league_eliminationseason;
            }

            $subModel->save();
            
            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $text = 'Başarıyla Eklendi...';
            }else if($request->crud == 'edit'){
                $text = 'Başarıyla Güncellendi...';
            }
        }

        return redirect('seasons')->with('message', array('text' => $text, 'status' => 'success'));
    }

    
}
