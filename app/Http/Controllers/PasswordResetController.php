<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Notifications\PasswordResetRequest;
use App\Notifications\PasswordResetSuccess;
use App\User;
use App\PasswordReset;

class PasswordResetController extends Controller
{

    public function create(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
        ]);
        $user = User::where('email', $request->email)->first();
        if (!$user)
            return response()->json([
                'message' => "We can't find a user with that e-mail address."
            ], 422);
        $passwordReset = PasswordReset::updateOrCreate(
            ['email' => $user->email],
            [
	            'email' => $user->email,
	            'token' => str_random(60)
            ]
        );

        if ($user && $passwordReset)
            $user->notify(
                new PasswordResetRequest($passwordReset->token, $user)
            );
        return response()->json([
            'status' => 'OK'
        ],200);
    }

    public function changeMailCreate(Request $request)
    {

        /*
            $request->validate([
                'email' => 'required|string|email',
            ]);
        */

        $birth_date = $request->input('birth_date');
        $identity_number = $request->input('identity_number');

        $user = User::select('profiles.birth_date', 'profiles.identity_number', 'users.*')->leftJoin('profiles', 'users.id', '=', 'profiles.user_id')->
        where('profiles.birth_date', $birth_date)->
        where('profiles.identity_number', $identity_number)->first();

        $user->username = $request->input('email');
        $user->email = $request->input('email');
        $user->email_confirmed = false;
        $user->save();

        if (!$user)
            return response()->json(['message' => "We can't find a user with that e-mail address."], 422);
            
        $passwordReset = PasswordReset::updateOrCreate(
            ['email' => $user->email],
            [
                'email' => $user->email,
                'token' => str_random(60)
            ]
        );

        if ($user && $passwordReset)
            $user->notify(
                new PasswordResetRequest($passwordReset->token, $user)
            );
        return response()->json([
            'status' => 'OK'
        ],200);
    }

    public function find(Request $request)
    {
        $passwordReset = PasswordReset::where('token', $request->token)->first();
        if (!$passwordReset)
            return response()->json([
                'message' => 'This password reset token is invalid.'
            ], 404);
        if (Carbon::parse($passwordReset->updated_at)->addMinutes(720)->isPast()) {
            $passwordReset->delete();
            return response()->json([
                'message' => 'This password reset token is invalid.'
            ], 404);
        }
        return response()->json($passwordReset->token);
    }

    public function reset(Request $request)
    {
        $passwordReset = PasswordReset::where('token', $request->input('token'))->first();
        if (!$passwordReset)
            return response()->json(['message' => 'This password reset token is invalid.'], 404);
        $user = User::where('email', $passwordReset->email)->first();
        if (!$user)
            return response()->json(['message' => "We can't find a user with that e-mail address."], 404);
        $user->password = $this->django_password_create($request->input('password'));
        $user->email_confirmed = true;
        $user->save();
        $passwordReset->delete();
        $user->notify(new PasswordResetSuccess($passwordReset, $user));
        return response()->json([
            'status' => 'OK'
        ],200);
    }

    public function changePassword(Request $request)
    {
        $user = Auth::user();
        $currentPassword = $request->input('currentPassword');
        $password = $request->input('password');
        $passwordRepeat = $request->input('passwordRepeat');

        if(strlen($request->input('password')) < 5){
            return response(array('message' => 'Şifre en az 5 karakter olmalıdır'), 401);
        }

        $pv = $this->django_password_verify(
            $currentPassword,
            $user->password
        );

        if(!$pv){
            return response(array('message' => 'Geçerli parolayı yanlış girdiniz'), 401);
        }
        
        $pv2 = $this->django_password_create($password);

        $user->password = $pv2;
        $user->save();

        return response(array('message' => 'Tamam'), 200);
    }

    public function django_password_create(string $password)
    {
        $algorithm = "pbkdf2_sha256";
        $iterations = 150000;

        $salt = base64_encode(openssl_random_pseudo_bytes(9));
        //$salt = base64_encode($salt);

        $hash = hash_pbkdf2("SHA256", $password, $salt, $iterations, 0, true);    
        $toDBStr = $algorithm ."$". $iterations ."$". $salt ."$". base64_encode($hash);

        return $toDBStr;
    }

	public function django_password_verify(string $password, string $djangoHash): bool
    {
        $pieces = explode('$', $djangoHash);
        if (count($pieces) !== 4) {
            throw new Exception("Illegal hash format");
        }
        list($header, $iter, $salt, $hash) = $pieces;
        // Get the hash algorithm used:
        if (preg_match('#^pbkdf2_([a-z0-9A-Z]+)$#', $header, $m)) {
            $algo = $m[1];
        } else {
            throw new Exception(sprintf("Bad header (%s)", $header));
        }
        if (!in_array($algo, hash_algos())) {
            throw new Exception(sprintf("Illegal hash algorithm (%s)", $algo));
        }

        $calc = hash_pbkdf2(
            $algo,
            $password,
            $salt,
            (int) $iter,
            32,
            true
        );

        return hash_equals($calc, base64_decode($hash));
    }

}