<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Validator;

use Carbon\Carbon;
use App\User;

use App\District;

class DistrictController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	return view('dashboard');
    }

    public function crud($id = null)
    {
        $district = NULL;
        if(!is_null($id)){
            $district = District::find($id);
        }
        return view('dashboard', array('model_data' => $district));
    }

    public function save(Request $request)
    {
        //dd($request->input());
        $text = "";
        if ($request->crud == 'delete') {

            $model = District::find($request->id);

            if($model->conferences_district->count() == 0 && $model->ground->count() == 0 && $model->profile->count() == 0 && $model->team->count() == 0){
                $model->delete();
                $text = 'Başarıyla Silindi...';
            }else{
                $text = 'Bu ilçe en az bir konferans, halı saha, profil veya takıma atandığı için silinemedi!';
                return \Redirect::back()->withInput()->with('message', array('text' => $text, 'status' => 'error'));
            }

        }else{

            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $model = new District();
            }else if($request->crud == 'edit'){
                $model = District::find($request->id);
            }

            $rules = array();
            $formConfig = config('forms.'.$request->segment);

            if(!is_null($formConfig)){
                foreach ($formConfig as $key => $value){
                    if(!empty(data_get($value, 'validation'))){
                        $rules[$key] = data_get($value, 'validation');
                    }
                }
            }
            
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
               return \Redirect::back()->withErrors($validator)->withInput();
            }

            $slugControl = District::where('slug', str_slug($request->name, '-'))->count();

            $model->name = $request->name;
            if($slugControl == 0){
                $model->slug = str_slug($request->name, '-');
            }else{
                $model->slug = str_slug($request->name, '-').'-'.str_random(3);
            }
            $model->province_id = $request->province_id;
            $model->save();

            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $text = 'Başarıyla Eklendi...';
            }else if($request->crud == 'edit'){
                $text = 'Başarıyla Güncellendi...';
            }
        }

        return redirect($request->segment)->with('message', array('text' => $text, 'status' => 'success'));
    }

}