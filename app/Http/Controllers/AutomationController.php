<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Validator;

use Carbon\Carbon;
use App\Classes\FormaStarClass;

use App\Agenda;
use App\Championships;
use App\Conference;
use App\Coordinator;
use App\District;
use App\EliminationTree;
use App\EliminationTreeItem;
use App\Galleries;
use App\Ground;
use App\GroundProperty;
use App\GroundImage;
use App\Group;
use App\LeagueGalleries;
use App\LeagueGalleryItems;
use App\League;
use App\MatchAction;
use App\MatchImage;
use App\MatchPanorama;
use App\MatchPlayer;
use App\MatchVideo;
use App\Match;
use App\News;
use App\NewsTypes;
use App\Nostalgia;
use App\PanoramaType;
use App\PenaltyType;
use App\Penalty;
use App\PhotoGalleryItem;
use App\PlayerPosition;
use App\PlayerValue;
use App\Player;
use App\PointType;
use App\PressConferences;
use App\Profile;
use App\Province;
use App\Referee;
use App\ReservationGroundsubscription;
use App\ReservationGroundtable;
use App\ReservationOffer;
use App\Reservation;
use App\Season;
use App\Tactic;
use App\Tag;
use App\TeamPlayerTeamHistory;
use App\TeamPotential;
use App\TeamSatisfaction;
use App\TeamSurvey;
use App\TeamTeamPlayer;
use App\TeamTeamSeason;
use App\TeamTeamSeasonAddPointLog;
use App\Team;
use App\Transfer;
use App\User;
use App\VideoGalleryItem;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class AutomationController extends Controller {

    public function index() {
        phpinfo();
    }

    public function addPermission($permName) {
        if($permName == 'branch'){
            $permission = Permission::create(['name' => 'view_branch']);
            $permission = Permission::create(['name' => 'add_branch']);
            $permission = Permission::create(['name' => 'change_branch']);
            $permission = Permission::create(['name' => 'delete_branch']);
        }
    }

    public function checkDate($param) {

        $listArray = array('smalls_1', 'smalls_2', 'smalls_3', 'smalls_4', 'gallery_1', 'gallery_2', 'gallery_3', 'match_action_1', 'match_action_2', 'match_action_3', 'match_action_4', 'match_action_5', 'match_action_6', 'match_action_7', 'match_action_8', 'match_action_9', 'match_action_10', 'match_action_11', 'match_action_12', 'match_action_13', 'match_action_14', 'match_action_15', 'match_action_16', 'match_action_17', 'match_action_18', 'match_action_19', 'match_action_20', 'match_action_21', 'match_action_22', 'match_action_23', 'match_action_24', 'match_action_25', 'match_action_26', 'match_action_27', 'match_action_28', 'match_action_29', 'match_action_30', 'match_action_31', 'match_action_32', 'match_action_33', 'match_action_34', 'match_action_35', 'match_action_36', 'match_action_37', 'match_action_38', 'match_action_39', 'match_action_40', 'match_action_41', 'match_action_42', 'match_action_43', 'match_action_44', 'match_action_45', 'match_action_46', 'match_action_47', 'match_action_48', 'match_action_49', 'match_action_50', 'match_image_1', 'match_image_2', 'match_image_3', 'match_image_4', 'match_image_5', 'match_image_6', 'match_image_7', 'match_image_8', 'match_image_9', 'match_image_10', 'match_image_11', 'match_image_12', 'match_image_13', 'match_image_14', 'match_image_15', 'match_image_16', 'match_image_17', 'match_image_18', 'match_image_19', 'match_image_20', 'match_image_21', 'match_image_22', 'match_image_23', 'match_image_24', 'match_image_25', 'match_image_26', 'match_image_27', 'match_image_28', 'match_image_29', 'match_image_30', 'match_image_31', 'match_image_32', 'match_image_33', 'match_image_34', 'match_image_35', 'match_image_36', 'match_image_37', 'match_image_38', 'match_image_39', 'match_image_40', 'match_image_41', 'match_image_42', 'match_image_43', 'match_image_44', 'match_image_45', 'match_image_46', 'match_image_47', 'match_image_48', 'match_image_49', 'match_image_50', 'match_image_51', 'match_image_52', 'match_image_53', 'match_image_54', 'match_image_55', 'match_image_56', 'match_image_57', 'match_image_58', 'match_image_59', 'match_image_60', 'match_image_61', 'match_image_62', 'match_image_63', 'match_image_64', 'match_image_65', 'match_image_66', 'match_image_67', 'match_image_68', 'match_image_69', 'match_image_70', 'match_image_71', 'match_image_72', 'match_image_73', 'match_image_74', 'match_image_75', 'match_image_76', 'match_image_77', 'match_image_78', 'match_image_79', 'match_image_80', 'match_image_81', 'match_image_82', 'match_image_83', 'match_image_84', 'match_image_85', 'match_image_86', 'match_image_87', 'match_image_88', 'match_image_89', 'match_image_90', 'match_image_91', 'match_image_92', 'match_image_93', 'match_image_94', 'match_image_95', 'match_image_96', 'match_image_97', 'match_image_98', 'match_image_99', 'match_image_100', 'match_panorama_1', 'match_panorama_2', 'match_panorama_3', 'match_panorama_4', 'match_player_1', 'match_player_2', 'match_player_3', 'match_player_4', 'match_player_5', 'match_player_6', 'match_player_7', 'match_player_8', 'match_player_9', 'match_player_10', 'match_player_11', 'match_player_12', 'match_player_13', 'match_player_14', 'match_player_15', 'match_player_16', 'match_player_17', 'match_player_18', 'match_player_19', 'match_player_20', 'match_player_21', 'match_player_22', 'match_player_23', 'match_player_24', 'match_player_25', 'match_player_26', 'match_player_27', 'match_player_28', 'match_player_29', 'match_player_30', 'match_player_31', 'match_player_32', 'match_player_33', 'match_player_34', 'match_player_35', 'match_player_36', 'match_player_37', 'match_player_38', 'match_player_39', 'match_player_40', 'match_player_41', 'match_player_42', 'match_player_43', 'match_player_44', 'match_player_45', 'match_player_46', 'match_player_47', 'match_player_48', 'match_player_49', 'match_player_50', 'match_player_51', 'match_player_52', 'match_player_53', 'match_player_54', 'match_player_55', 'match_player_56', 'match_player_57', 'match_player_58', 'match_player_59', 'match_player_60', 'match_player_61', 'match_player_62', 'match_player_63', 'match_player_64', 'match_player_65', 'match_player_66', 'match_player_67', 'match_player_68', 'match_player_69', 'match_player_70', 'match_player_71', 'match_player_72', 'match_player_73', 'match_player_74', 'match_player_75', 'match_player_76', 'match_player_77', 'match_player_78', 'match_player_79', 'match_player_80', 'match_player_81', 'match_player_82', 'match_player_83', 'match_player_84', 'match_player_85', 'match_player_86', 'match_player_87', 'match_player_88', 'match_player_89', 'match_player_90', 'match_player_91', 'match_player_92', 'match_player_93', 'match_player_94', 'match_player_95', 'match_player_96', 'match_player_97', 'match_player_98', 'match_player_99', 'match_player_100', 'match_player_101', 'match_player_102', 'match_player_103', 'match_player_104', 'match_player_105', 'match_player_106', 'match_player_107', 'match_player_108', 'match_player_109', 'match_player_110', 'match_player_111', 'match_player_112', 'match_player_113', 'match_player_114', 'match_player_115', 'match_player_116', 'match_player_117', 'match_player_118', 'match_player_119', 'match_player_120', 'match_player_121', 'match_player_122', 'match_player_123', 'match_player_124', 'match_player_125', 'match_player_126', 'match_player_127', 'match_player_128', 'match_player_129', 'match_player_130', 'match_player_131', 'match_player_132', 'match_player_133', 'match_player_134', 'match_player_135', 'match_player_136', 'match_player_137', 'match_player_138', 'match_player_139', 'match_video_1', 'match_video_2', 'match_video_3', 'match_video_4', 'match_video_5', 'match_video_6', 'match_video_7', 'match_video_8', 'match_video_9', 'match_video_10', 'match_video_11', 'match_1', 'match_2', 'match_3', 'match_4', 'match_5', 'match_6', 'match_7', 'match_8', 'match_9', 'match_10', 'match_11', 'new_1', 'new_2', 'new_3', 'photogalleryitem_1', 'photogalleryitem_2', 'photogalleryitem_3', 'playervalue_1', 'playervalue_2', 'playervalue_3', 'playervalue_4', 'playervalue_5', 'playervalue_6', 'playervalue_7', 'playervalue_8', 'playervalue_9', 'playervalue_10', 'playervalue_11', 'playervalue_12', 'playervalue_13', 'playervalue_14', 'player_1', 'player_2', 'player_3', 'player_4', 'player_5', 'player_6', 'player_7', 'player_8', 'player_9', 'player_10', 'player_11', 'player_12', 'player_13', 'player_14', 'player_15', 'player_16', 'player_17', 'player_18', 'player_19', 'player_20', 'player_21', 'player_22', 'pressconferences_1', 'pressconferences_2', 'pressconferences_3', 'pressconferences_4', 'pressconferences_5', 'pressconferences_6', 'pressconferences_7', 'pressconferences_8', 'profile_1', 'profile_2', 'profile_3', 'profile_4', 'profile_5', 'profile_6', 'profile_7', 'profile_8', 'profile_9', 'profile_10', 'profile_11', 'profile_12', 'profile_13', 'profile_14', 'profile_15', 'profile_16', 'profile_17', 'profile_18', 'profile_19', 'profile_20', 'profile_21', 'profile_22', 'team_playerteamhistory_1', 'team_playerteamhistory_2', 'team_playerteamhistory_3', 'team_playerteamhistory_4', 'team_playerteamhistory_5', 'team_playerteamhistory_6', 'team_playerteamhistory_7', 'team_playerteamhistory_8', 'team_playerteamhistory_9', 'team_playerteamhistory_10', 'team_playerteamhistory_11', 'team_playerteamhistory_12', 'team_playerteamhistory_13', 'team_playerteamhistory_14', 'team_playerteamhistory_15', 'team_playerteamhistory_16', 'team_playerteamhistory_17', 'teamsurvey_1', 'teamsurvey_2', 'teamplayer_1', 'teamplayer_2', 'teamplayer_3', 'teamplayer_4', 'teamplayer_5', 'teamplayer_6', 'teamplayer_7', 'teamplayer_8', 'teamplayer_9', 'teamplayer_10', 'teamplayer_11', 'teamseason_1', 'teamseason_2', 'teamseason_3', 'teamseason_4', 'teamseason_5', 'teamseason_6', 'teamseason_7', 'team_1', 'team_2', 'team_3', 'team_4', 'transfer_1', 'transfer_2', 'transfer_3', 'transfer_4', 'transfer_5', 'transfer_6', 'transfer_7', 'transfer_8', 'transfer_9', 'transfer_10', 'transfer_11', 'transfer_12', 'transfer_13', 'transfer_14', 'transfer_15', 'transfer_16', 'transfer_17', 'transfer_18', 'transfer_19', 'transfer_20', 'transfer_21', 'transfer_22', 'user_1', 'user_2', 'user_3', 'user_4', 'user_5', 'user_6', 'user_7', 'user_8', 'user_9', 'user_10', 'user_11', 'user_12', 'user_13', 'user_14', 'user_15', 'user_16', 'user_17', 'user_18', 'user_19', 'user_20', 'user_21', 'user_22', 'user_23', 'videogalleryitem_1', 'videogalleryitem_2');

        if(!empty($_GET['s'])){
            $s = $_GET['s']+1;
        }else{
            $s = 1;
        }

        $block = 10000;

        if($param == 'smalls_1'){

            $models = Agenda::orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }

            $models = Championships::orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }

            $models = Conference::orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }

            $models = Coordinator::orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }

            $models = District::orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }

            $models = EliminationTree::orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }

            $models = EliminationTreeItem::orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }

            $models = GroundImage::orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }

        }

        if($param == 'smalls_2'){

            $models = GroundProperty::orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }

            $models = Ground::orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }

            $models = Group::orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }

            $models = LeagueGalleries::orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }

            $models = LeagueGalleryItems::orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }

            $models = League::orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }

            $models = NewsTypes::orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }

            $models = Nostalgia::orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }

            $models = PanoramaType::orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }

            $models = PenaltyType::orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }

        }

        if($param == 'smalls_3'){

            $models = Penalty::orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }

            $models = PlayerPosition::orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }

            $models = PointType::orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }

            $models = Province::orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }

            $models = Referee::orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }

            $models = ReservationGroundsubscription::orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }

        }

        if($param == 'smalls_4'){

            $models = ReservationGroundtable::orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }

            $models = ReservationOffer::orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }

            $models = Reservation::orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }

            $models = Season::orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }

            $models = Tactic::orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }

            $models = Tag::orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }

            $models = TeamPotential::orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }

            $models = TeamSatisfaction::orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }

            $models = TeamTeamSeasonAddPointLog::orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }

        }





        if($param == 'gallery_1'){
            $models = Galleries::offset(0*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'gallery_2'){
            $models = Galleries::offset(1*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'gallery_3'){
            $models = Galleries::offset(2*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }





        if($param == 'match_action_1'){
            $models = MatchAction::offset(0*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_action_2'){
            $models = MatchAction::offset(1*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_action_3'){
            $models = MatchAction::offset(2*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_action_4'){
            $models = MatchAction::offset(3*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_action_5'){
            $models = MatchAction::offset(4*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_action_6'){
            $models = MatchAction::offset(5*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_action_7'){
            $models = MatchAction::offset(6*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_action_8'){
            $models = MatchAction::offset(7*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_action_9'){
            $models = MatchAction::offset(8*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_action_10'){
            $models = MatchAction::offset(9*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_action_11'){
            $models = MatchAction::offset(10*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_action_12'){
            $models = MatchAction::offset(11*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_action_13'){
            $models = MatchAction::offset(12*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_action_14'){
            $models = MatchAction::offset(13*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_action_15'){
            $models = MatchAction::offset(14*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_action_16'){
            $models = MatchAction::offset(15*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_action_17'){
            $models = MatchAction::offset(16*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_action_18'){
            $models = MatchAction::offset(17*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_action_19'){
            $models = MatchAction::offset(18*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_action_20'){
            $models = MatchAction::offset(19*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_action_21'){
            $models = MatchAction::offset(20*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_action_22'){
            $models = MatchAction::offset(21*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_action_23'){
            $models = MatchAction::offset(22*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_action_24'){
            $models = MatchAction::offset(23*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_action_25'){
            $models = MatchAction::offset(24*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_action_26'){
            $models = MatchAction::offset(25*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_action_27'){
            $models = MatchAction::offset(26*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_action_28'){
            $models = MatchAction::offset(27*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_action_29'){
            $models = MatchAction::offset(28*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_action_30'){
            $models = MatchAction::offset(29*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_action_31'){
            $models = MatchAction::offset(30*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_action_32'){
            $models = MatchAction::offset(31*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_action_33'){
            $models = MatchAction::offset(32*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_action_34'){
            $models = MatchAction::offset(33*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_action_35'){
            $models = MatchAction::offset(34*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_action_36'){
            $models = MatchAction::offset(35*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_action_37'){
            $models = MatchAction::offset(36*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_action_38'){
            $models = MatchAction::offset(37*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_action_39'){
            $models = MatchAction::offset(38*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_action_40'){
            $models = MatchAction::offset(39*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_action_41'){
            $models = MatchAction::offset(40*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_action_42'){
            $models = MatchAction::offset(41*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_action_43'){
            $models = MatchAction::offset(42*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_action_44'){
            $models = MatchAction::offset(43*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_action_45'){
            $models = MatchAction::offset(44*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_action_46'){
            $models = MatchAction::offset(45*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_action_47'){
            $models = MatchAction::offset(46*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_action_48'){
            $models = MatchAction::offset(47*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_action_49'){
            $models = MatchAction::offset(48*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_action_50'){
            $models = MatchAction::offset(49*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }





        if($param == 'match_image_1'){
            $models = MatchImage::offset(0*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_2'){
            $models = MatchImage::offset(1*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_3'){
            $models = MatchImage::offset(2*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_4'){
            $models = MatchImage::offset(3*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_5'){
            $models = MatchImage::offset(4*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_6'){
            $models = MatchImage::offset(5*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_7'){
            $models = MatchImage::offset(6*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_8'){
            $models = MatchImage::offset(7*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_9'){
            $models = MatchImage::offset(8*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_10'){
            $models = MatchImage::offset(9*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_11'){
            $models = MatchImage::offset(10*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_12'){
            $models = MatchImage::offset(11*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_13'){
            $models = MatchImage::offset(12*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_14'){
            $models = MatchImage::offset(13*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_15'){
            $models = MatchImage::offset(14*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_16'){
            $models = MatchImage::offset(15*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_17'){
            $models = MatchImage::offset(16*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_18'){
            $models = MatchImage::offset(17*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_19'){
            $models = MatchImage::offset(18*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_20'){
            $models = MatchImage::offset(19*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_21'){
            $models = MatchImage::offset(20*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_22'){
            $models = MatchImage::offset(21*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_23'){
            $models = MatchImage::offset(22*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_24'){
            $models = MatchImage::offset(23*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_25'){
            $models = MatchImage::offset(24*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_26'){
            $models = MatchImage::offset(25*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_27'){
            $models = MatchImage::offset(26*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_28'){
            $models = MatchImage::offset(27*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_29'){
            $models = MatchImage::offset(28*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_30'){
            $models = MatchImage::offset(29*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_31'){
            $models = MatchImage::offset(30*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_32'){
            $models = MatchImage::offset(31*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_33'){
            $models = MatchImage::offset(32*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_34'){
            $models = MatchImage::offset(33*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_35'){
            $models = MatchImage::offset(34*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_36'){
            $models = MatchImage::offset(35*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_37'){
            $models = MatchImage::offset(36*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_38'){
            $models = MatchImage::offset(37*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_39'){
            $models = MatchImage::offset(38*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_40'){
            $models = MatchImage::offset(39*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_41'){
            $models = MatchImage::offset(40*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_42'){
            $models = MatchImage::offset(41*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_43'){
            $models = MatchImage::offset(42*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_44'){
            $models = MatchImage::offset(43*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_45'){
            $models = MatchImage::offset(44*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_46'){
            $models = MatchImage::offset(45*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_47'){
            $models = MatchImage::offset(46*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_48'){
            $models = MatchImage::offset(47*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_49'){
            $models = MatchImage::offset(48*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_50'){
            $models = MatchImage::offset(49*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_51'){
            $models = MatchImage::offset(50*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_52'){
            $models = MatchImage::offset(51*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_53'){
            $models = MatchImage::offset(52*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_54'){
            $models = MatchImage::offset(53*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_55'){
            $models = MatchImage::offset(54*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_56'){
            $models = MatchImage::offset(55*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_57'){
            $models = MatchImage::offset(56*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_58'){
            $models = MatchImage::offset(57*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_59'){
            $models = MatchImage::offset(58*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_60'){
            $models = MatchImage::offset(59*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_61'){
            $models = MatchImage::offset(60*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_62'){
            $models = MatchImage::offset(61*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_63'){
            $models = MatchImage::offset(62*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_64'){
            $models = MatchImage::offset(63*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_65'){
            $models = MatchImage::offset(64*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_66'){
            $models = MatchImage::offset(65*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_67'){
            $models = MatchImage::offset(66*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_68'){
            $models = MatchImage::offset(67*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_69'){
            $models = MatchImage::offset(68*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_70'){
            $models = MatchImage::offset(69*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_71'){
            $models = MatchImage::offset(70*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_72'){
            $models = MatchImage::offset(71*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_73'){
            $models = MatchImage::offset(72*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_74'){
            $models = MatchImage::offset(73*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_75'){
            $models = MatchImage::offset(74*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_76'){
            $models = MatchImage::offset(75*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_77'){
            $models = MatchImage::offset(76*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_78'){
            $models = MatchImage::offset(77*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_79'){
            $models = MatchImage::offset(78*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_80'){
            $models = MatchImage::offset(79*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_81'){
            $models = MatchImage::offset(80*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_82'){
            $models = MatchImage::offset(81*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_83'){
            $models = MatchImage::offset(82*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_84'){
            $models = MatchImage::offset(83*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_85'){
            $models = MatchImage::offset(84*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_86'){
            $models = MatchImage::offset(85*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_87'){
            $models = MatchImage::offset(86*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_88'){
            $models = MatchImage::offset(87*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_89'){
            $models = MatchImage::offset(88*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_90'){
            $models = MatchImage::offset(89*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_91'){
            $models = MatchImage::offset(90*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_92'){
            $models = MatchImage::offset(91*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_93'){
            $models = MatchImage::offset(92*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_94'){
            $models = MatchImage::offset(93*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_95'){
            $models = MatchImage::offset(94*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_96'){
            $models = MatchImage::offset(95*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_97'){
            $models = MatchImage::offset(96*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_98'){
            $models = MatchImage::offset(97*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_99'){
            $models = MatchImage::offset(98*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_image_100'){
            $models = MatchImage::offset(99*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }





        if($param == 'match_panorama_1'){
            $models = MatchPanorama::offset(0*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_panorama_2'){
            $models = MatchPanorama::offset(1*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_panorama_3'){
            $models = MatchPanorama::offset(2*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_panorama_4'){
            $models = MatchPanorama::offset(3*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }





        // match_player - 1380 K
        if($param == 'match_player_1'){
            $models = MatchPlayer::offset(0*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_2'){
            $models = MatchPlayer::offset(1*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_3'){
            $models = MatchPlayer::offset(2*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_4'){
            $models = MatchPlayer::offset(3*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_5'){
            $models = MatchPlayer::offset(4*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_6'){
            $models = MatchPlayer::offset(5*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_7'){
            $models = MatchPlayer::offset(6*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_8'){
            $models = MatchPlayer::offset(7*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_9'){
            $models = MatchPlayer::offset(8*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_10'){
            $models = MatchPlayer::offset(9*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_11'){
            $models = MatchPlayer::offset(10*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_12'){
            $models = MatchPlayer::offset(11*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_13'){
            $models = MatchPlayer::offset(12*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_14'){
            $models = MatchPlayer::offset(13*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_15'){
            $models = MatchPlayer::offset(14*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_16'){
            $models = MatchPlayer::offset(15*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_17'){
            $models = MatchPlayer::offset(16*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_18'){
            $models = MatchPlayer::offset(17*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_19'){
            $models = MatchPlayer::offset(18*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_20'){
            $models = MatchPlayer::offset(19*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_21'){
            $models = MatchPlayer::offset(20*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_22'){
            $models = MatchPlayer::offset(21*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_23'){
            $models = MatchPlayer::offset(22*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_24'){
            $models = MatchPlayer::offset(23*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_25'){
            $models = MatchPlayer::offset(24*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_26'){
            $models = MatchPlayer::offset(25*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_27'){
            $models = MatchPlayer::offset(26*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_28'){
            $models = MatchPlayer::offset(27*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_29'){
            $models = MatchPlayer::offset(28*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_30'){
            $models = MatchPlayer::offset(29*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_31'){
            $models = MatchPlayer::offset(30*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_32'){
            $models = MatchPlayer::offset(31*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_33'){
            $models = MatchPlayer::offset(32*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_34'){
            $models = MatchPlayer::offset(33*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_35'){
            $models = MatchPlayer::offset(34*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_36'){
            $models = MatchPlayer::offset(35*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_37'){
            $models = MatchPlayer::offset(36*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_38'){
            $models = MatchPlayer::offset(37*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_39'){
            $models = MatchPlayer::offset(38*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_40'){
            $models = MatchPlayer::offset(39*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_41'){
            $models = MatchPlayer::offset(40*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_42'){
            $models = MatchPlayer::offset(41*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_43'){
            $models = MatchPlayer::offset(42*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_44'){
            $models = MatchPlayer::offset(43*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_45'){
            $models = MatchPlayer::offset(44*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_46'){
            $models = MatchPlayer::offset(45*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_47'){
            $models = MatchPlayer::offset(46*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_48'){
            $models = MatchPlayer::offset(47*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_49'){
            $models = MatchPlayer::offset(48*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_50'){
            $models = MatchPlayer::offset(49*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_51'){
            $models = MatchPlayer::offset(50*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_52'){
            $models = MatchPlayer::offset(51*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_53'){
            $models = MatchPlayer::offset(52*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_54'){
            $models = MatchPlayer::offset(53*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_55'){
            $models = MatchPlayer::offset(54*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_56'){
            $models = MatchPlayer::offset(55*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_57'){
            $models = MatchPlayer::offset(56*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_58'){
            $models = MatchPlayer::offset(57*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_59'){
            $models = MatchPlayer::offset(58*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_60'){
            $models = MatchPlayer::offset(59*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_61'){
            $models = MatchPlayer::offset(60*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_62'){
            $models = MatchPlayer::offset(61*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_63'){
            $models = MatchPlayer::offset(62*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_64'){
            $models = MatchPlayer::offset(63*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_65'){
            $models = MatchPlayer::offset(64*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_66'){
            $models = MatchPlayer::offset(65*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_67'){
            $models = MatchPlayer::offset(66*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_68'){
            $models = MatchPlayer::offset(67*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_69'){
            $models = MatchPlayer::offset(68*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_70'){
            $models = MatchPlayer::offset(69*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_71'){
            $models = MatchPlayer::offset(70*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_72'){
            $models = MatchPlayer::offset(71*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_73'){
            $models = MatchPlayer::offset(72*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_74'){
            $models = MatchPlayer::offset(73*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_75'){
            $models = MatchPlayer::offset(74*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_76'){
            $models = MatchPlayer::offset(75*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_77'){
            $models = MatchPlayer::offset(76*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_78'){
            $models = MatchPlayer::offset(77*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_79'){
            $models = MatchPlayer::offset(78*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_80'){
            $models = MatchPlayer::offset(79*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_81'){
            $models = MatchPlayer::offset(80*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_82'){
            $models = MatchPlayer::offset(81*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_83'){
            $models = MatchPlayer::offset(82*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_84'){
            $models = MatchPlayer::offset(83*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_85'){
            $models = MatchPlayer::offset(84*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_86'){
            $models = MatchPlayer::offset(85*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_87'){
            $models = MatchPlayer::offset(86*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_88'){
            $models = MatchPlayer::offset(87*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_89'){
            $models = MatchPlayer::offset(88*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_90'){
            $models = MatchPlayer::offset(89*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_91'){
            $models = MatchPlayer::offset(90*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_92'){
            $models = MatchPlayer::offset(91*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_93'){
            $models = MatchPlayer::offset(92*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_94'){
            $models = MatchPlayer::offset(93*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_95'){
            $models = MatchPlayer::offset(94*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_96'){
            $models = MatchPlayer::offset(95*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_97'){
            $models = MatchPlayer::offset(96*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_98'){
            $models = MatchPlayer::offset(97*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_99'){
            $models = MatchPlayer::offset(98*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_100'){
            $models = MatchPlayer::offset(99*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_101'){
            $models = MatchPlayer::offset(100*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_102'){
            $models = MatchPlayer::offset(101*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_103'){
            $models = MatchPlayer::offset(102*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_104'){
            $models = MatchPlayer::offset(103*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_105'){
            $models = MatchPlayer::offset(104*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_106'){
            $models = MatchPlayer::offset(105*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_107'){
            $models = MatchPlayer::offset(106*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_108'){
            $models = MatchPlayer::offset(107*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_109'){
            $models = MatchPlayer::offset(108*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_110'){
            $models = MatchPlayer::offset(109*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_111'){
            $models = MatchPlayer::offset(110*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_112'){
            $models = MatchPlayer::offset(111*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_113'){
            $models = MatchPlayer::offset(112*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_114'){
            $models = MatchPlayer::offset(113*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_115'){
            $models = MatchPlayer::offset(114*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_116'){
            $models = MatchPlayer::offset(115*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_117'){
            $models = MatchPlayer::offset(116*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_118'){
            $models = MatchPlayer::offset(117*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_119'){
            $models = MatchPlayer::offset(118*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_120'){
            $models = MatchPlayer::offset(119*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_121'){
            $models = MatchPlayer::offset(120*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_122'){
            $models = MatchPlayer::offset(121*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_123'){
            $models = MatchPlayer::offset(122*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_124'){
            $models = MatchPlayer::offset(123*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_125'){
            $models = MatchPlayer::offset(124*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_126'){
            $models = MatchPlayer::offset(125*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_127'){
            $models = MatchPlayer::offset(126*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_128'){
            $models = MatchPlayer::offset(127*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_129'){
            $models = MatchPlayer::offset(128*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_130'){
            $models = MatchPlayer::offset(129*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_131'){
            $models = MatchPlayer::offset(130*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_132'){
            $models = MatchPlayer::offset(131*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_133'){
            $models = MatchPlayer::offset(132*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_134'){
            $models = MatchPlayer::offset(133*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_135'){
            $models = MatchPlayer::offset(134*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_136'){
            $models = MatchPlayer::offset(135*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_137'){
            $models = MatchPlayer::offset(136*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_138'){
            $models = MatchPlayer::offset(137*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_player_139'){
            $models = MatchPlayer::offset(138*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }





        if($param == 'match_video_1'){
            $models = MatchVideo::offset(0*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_video_2'){
            $models = MatchVideo::offset(1*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_video_3'){
            $models = MatchVideo::offset(2*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_video_4'){
            $models = MatchVideo::offset(3*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_video_5'){
            $models = MatchVideo::offset(4*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_video_6'){
            $models = MatchVideo::offset(5*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_video_7'){
            $models = MatchVideo::offset(6*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_video_8'){
            $models = MatchVideo::offset(7*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_video_9'){
            $models = MatchVideo::offset(8*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_video_10'){
            $models = MatchVideo::offset(9*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_video_11'){
            $models = MatchVideo::offset(10*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }





        if($param == 'match_1'){
            $models = Match::offset(0*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_2'){
            $models = Match::offset(1*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_3'){
            $models = Match::offset(2*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_4'){
            $models = Match::offset(3*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_5'){
            $models = Match::offset(4*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_6'){
            $models = Match::offset(5*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_7'){
            $models = Match::offset(6*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_8'){
            $models = Match::offset(7*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_9'){
            $models = Match::offset(8*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_10'){
            $models = Match::offset(9*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'match_11'){
            $models = Match::offset(10*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }





        if($param == 'new_1'){
            $models = News::offset(0*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->date = Carbon::parse($model->date);
                $model->save();
            }
        }

        if($param == 'new_2'){
            $models = News::offset(1*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->date = Carbon::parse($model->date);
                $model->save();
            }
        }

        if($param == 'new_3'){
            $models = News::offset(2*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->date = Carbon::parse($model->date);
                $model->save();
            }
        }





        if($param == 'photogalleryitem_1'){
            $models = PhotoGalleryItem::offset(0*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'photogalleryitem_2'){
            $models = PhotoGalleryItem::offset(1*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'photogalleryitem_3'){
            $models = PhotoGalleryItem::offset(2*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }





        if($param == 'playervalue_1'){
            $models = PlayerValue::offset(0*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'playervalue_2'){
            $models = PlayerValue::offset(1*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'playervalue_3'){
            $models = PlayerValue::offset(2*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'playervalue_4'){
            $models = PlayerValue::offset(3*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'playervalue_5'){
            $models = PlayerValue::offset(4*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'playervalue_6'){
            $models = PlayerValue::offset(5*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'playervalue_7'){
            $models = PlayerValue::offset(6*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'playervalue_8'){
            $models = PlayerValue::offset(7*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'playervalue_9'){
            $models = PlayerValue::offset(8*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'playervalue_10'){
            $models = PlayerValue::offset(9*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'playervalue_11'){
            $models = PlayerValue::offset(10*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'playervalue_12'){
            $models = PlayerValue::offset(11*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'playervalue_13'){
            $models = PlayerValue::offset(12*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'playervalue_14'){
            $models = PlayerValue::offset(13*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }





        // players - 210 K
        if($param == 'player_1'){
            $models = Player::offset(0*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'player_2'){
            $models = Player::offset(1*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'player_3'){
            $models = Player::offset(2*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'player_4'){
            $models = Player::offset(3*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'player_5'){
            $models = Player::offset(4*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'player_6'){
            $models = Player::offset(5*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'player_7'){
            $models = Player::offset(6*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'player_8'){
            $models = Player::offset(7*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'player_9'){
            $models = Player::offset(8*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'player_10'){
            $models = Player::offset(9*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'player_11'){
            $models = Player::offset(10*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'player_12'){
            $models = Player::offset(11*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'player_13'){
            $models = Player::offset(12*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'player_14'){
            $models = Player::offset(13*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'player_15'){
            $models = Player::offset(14*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'player_16'){
            $models = Player::offset(15*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'player_17'){
            $models = Player::offset(16*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'player_18'){
            $models = Player::offset(17*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'player_19'){
            $models = Player::offset(18*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'player_20'){
            $models = Player::offset(19*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'player_21'){
            $models = Player::offset(20*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'player_22'){
            $models = Player::offset(21*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }





        if($param == 'pressconferences_1'){
            $models = PressConferences::offset(0*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'pressconferences_2'){
            $models = PressConferences::offset(1*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'pressconferences_3'){
            $models = PressConferences::offset(2*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'pressconferences_4'){
            $models = PressConferences::offset(3*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'pressconferences_5'){
            $models = PressConferences::offset(4*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'pressconferences_6'){
            $models = PressConferences::offset(5*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'pressconferences_7'){
            $models = PressConferences::offset(6*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'pressconferences_8'){
            $models = PressConferences::offset(7*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }





        // profile - 210 K
        if($param == 'profile_1'){
            $models = Profile::offset(0*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'profile_2'){
            $models = Profile::offset(1*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'profile_3'){
            $models = Profile::offset(2*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'profile_4'){
            $models = Profile::offset(3*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'profile_5'){
            $models = Profile::offset(4*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'profile_6'){
            $models = Profile::offset(5*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'profile_7'){
            $models = Profile::offset(6*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'profile_8'){
            $models = Profile::offset(7*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'profile_9'){
            $models = Profile::offset(8*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'profile_10'){
            $models = Profile::offset(9*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'profile_11'){
            $models = Profile::offset(10*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'profile_12'){
            $models = Profile::offset(11*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'profile_13'){
            $models = Profile::offset(12*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'profile_14'){
            $models = Profile::offset(13*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'profile_15'){
            $models = Profile::offset(14*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'profile_16'){
            $models = Profile::offset(15*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'profile_17'){
            $models = Profile::offset(16*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'profile_18'){
            $models = Profile::offset(17*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'profile_19'){
            $models = Profile::offset(18*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'profile_20'){
            $models = Profile::offset(19*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'profile_21'){
            $models = Profile::offset(20*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'profile_22'){
            $models = Profile::offset(21*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }





        // TeamPlayerTeamHistory - 159 K
        if($param == 'team_playerteamhistory_1'){
            $models = TeamPlayerTeamHistory::offset(0*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'team_playerteamhistory_2'){
            $models = TeamPlayerTeamHistory::offset(1*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'team_playerteamhistory_3'){
            $models = TeamPlayerTeamHistory::offset(2*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'team_playerteamhistory_4'){
            $models = TeamPlayerTeamHistory::offset(3*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'team_playerteamhistory_5'){
            $models = TeamPlayerTeamHistory::offset(4*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'team_playerteamhistory_6'){
            $models = TeamPlayerTeamHistory::offset(5*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'team_playerteamhistory_7'){
            $models = TeamPlayerTeamHistory::offset(6*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'team_playerteamhistory_8'){
            $models = TeamPlayerTeamHistory::offset(7*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'team_playerteamhistory_9'){
            $models = TeamPlayerTeamHistory::offset(8*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'team_playerteamhistory_10'){
            $models = TeamPlayerTeamHistory::offset(9*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'team_playerteamhistory_11'){
            $models = TeamPlayerTeamHistory::offset(10*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'team_playerteamhistory_12'){
            $models = TeamPlayerTeamHistory::offset(11*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'team_playerteamhistory_13'){
            $models = TeamPlayerTeamHistory::offset(12*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'team_playerteamhistory_14'){
            $models = TeamPlayerTeamHistory::offset(13*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'team_playerteamhistory_15'){
            $models = TeamPlayerTeamHistory::offset(14*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'team_playerteamhistory_16'){
            $models = TeamPlayerTeamHistory::offset(15*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'team_playerteamhistory_17'){
            $models = TeamPlayerTeamHistory::offset(16*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }





        if($param == 'teamsurvey_1'){
            $models = TeamSurvey::offset(0*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'teamsurvey_2'){
            $models = TeamSurvey::offset(1*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }





        if($param == 'teamplayer_1'){
            $models = TeamTeamPlayer::offset(0*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'teamplayer_2'){
            $models = TeamTeamPlayer::offset(1*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'teamplayer_3'){
            $models = TeamTeamPlayer::offset(2*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'teamplayer_4'){
            $models = TeamTeamPlayer::offset(3*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'teamplayer_5'){
            $models = TeamTeamPlayer::offset(4*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'teamplayer_6'){
            $models = TeamTeamPlayer::offset(5*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'teamplayer_7'){
            $models = TeamTeamPlayer::offset(6*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'teamplayer_8'){
            $models = TeamTeamPlayer::offset(7*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'teamplayer_9'){
            $models = TeamTeamPlayer::offset(8*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'teamplayer_10'){
            $models = TeamTeamPlayer::offset(9*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'teamplayer_11'){
            $models = TeamTeamPlayer::offset(10*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }





        if($param == 'teamseason_1'){
            $models = TeamTeamSeason::offset(0*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'teamseason_2'){
            $models = TeamTeamSeason::offset(1*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'teamseason_3'){
            $models = TeamTeamSeason::offset(2*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'teamseason_4'){
            $models = TeamTeamSeason::offset(3*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'teamseason_5'){
            $models = TeamTeamSeason::offset(4*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'teamseason_6'){
            $models = TeamTeamSeason::offset(5*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'teamseason_7'){
            $models = TeamTeamSeason::offset(6*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }





        if($param == 'team_1'){
            $models = Team::offset(0*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'team_2'){
            $models = Team::offset(1*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'team_3'){
            $models = Team::offset(2*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'team_4'){
            $models = Team::offset(3*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }





        // Transfer - 210 K
        if($param == 'transfer_1'){
            $models = Transfer::offset(0*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->replied_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'transfer_2'){
            $models = Transfer::offset(1*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->replied_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'transfer_3'){
            $models = Transfer::offset(2*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->replied_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'transfer_4'){
            $models = Transfer::offset(3*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->replied_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'transfer_5'){
            $models = Transfer::offset(4*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->replied_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'transfer_6'){
            $models = Transfer::offset(5*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->replied_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'transfer_7'){
            $models = Transfer::offset(6*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->replied_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'transfer_8'){
            $models = Transfer::offset(7*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->replied_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'transfer_9'){
            $models = Transfer::offset(8*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->replied_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'transfer_10'){
            $models = Transfer::offset(9*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->replied_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'transfer_11'){
            $models = Transfer::offset(10*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->replied_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'transfer_12'){
            $models = Transfer::offset(11*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->replied_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'transfer_13'){
            $models = Transfer::offset(12*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->replied_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'transfer_14'){
            $models = Transfer::offset(13*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->replied_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'transfer_15'){
            $models = Transfer::offset(14*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->replied_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'transfer_16'){
            $models = Transfer::offset(15*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->replied_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'transfer_17'){
            $models = Transfer::offset(16*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->replied_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'transfer_18'){
            $models = Transfer::offset(17*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->replied_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'transfer_19'){
            $models = Transfer::offset(18*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->replied_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'transfer_20'){
            $models = Transfer::offset(19*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->replied_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'transfer_21'){
            $models = Transfer::offset(20*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->replied_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'transfer_22'){
            $models = Transfer::offset(21*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->replied_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }





        // User - 220 K
        if($param == 'user_1'){
            $models = User::offset(0*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'user_2'){
            $models = User::offset(1*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'user_3'){
            $models = User::offset(2*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'user_4'){
            $models = User::offset(3*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'user_5'){
            $models = User::offset(4*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'user_6'){
            $models = User::offset(5*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'user_7'){
            $models = User::offset(6*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'user_8'){
            $models = User::offset(7*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'user_9'){
            $models = User::offset(8*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'user_10'){
            $models = User::offset(9*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'user_11'){
            $models = User::offset(10*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'user_12'){
            $models = User::offset(11*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'user_13'){
            $models = User::offset(12*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'user_14'){
            $models = User::offset(13*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'user_15'){
            $models = User::offset(14*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'user_16'){
            $models = User::offset(15*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'user_17'){
            $models = User::offset(16*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'user_18'){
            $models = User::offset(17*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'user_19'){
            $models = User::offset(18*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'user_20'){
            $models = User::offset(19*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'user_21'){
            $models = User::offset(20*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'user_22'){
            $models = User::offset(21*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'user_23'){
            $models = User::offset(22*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }





        if($param == 'videogalleryitem_1'){
            $models = VideoGalleryItem::offset(0*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }

        if($param == 'videogalleryitem_2'){
            $models = VideoGalleryItem::offset(1*$block)->limit($block)->orderBy('created_at', 'desc')->get();
            foreach ($models as $model) {
                $model->created_at = Carbon::parse($model->created_at);
                $model->updated_at = Carbon::parse($model->updated_at);
                $model->save();
            }
        }




        echo '<a href="'.url('/date').'/'.$listArray[$s].'?s='.$s.'">Sıradaki</a>';
        dd('OK: '.$param);



        //dd($teams);
    }

    public function playerDegerGuncelle($player_id){

        $player = Player::find($player_id);

        if(!empty($player)){
            $playerPoint = $this->hesapla($player_id, true);
            $player->value = $playerPoint;
            $player->save();
            echo 'Oyuncu Değeri başarıyla güncellendi.';
        }else{
            echo 'Oyuncu bulunamadı.';
        }
    }

    public function degerUygula($offset, $limit) {

        $users = User::where('is_active', true)->offset($offset)->limit($limit)->orderBy('id')->get();
        //dump($users);
        echo 'OK ---- offset:'.$offset.' - limit:'.$limit.'</br>';
        echo '<a href="'.url('/degeruygula').'/'.($offset+$limit).'/'.$limit.'">Sıradaki</a> (offset:'.($offset+$limit).' - limit:'.$limit.') <br>';
        foreach ($users as $user) {

            $isPlayer = false;
            $playerPoint = 0;
            if(!empty($user->player) && !empty($user->profile->province_id)){
                $isPlayer = true;
                $playerPoint = $this->hesapla($user->player->id, false);
                $user->player->value = $playerPoint;
                $user->player->save();
                echo 'UID:'.$user->id.' / '.$user->first_name.' '.$user->last_name.' ('.$user->player->id.') #'.$isPlayer.'<br>';
            }else{
                $isPlayer = false;
                echo 'UID:'.$user->id.' / '.$user->first_name.' '.$user->last_name.' # Oyuncu Değil<br>';
            }

            if($isPlayer){
                echo '<script>document.querySelector("body").onload = function(){
                    console.log("asd");
                    setTimeout(function(){  window.location = "'.url('/degeruygula').'/'.($offset+$limit).'/'.$limit.'"; }, 5000);
                };</script>';
            }
        }
    }

    public function hesapla($player_id, $visibleLog) {

        // AÇÇ echo 'Ayın MVPsi, Oyuncu Ayın Centilmeni/Mevki/Golü, Sezonun MVPsi, Oyuncu Sezonun Centilmeni/Mevki/Golü bilgileri veritabanında mevcut değil. Bu sebeple hesaplama dışında tutulmuştur.';

        $pointTotalPlayer = 100000;

        $pointMatch = 8000;
        $pointSave = 3000;
        $pointGoal = 5000;
        $pointAssist = 3000;


        $pointYellowCard = -3000;
        $pointRedCard = -25000;


        $panoramaPoint = array(2 => array(8000, 'Maçın Golü'), 3 => array(25000, 'Maçın Adamı'), 5 => array(8000, 'Maçın Defansı'), 6 => array(8000, 'Maçın Orta Sahası'), 7 => array(8000, 'Maçın Forveti'), 8 => array(8000, 'Maçın Kalecisi'));


        $seasonCross = array(4, 3.5, 2.5, 1.5);
        $seasonCrossIndex = 0;

        $seasonTRCross = array(10, 3);
        $seasonTRCrossIndex = 0;

        $seasonList = array();
        $seasonTRList = array();

        $player = Player::find($player_id);
        $user = $player->user;
        $league_provinces = $user->profile->province->league_province;

        /*********** Sezon sıralama ve çarpan değerleri hesaplaması ***********/
        if($visibleLog){
            dump('(id: '.$user->id.') (player_id: '.$user->player->id.') '.$user->first_name.' '.$user->last_name.' ('.$user->profile->province->id.' - '.$user->profile->province->name.')');
        }

        // Türkiye Finalleri Ligi ve Avrupa Finalleri Ligi ayrı tutulacak.
        foreach ($league_provinces as $league_province) {

            if($league_province->league->league_province->count() == 1){
                
                if($visibleLog){
                    dump('Lig: '.$league_province->league->name);
                }

                $seasons = $league_province->league->seasonApi;
                foreach ($seasons as $season) {

                    if($season->polymorphic_ctype_id == 32){ $seasonType = 'elimination'; }else if($season->polymorphic_ctype_id == 33){ $seasonType = 'fixture'; }else if($season->polymorphic_ctype_id == 34){ $seasonType = 'point'; }
                    // dump('('.$seasonCross[$seasonCrossIndex].') (id:'.$season->id.') - '.$season->year.' - '.$season->name.' - '.$seasonType);
                    $seasonList[] = array($seasonCross[$seasonCrossIndex], $season->id, $seasonType, ($season->year.' - '.$season->name), $season->year);

                    if(!empty($season->league_eliminationseasonApi)){

                        if($season->league_eliminationseasonApi->season_ptr->polymorphic_ctype_id == 32){ $seasonRType = 'elimination'; }else if($season->league_eliminationseasonApi->season_ptr->polymorphic_ctype_id == 33){ $seasonRType = 'fixture'; }else if($season->league_eliminationseasonApi->season_ptr->polymorphic_ctype_id == 34){ $seasonRType = 'point'; }

                        // dump('('.$seasonCross[$seasonCrossIndex].') (id:'.$season->league_eliminationseasonApi->season_ptr->id.') - '.' ---- '.$season->league_eliminationseasonApi->season_ptr->year.' - '.$season->league_eliminationseasonApi->season_ptr->name.' - '.$seasonRType);

                        $seasonList[] = array($seasonCross[$seasonCrossIndex], $season->league_eliminationseasonApi->season_ptr->id, $seasonRType, ($season->league_eliminationseasonApi->season_ptr->year.' - '.$season->league_eliminationseasonApi->season_ptr->name), $season->league_eliminationseasonApi->season_ptr->year );

                    }

                    if(count($seasonCross)-1 > $seasonCrossIndex){
                        $seasonCrossIndex++;
                    }
                }
                
                // dump('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'); 
                
                $all_seasons = $league_province->league->season->where('active', true);
                foreach ($all_seasons as $season) {

                    if($season->polymorphic_ctype_id == 32){ $seasonEType = 'elimination'; }else if($season->polymorphic_ctype_id == 33){ $seasonEType = 'fixture'; }else if($season->polymorphic_ctype_id == 34){ $seasonEType = 'point'; }
                    $is_valid = array_search($season->id, array_column($seasonList, 1));
                    // dump('---('.end($seasonCross).') (id:'.$season->id.') - '.$season->year.' - '.$season->name.' - '.$seasonEType.' - '.$is_valid.' - '.( ($is_valid !== false) ? $is_valid : 'false') );
                    
                    if($is_valid === false){
                        $seasonList[] = array(end($seasonCross), $season->id, $seasonEType, ($season->year.' - '.$season->name), $season->year);
                    }
                }

                if($visibleLog){
                    dump($seasonList);
                }
                

            }else if($league_province->league->league_province->count() > 1){

                if($visibleLog){
                    dump('Lig: '.$league_province->league->name);
                }

                $seasonsTR = $league_province->league->seasonApi;
                foreach ($seasonsTR as $seasonTR) {

                    if($seasonTR->polymorphic_ctype_id == 32){ $seasonType = 'elimination'; }else if($seasonTR->polymorphic_ctype_id == 33){ $seasonType = 'fixture'; }else if($seasonTR->polymorphic_ctype_id == 34){ $seasonType = 'point'; }
                    // dump('('.$seasonTRCross[$seasonTRCrossIndex].') (id:'.$seasonTR->id.') - '.$seasonTR->year.' - '.$seasonTR->name.' - '.$seasonType);
                    $seasonTRList[] = array($seasonTRCross[$seasonTRCrossIndex], $seasonTR->id, $seasonType, ($seasonTR->year.' - '.$seasonTR->name), $seasonTR->year);

                    if(!empty($seasonTR->league_eliminationseasonApi)){

                        if($seasonTR->league_eliminationseasonApi->season_ptr->polymorphic_ctype_id == 32){ $seasonRType = 'elimination'; }else if($seasonTR->league_eliminationseasonApi->season_ptr->polymorphic_ctype_id == 33){ $seasonRType = 'fixture'; }else if($seasonTR->league_eliminationseasonApi->season_ptr->polymorphic_ctype_id == 34){ $seasonRType = 'point'; }

                        // dump('('.$seasonTRCross[$seasonTRCrossIndex].') (id:'.$seasonTR->league_eliminationseasonApi->season_ptr->id.') - '.' ---- '.$seasonTR->league_eliminationseasonApi->season_ptr->year.' - '.$seasonTR->league_eliminationseasonApi->season_ptr->name.' - '.$seasonRType);

                        $seasonTRList[] = array($seasonTRCross[$seasonTRCrossIndex], $seasonTR->league_eliminationseasonApi->season_ptr->id, $seasonRType, ($seasonTR->league_eliminationseasonApi->season_ptr->year.' - '.$seasonTR->league_eliminationseasonApi->season_ptr->name), $seasonTR->league_eliminationseasonApi->season_ptr->year );

                    }

                    if(count($seasonTRCross)-1 > $seasonTRCrossIndex){
                        $seasonTRCrossIndex++;
                    }
                }


                $all_seasonsTR = $league_province->league->season->where('active', true);
                foreach ($all_seasonsTR as $seasonTR) {

                    if($seasonTR->polymorphic_ctype_id == 32){ $seasonEType = 'elimination'; }else if($seasonTR->polymorphic_ctype_id == 33){ $seasonEType = 'fixture'; }else if($seasonTR->polymorphic_ctype_id == 34){ $seasonEType = 'point'; }
                    $is_valid = array_search($seasonTR->id, array_column($seasonTRList, 1));
                    // dump('---('.end($seasonTRCross).') (id:'.$seasonTR->id.') - '.$seasonTR->year.' - '.$seasonTR->name.' - '.$seasonEType.' - '.$is_valid.' - '.( ($is_valid !== false) ? $is_valid : 'false') );
                    
                    if($is_valid === false){
                        $seasonTRList[] = array(end($seasonTRCross), $seasonTR->id, $seasonEType, ($seasonTR->year.' - '.$seasonTR->name), $seasonTR->year);
                    }
                }
                if($visibleLog){
                    dump($seasonTRList);
                }

            }else{
                dump('böyle bişey olmamalıydı !!!');                
                dump('???? '.$league_province->league->name);
            }
            

        }
        /*********** Sezon sıralama ve çarpan değerleri hesaplaması ***********/
        if($visibleLog){
            dump('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'); 
        }

        /*
            - Sezon Çarpanı
        */
        $matchTotalPointArray = array();

        foreach ($user->player->match_player as $match_player) {

            $match = $match_player->match;
            $season = $match_player->match->season;

            //dump('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
            //dump('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
            //dump($match);
            //dump($season);
            //dump('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
            //dump('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');



            if(!empty($season)){
                $is_valid = array_search($season->id, array_column($seasonList, 1));
                dump($season->id);
                if($is_valid !== false){

                    
                    if($visibleLog){
                        dump('Sezon ID: '.$seasonList[$is_valid][1].' | Adı: '.$seasonList[$is_valid][3].' | Çarpanı: '.$seasonList[$is_valid][0]);
                        dump('Maç ID: '.$match_player->match_id);

                        dump('Oyuncu oynadığı maç : +'.$pointMatch);
                        dump('Oyuncu Gol : ['.$match_player->match_action_goal->count().'] * +'.$pointGoal);
                        dump('Oyuncu Asist : ['.$match_player->match_action_assist->count().'] * +'.$pointAssist);
                        dump('Oyuncu Kurtarış : ['.((is_null($match_player->saving) ? 0 : $match_player->saving)).'] * +'.$pointSave);
                        dump('Oyuncu Sarı Kart : ['.$match_player->match_action_yellow_card->count().'] * '.$pointYellowCard);
                        dump('Oyuncu Kırmızı Kart : ['.$match_player->match_action_red_card->count().'] * '.$pointRedCard);
                    }

                    $panoramaTotalPoint = 0;
                    foreach ($match_player->match_player_panorama as $panorama) {
                        $panoramaTotalPoint += $panoramaPoint[$panorama->panorama_id][0];
                        if($visibleLog){
                            dump($panoramaPoint[$panorama->panorama_id][1].' +'.$panoramaPoint[$panorama->panorama_id][0]);
                        }
                    }
                    
                    $matchPoint = $seasonList[$is_valid][0] * (
                        $pointMatch + 
                        ($match_player->match_action_goal->count() * $pointGoal) + 
                        ($match_player->match_action_assist->count() * $pointAssist) + 
                        (((is_null($match_player->saving) ? 0 : $match_player->saving)) * $pointSave) + 
                        ($match_player->match_action_yellow_card->count() * $pointYellowCard) + 
                        ($match_player->match_action_red_card->count() * $pointRedCard) +
                        $panoramaTotalPoint
                    );

                    $matchTotalPointArray[] = array('match_id' => $match_player->match_id, 'point' => $matchPoint);
                    //dump("match id: ".$match_player->match_id." -- (".$season->id.") (".$seasonType.") (".Carbon::parse($season->start_date)->format('d.m.Y')." / ".Carbon::parse($season->end_date)->format('d.m.Y').") ".$season->name);
                    //$this->seasonCarpan($season);
                    if($visibleLog){
                        dump('TOPLAM PUAN: '. $matchPoint);
                        dump('***************************************************************************');
                    }
                }
            }
        }

        //dump($seasonList);

        foreach ($seasonList as $seasonl) {
            
            //dump($seasonl);
            $playerHistory = TeamPlayerTeamHistory::where('player_id', $player->id)->where('season_id', $seasonl[1])->get();
            //dump($playerHistory);

            $playerHistoryPoint = 0;

            //(2019 ve öncesinde panorama kaydı olmadığından bu bölümde hesaplama dışı bırakıldı.)
            foreach ($playerHistory as $history) {
                
                $playerHistoryPoint += end($seasonCross) * (
                    ($history->match * $pointMatch) + 
                    ($history->goal * $pointGoal) + 
                    (((is_null($history->gk_saving) ? 0 : $history->gk_saving)) * $pointSave) + 
                    ($history->yellow_card * $pointYellowCard) + 
                    ($history->red_card * $pointRedCard)
                );
                if($visibleLog){
                    dump('Hesaplanmış Tüm Sezon Puanlarıdır.');
                    dump('Sezon ID: '.$seasonl[1].' | Adı: '.$seasonl[3].' | Çarpanı: '.$seasonl[0]);

                    dump('Oyuncu oynadığı maç : MS['.$history->match.'] * +'.$pointMatch);
                    dump('Oyuncu Gol : ['.$history->goal.'] * +'.$pointGoal);
                    dump('Oyuncu Kurtarış : ['.((is_null($history->gk_saving) ? 0 : $history->gk_saving)).'] * +'.$pointSave);
                    dump('Oyuncu Sarı Kart : ['.$history->yellow_card.'] * '.$pointYellowCard);
                    dump('Oyuncu Kırmızı Kart : ['.$history->red_card.'] * '.$pointRedCard);
                }


                $matchTotalPointArray[] = array('season_id' => $seasonl[1], 'point' => $playerHistoryPoint);

                if($visibleLog){
                    dump('TOPLAM PUAN: '. $playerHistoryPoint);
                    dump('***************************************************************************');
                }

            }

        }

        foreach ($seasonTRList as $seasonlTR) {
            
            //dump($seasonlTR);
            $playerHistoryTR = TeamPlayerTeamHistory::where('player_id', $player->id)->where('season_id', $seasonlTR[1])->get();
            //dump($playerHistoryTR);

            $playerHistoryTRPoint = 0;

            foreach ($playerHistoryTR as $historyTR) {

                $playerHistoryTRPoint += end($seasonTRCross) * (
                    ($historyTR->match * $pointMatch) + 
                    ($historyTR->goal * $pointGoal) + 
                    (((is_null($historyTR->gk_saving) ? 0 : $historyTR->gk_saving)) * $pointSave) + 
                    ($historyTR->yellow_card * $pointYellowCard) + 
                    ($historyTR->red_card * $pointRedCard)
                );

                if($visibleLog){
                    dump('Hesaplanmış Tüm Sezon Puanlarıdır. (Türkiye Finalleri)');
                    dump('Sezon ID: '.$seasonlTR[1].' | Adı: '.$seasonlTR[3].' | Çarpanı: '.$seasonlTR[0]);

                    dump('Oyuncu oynadığı maç : MS['.$historyTR->match.'] * +'.$pointMatch);
                    dump('Oyuncu Gol : ['.$historyTR->goal.'] * +'.$pointGoal);
                    dump('Oyuncu Kurtarış : ['.((is_null($historyTR->gk_saving) ? 0 : $historyTR->gk_saving)).'] * +'.$pointSave);
                    dump('Oyuncu Sarı Kart : ['.$historyTR->yellow_card.'] * '.$pointYellowCard);
                    dump('Oyuncu Kırmızı Kart : ['.$historyTR->red_card.'] * '.$pointRedCard);
                }

                $matchTotalPointArray[] = array('season_id_tr' => $seasonlTR[1], 'point' => $playerHistoryTRPoint);

                if($visibleLog){
                    dump('TOPLAM PUAN: '. $playerHistoryTRPoint);
                    dump('***************************************************************************');
                }

            }

        }

        if($visibleLog){
            dump('--------------------------');
            dump('Maç Başı Puan Özeti');
            dump($matchTotalPointArray);
        }

        foreach ($matchTotalPointArray as $value) {
            //dump($value['point']);
            $pointTotalPlayer += $value['point'];
            //dump($pointTotalPlayer);
        }

        /*
        $calculetedTotalPoint = (round($pointTotalPlayer / 10000)*10);
        
        if($calculetedTotalPoint < 1000){
            dump('GENEL TOPLAM PUAN: '. $calculetedTotalPoint .'K RB$');
        }else{
            $calculetedTotalPoint = round($calculetedTotalPoint/100)/10;
            dump('GENEL TOPLAM PUAN: '. $calculetedTotalPoint .'M RB$');
        }
        */

        if($pointTotalPlayer < 100000){
            $pointTotalPlayer = 100000;
        }
        
        return $pointTotalPlayer;
        //dd('GENEL TOPLAM PUAN: '.$pointTotalPlayer.' RB$');
    }

    public function videoUrlDegistir($offset, $limit) {

        $match_videos = MatchVideo::where('full_embed_code', 'ilike', '%bo3studio%')->orWhere('summary_embed_code', 'ilike', '%bo3studio%')->offset($offset)->limit($limit)->orderBy('id')->get();

        $match_videos_count = MatchVideo::count();
        //dump($users);
        echo 'OK ---- offset:'.$offset.' - limit:'.$limit.' - count:'.$match_videos_count.'</br>';
        echo '<a href="'.url('/videourldegistir').'/'.($offset+$limit).'/'.$limit.'">Sıradaki</a> (offset:'.($offset+$limit).' - limit:'.$limit.') <br>';
        
        foreach ($match_videos as $video) {
            if(str_contains($video->full_embed_code, 'bo3studio')){
                $video->full_embed_code = str_replace_first('bo3studio.com', 'api.yazbu.com', $video->full_embed_code);
                $video->save();
            }
            if(str_contains($video->summary_embed_code, 'bo3studio')){
                $video->summary_embed_code = str_replace_first('bo3studio.com', 'api.yazbu.com', $video->summary_embed_code);
                $video->save();
            }
        }

        echo '<script>document.querySelector("body").onload = function(){
            console.log("asd");
            setTimeout(function(){  window.location = "'.url('/videourldegistir').'/'.($offset+$limit).'/'.$limit.'"; }, 5000);
        };</script>';
    }

    public function sezonIstatistikHesapla($sezon_id, $offset, $limit, $mode = null) {

        echo 'OK ---- offset:'.$offset.' - limit:'.$limit.'</br>';


        $match_completed_status = 'completed:true';

        //$season = Season::find($sezon_id);
        $matches = Match::where('season_id', $sezon_id)->where('completed', true)->offset($offset)->limit($limit)->orderBy('date')->orderBy('time')->get();

        dump(count($matches));
        
        foreach ($matches as $model) {
            echo '---------------------------------------------------------------------------------------------------------------------------------- <br>';
            echo '--> Maç ID: '.$model->id.' <br>';
            $this->players_history_update($model, $match_completed_status);
        }

        echo '<script>document.querySelector("body").onload = function(){
            console.log("asd");
            setTimeout(function(){  window.location = "'.url('/sezonistatistikhesapla').'/'.$sezon_id.'/'.($offset+$limit).'/'.$limit.'"; }, 5000);
        };</script>';
        https://panel.rakipbul.com/sezonistatistikhesapla/2438/450/50
    }

    public function players_history_update($match, $match_completed_status){
        //Maç tamamlandı durumuna alındığında oyuncuların tüm sezon istatistikleri
        //Maç silme işleminde eğer tamamlanan bir mç ise hesaplama tekrar yapılır
        //Maç güncellemesinde tamamlandı moduna alınan durumlarda yeniden hesaplama yapılır
        //dump($match);
        $season_id = $match->season_id;
        $match_id = $match->id;
        
        $statisticsArr = array();
        //Maçın oyuncularının player id lerini al, misafirleri dışla
        $all_player_IDs = array();
        
        foreach ($match->match_player_is_not_guest as $mplayer) {

            $sArr = array();
            $sArr['player_id'] = $mplayer->player_id;
            $sArr['team_id'] = $mplayer->team_id;
            $sArr['season_id'] = $season_id;

            $sArr['rate_total'] = 0;

            $sArr['gk_save'] = 0;
            $sArr['rating'] = 0;
            $sArr['match'] = 0;
            $sArr['goal'] = 0;
            $sArr['yellow_card'] = 0;
            $sArr['red_card'] = 0;

            $statisticsArr[] = collect($sArr);
            $all_player_IDs[] = $mplayer->player;
        }

        //dump($statisticsArr);
        //dump($all_player_IDs);
        
        foreach ($all_player_IDs as $player) {
            //dump($player->id.'----------------');
            $player->match_player = $player->match_player->filter(function ($value) use ($season_id, $player) {
                return $value->match->season_id == $season_id && $value->team_id == $player->team_id && $value->match->completed == true;
            });
        }

        //dump('----');
        //dump($all_player_IDs);
        $statistics = collect($statisticsArr);
        //dump('******');
        //dump($statistics);

        foreach ($all_player_IDs as $player) {
            $pstat = $statistics->where('player_id', $player->id)->first();
            foreach ($player->match_player as $mplayer) {

                $pstat['rate_total'] = $pstat['rate_total'] + $mplayer->rating;
                $pstat['gk_save'] = $pstat['gk_save'] + $mplayer->saving;
                $pstat['match'] = $pstat['match'] + 1;

                if(is_null($match->completed_status)){
                    foreach ($mplayer->match_action as $maction) {
                        if($maction->type != 'critical'){
                            $pstat[$maction->type] = $pstat[$maction->type] + 1;
                        }
                    }
                }
            }

            if($pstat['rate_total'] == 0){
                $pstat['rating'] = 0;
            }else{
                $pstat['rating'] = number_format((float)($pstat['rate_total'] / $pstat['match']), 2, '.', '');
            }
        }

        
        foreach ($all_player_IDs as $player) {
            $tpth = $player->team_playerteamhistory->where('season_id', $season_id)->where('team_id', $player->team_id)->first();
            $stats = $statistics->where('season_id', $season_id)->where('team_id', $player->team_id)->where('player_id', $player->id)->first();
            
            if(empty($tpth)){
                $tpth = new TeamPlayerTeamHistory();
                $tpth->season_id = $season_id;
                $tpth->team_id = $player->team_id;
                $tpth->player_id = $player->id;
            }

            if(!empty($stats)){
                echo "<br>player_id: ".$stats['player_id']." - ".
                "team_id: ".$stats['team_id']." - ".
                "season_id: ".$stats['season_id']."<br>";

                echo "gk_save: ".$stats['gk_save']." - ".
                "rating: ".$stats['rating']." - ".
                "match: ".$stats['match']." - ".
                "goal: ".$stats['goal']." - ".
                "yellow_card: ".$stats['yellow_card']." - ".
                "red_card: ".$stats['red_card']." <br>";

                $tpth->gk_save = $stats['gk_save'];
                $tpth->rating = $stats['rating'];
                $tpth->match = $stats['match'];
                $tpth->goal = $stats['goal'];
                $tpth->yellow_card = $stats['yellow_card'];
                $tpth->red_card = $stats['red_card'];
                $tpth->save();

            }
            // dump($tpth);
        }
        
        //dump('İlgili Player ID leri');
        //dump($statistics);
    }

    public function fsLeague() {

        $formaStar = new FormaStarClass();

        $leagueId = 103;
        $name = "İstanbul Demo";
        $minPlayer = 7;
        $maxPlayer = 15;
        $cityId = 34;
        $activeSeason = 20231;

        //dd($formaStar->addLeague($leagueId, $name, $minPlayer, $maxPlayer, $cityId, $activeSeason));
        dd($formaStar->updateLeague($leagueId, $name, $minPlayer, $maxPlayer, $cityId, $activeSeason));
    }

    public function fsSeason() {

        $formaStar = new FormaStarClass();

        $leagueId = 103;
        $cityId = 34;
        $name = "Sezon Bahar";
        $year = 2023;
        $type = 1;
        $seasonId = 3002;

        //dd($formaStar->addSeason($leagueId, $cityId, $name, $year, $type, $seasonId));
        dd($formaStar->updateSeason($leagueId, $cityId, $name, $year, $type, $seasonId));
    }

    public function fsGroup() {

        $formaStar = new FormaStarClass();

        $leagueId = 103;
        $seasonId = 3001;
        $name = "Istanbul Sezon Bahar A Grubu 1";
        $groupId = 4001;

        //dd($formaStar->addGroup($leagueId, $seasonId, $name, $groupId));
        dd($formaStar->updateGroup($leagueId, $seasonId, $name, $groupId));
    }

    public function fsTeam() {

        $formaStar = new FormaStarClass();

        $leagueId = 103;
        $groupId = 4001;
        $seasonId = 3001;
        $name = "Team 2"; //"Team 1";
        $cityId = 34;
        $teamId = 5002; //5001;
        $profileUrl = "https://panel.businesscup.com.tr/media/team-logos/4b3c85db89719320e4f4abcc4e1d7f40.jpg";
        $userId = 6002; //6001; //Kaptan id
        $email = "assdaad@asd.com";
        $mobile = "0555 555 55 55";

        dd($formaStar->addTeam($leagueId, $groupId, $seasonId, $name, $cityId, $teamId, $profileUrl, $userId, $email, $mobile));
        //dd($formaStar->updateTeam($leagueId, $groupId, $seasonId, $name, $cityId, $teamId, $profileUrl, $userId, $email, $mobile));
    }

    public function fsPlayer() {

        $formaStar = new FormaStarClass();

        $firstName = "Oyuncu 2";
        $lastName = "Soyisim";
        $email = "asd2@asd.com";
        $mobile = "0555 555 55 55";
        $birthDate = 19990101;
        $teamId = 5002;
        $profileUrl = "https://panel.businesscup.com.tr/media/team-logos/4b3c85db89719320e4f4abcc4e1d7f40.jpg";
        $roleCode = 1;
        $userId = 7011; //7001 - 7002 - 7003 - 7004 / 7011 - 7012 - 7013 - 7014
        $height = 177;
        $weight = 77;

        //dd($formaStar->addPlayer($firstName, $lastName, $email, $mobile, $birthDate, $teamId, $profileUrl, $roleCode, $userId, $height, $weight));
        dd($formaStar->updatePlayer($firstName, $lastName, $email, $mobile, $birthDate, $teamId, $profileUrl, $roleCode, $userId, $height, $weight));
    }

    public function fsFixture() {

        $formaStar = new FormaStarClass();

        $leagueId = 103;
        $seasonId = 3001;
        $groundId = 1;
        $team1Id = 5001;
        $team2Id = 5002;
        $matchDate = 20231011;
        $matchTime = "11:00";
        $matchId = 1002;

        //dd($formaStar->addFixture($leagueId, $seasonId, $groundId, $team1Id, $team2Id, $matchDate, $matchTime, $matchId));
        dd($formaStar->updateFixture($leagueId, $seasonId, $groundId, $team1Id, $team2Id, $matchDate, $matchTime, $matchId));
    }

    public function fsGetMatchData() {

        $formaStar = new FormaStarClass();

        $leagueId = 72;
        $matchId = 132023;

        dd($formaStar->getMatchData($leagueId, $matchId));
    }

    public function fsGetMatchActionData() {

        $formaStar = new FormaStarClass();

        $leagueId = 72;
        $matchId = 132023;

        dd($formaStar->getMatchActionData($leagueId, $matchId));
    }

    public function fsGetMatchTeamData() {

        $formaStar = new FormaStarClass();

        $leagueId = 72;
        $matchId = 132023;

        dd($formaStar->getMatchTeamData($leagueId, $matchId));
    }

    public function fsDakikaDuzelt() {

        $matches = Match::whereNotNull('fsdata')->get();
        
        foreach ($matches as $match) {

            $fs_result = null;
            $fs_result = json_decode($match->fsdata);
            // dump($fs_result->resultMatchActionData[8]);

            foreach ($fs_result->resultMatchActionData as $fs_action) {

                $cont = false;
                $actionName = '';
                if($fs_action->actionCode == 5) {// "actionCode": 5, "actionName": "GOL", "actionDesc": "Gol",
                    $actionName = 'goal';
                    $cont = true;
                }else if($fs_action->actionCode == 60 && $fs_action->nextActionCode == 0) {// Penaltı
                    $actionName = 'goal';
                    $cont = true;
                }else if($fs_action->actionCode == 15) {// "actionCode": 15, "actionName": "AST", "actionDesc": "Asist",
                    $actionName = 'assist';
                    $cont = true;
                }else if($fs_action->actionCode == 50) {// "actionCode": 50, "actionName": "SK", "actionDesc": "Sarı Kart",
                    $actionName = 'yellow_card';
                    $cont = true;
                }else if($fs_action->actionCode == 55) {// "actionCode": 55, "actionName": "KK", "actionDesc": "Kırmızı Kart",
                    $actionName = 'red_card';
                    $cont = true;
                }


                $fs_actionTime = 0;
                if($fs_action->period == 1){
                    $fs_actionTime = intval($fs_action->periodTime/100);
                }else if($fs_action->period == 2){
                    $fs_actionTime = intval($fs_action->periodTime/100)+25;
                }
                    
                if($cont){
                    dump($fs_action->matchId.' - '.$fs_action->teamId.' - '.$fs_action->userId.' - '.$fs_action->actionName.' - '.$fs_action->period.' - '.$fs_action->periodTime);

                    $match_player_id = null;
                    $team_id = null;
                    if(empty($fs_action->userId)){
                        $team_id = $fs_action->teamId;
                    }else{
                        $player_id = User::find($fs_action->userId)->player->id;
                        $mplayer = MatchPlayer::where('match_id', $fs_action->matchId)->where('team_id', $fs_action->teamId)->where('player_id', $player_id)->first();
                        if(!empty($mplayer)){
                            $match_player_id = $mplayer->id;
                        }
                        $team_id = $fs_action->teamId;
                    }
                    
                    dump($fs_action->matchId.' - '.$fs_action->teamId.' - '.$match_player_id.' - '.$actionName.' - '.$fs_actionTime);

                    if(!empty($match_player_id)){
                        $match_action = MatchAction::where('match_id', $fs_action->matchId)->where('team_id', $fs_action->teamId)->where('match_player_id', $match_player_id)->where('type', $actionName)->where('minute', $fs_actionTime)->whereNull('updated_at')->first();
                        dump('1 count '.$match_action);
                    }else{
                        $match_action = MatchAction::where('match_id', $fs_action->matchId)->where('team_id', $fs_action->teamId)->where('type', $actionName)->where('minute', $fs_actionTime)->whereNull('match_player_id')->whereNull('updated_at')->first();
                        dump('2 count '.$match_action);
                    }

                    
                    if(!empty($match_action)){
                        $fs_actionTimeNew = 0;
                        if($fs_action->period == 1){
                            $fs_actionTimeNew = intval($fs_action->periodTime/60);
                        }else if($fs_action->period == 2){
                            $fs_actionTimeNew = intval($fs_action->periodTime/60)+25;
                        }

                        if($fs_actionTimeNew == 0){
                            $fs_actionTimeNew = 1;
                        }
                        
                        dump($fs_actionTimeNew);
                        if(!empty($fs_actionTimeNew)){
                            $match_action->minute = $fs_actionTimeNew;
                            $match_action->updated_at = Carbon::parse('2023-11-03');
                            $match_action->save();

                        }
                    }else{
                        dump('bos');
                    }
                    
                    dump('------------------------------------------');
                }

            }

        }

    }

}
