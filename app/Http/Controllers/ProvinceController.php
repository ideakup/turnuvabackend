<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Validator;

use Carbon\Carbon;
use App\User;

use App\Province;

class ProvinceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	return view('dashboard');
    }

    public function crud($id = null)
    {
        $province = NULL;
        if(!is_null($id)){
            $province = Province::find($id);
        }
        return view('dashboard', array('model_data' => $province));
    }

    public function save(Request $request)
    {
        //dd($request->input());
        $text = "";
        if ($request->crud == 'delete') {

            $model = Province::find($request->id);

            if($model->conference->count() == 0 && $model->district->count() == 0 && $model->ground->count() == 0 && $model->league_province->count() == 0&& $model->nostalgia->count() == 0 && $model->profile->count() == 0 && $model->team->count() == 0){
                $model->delete();
                $text = 'Başarıyla Silindi...';
            }else{
                $text = 'Bu ilçe en az bir konferans, ilçe, halı saha, lig, nostalji, profil veya takıma atandığı için silinemedi!';
                return \Redirect::back()->withInput()->with('message', array('text' => $text, 'status' => 'error'));
            }

        }else{

            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $model = new Province();
            }else if($request->crud == 'edit'){
                $model = Province::find($request->id);
            }

            $rules = array();
            $formConfig = config('forms.'.$request->segment);

            if(!is_null($formConfig)){
                foreach ($formConfig as $key => $value){
                    if(!empty(data_get($value, 'validation'))){
                        $rules[$key] = data_get($value, 'validation');
                    }
                }
            }
            
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
               return \Redirect::back()->withErrors($validator)->withInput();
            }

            $slugControl = Province::where('slug', str_slug($request->name, '-'))->count();

            $model->name = $request->name;
            if($slugControl == 0){
                $model->slug = str_slug($request->name, '-');
            }else{
                $model->slug = str_slug($request->name, '-').'-'.str_random(3);
            }
            $model->plate_code = $request->plate_code;

            $model->save();

            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $text = 'Başarıyla Eklendi...';
            }else if($request->crud == 'edit'){
                $text = 'Başarıyla Güncellendi...';
            }
        }

        return redirect($request->segment)->with('message', array('text' => $text, 'status' => 'success'));
    }
}
