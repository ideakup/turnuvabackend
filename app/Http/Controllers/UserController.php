<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Validator;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use Carbon\Carbon;
use App\User;
use App\Profile;
use App\Player;
use App\UserRoleHasProvince;

class UserController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        if(request()->segment(1) == 'users' && !Auth::user()->hasPermissionTo('view_user')){
            return redirect('/dashboard')->with('message', array('text' => "Bu sayfaya ulaşmak için yetkiniz bulunmuyor!", 'status' => 'error'));
        }
    	return view('dashboard');
    }

    public function crud($id = null) {
        if(
            (request()->segment(2) == 'edit' && !Auth::user()->hasPermissionTo('change_user')) || 
            (request()->segment(2) == 'delete' && !Auth::user()->hasPermissionTo('delete_user'))
        ) {
            return redirect('/roles')->with('message', array('text' => "Bu sayfaya ulaşmak için yetkiniz bulunmuyor!", 'status' => 'error'));
        }

        $user = NULL;
        if(!is_null($id)) {
            $user = User::find($id);
        }
        return view('dashboard', array('model_data' => $user));
    }

    public function save(Request $request) {
        // dd($request->input());
        $text = "";
        if ($request->crud == 'delete'){

            $model = User::find($request->id);
            $model->is_active = false;
            $model->save();
         
            $text = 'Başarıyla Silindi...';

        }else{

            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $model = new User();
            }else if($request->crud == 'edit'){
                $model = User::find($request->id);
            }

            $rules = array();
            $formConfig = config('forms.'.$request->segment);

            if(!is_null($formConfig)){
                foreach ($formConfig as $key => $value){
                    if(!empty(data_get($value, 'validation'))){
                        $rules[$key] = data_get($value, 'validation');
                    }
                }
            }
            
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                return \Redirect::back()->withErrors($validator)->withInput();
            }

            if ($request->crud == 'add') {
                $model->password = 'pbkdf2_sha256$150000$F6IZcQ0L59An$9KsQ7wipJRalPRzydg3m/8Get2x02+U/HqGAfJd+6ro=';
                $model->is_superuser = false;
                $model->is_staff = false;
                $model->email_confirmed = false;
                $model->phone_confirmed = false;
                $model->is_active = true;
                $model->date_joined = Carbon::now();
            }

            $model->username = $request->email;
            $model->first_name = $request->first_name;
            $model->last_name = $request->last_name;
            $model->email = $request->email;
            $model->phone = $request->phone;
            $model->save();

            if ($request->crud == 'add') {
                $profile = new Profile();
                $profile->user_id = $model->id;
            }else if($request->crud == 'edit'){
                $profile = $model->profile;
            }

            //$profile->identity_number = $request->identity_number;
            $profile->birth_date = Carbon::parse($request->birth_date)->format('Y-m-d');
            $profile->province_id = $request->province;
            //$profile->district_id = $request->district_id;
            $profile->save();
        
            $player = $model->player;

            if(empty($player) && $request->crud == 'edit'){
                $player = new Player();
                $player->user_id = $model->id;
                $player->value = 100000;
                $player->old_system_value = 0;
            }

            if (!empty($player)) {
                $player->facebook = $request->facebook;
                $player->twitter = $request->twitter;
                $player->instagram = $request->instagram;
                $player->number = $request->number;
                $player->foot = $request->foot;
                $player->weight = $request->weight;
                $player->height = $request->height;
                $player->position_id = $request->position_id;
                $player->position2_id = $request->position2_id;
                $player->position3_id = $request->position3_id;
                $player->save();
            }

            if($model->getRoleNames()->count() > 0){
                foreach ($model->getRoleNames() as $role_name) {
                    $model->removeRole($role_name);
                }
                foreach ($model->roleprovince as $role_province) {
                    $role_province->delete();
                }
            }
            if(!empty($request->role)){
                $role = Role::find($request->role);
                $model->assignRole($role->name);

                if(!empty($request->role_province)){
                    foreach ($request->role_province as $role_province) {
                        $roleProvince = new UserRoleHasProvince();
                        $roleProvince->user_id = $model->id;
                        $roleProvince->province_id = $role_province;
                        $roleProvince->save();
                    }
                }
            }

            /* ADD - EDIT */
            if ($request->crud == 'add') {
                $text = 'Başarıyla Eklendi...';
            }else if($request->crud == 'edit'){
                $text = 'Başarıyla Güncellendi...';
            }
        }

        return redirect($request->segment)->with('message', array('text' => $text, 'status' => 'success'));
    }

    public function userPhotoDelete($id) {
        $user = User::find($id);
        $user->image = null;
        $user->save();

        $text = 'Kullanıcı Fotoğrafı Başarıyla Silindi...';
        return redirect('users/edit/'.$id.'?t=tab_02')->with('message', array('text' => $text, 'status' => 'success'));
    }

    public function password($id = null) {
        $text = "Bu sayfaya ulaşmak için yetkiniz bulunmuyor!";
        if(
            (request()->segment(2) == 'edit' && !Auth::user()->hasPermissionTo('change_user')) ||
            (request()->segment(2) == 'delete' && !Auth::user()->hasPermissionTo('delete_user'))
        ){
            return redirect('/roles')->with('message', array('text' => $text, 'status' => 'error'));
        }

        $user = NULL;
        if(!is_null($id)){
            $user = User::find($id);
        }
        return view('dashboard', array('model_data' => $user));
    }

    public function save_password(Request $request) {
        $text = "";
        $model = User::find($request->id);
        $model->password = $this->django_password_create($request->input('password'));
        $model->save();

        $text = 'Başarıyla Güncellendi...';
        return redirect($request->segment)->with('message', array('text' => $text, 'status' => 'success'));
    }

    public function django_password_create(string $password) {
        $algorithm = "pbkdf2_sha256";
        $iterations = 150000;

        $salt = base64_encode(openssl_random_pseudo_bytes(9));
        //$salt = base64_encode($salt);

        $hash = hash_pbkdf2("SHA256", $password, $salt, $iterations, 0, true);    
        $toDBStr = $algorithm ."$". $iterations ."$". $salt ."$". base64_encode($hash);

        return $toDBStr;
    }

}
