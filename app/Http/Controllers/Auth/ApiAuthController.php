<?php

namespace App\Http\Controllers\Auth;

use App\Classes\SmsClass;

use App\Http\Controllers\Controller;

use Exception;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

use App\User;

class ApiAuthController extends Controller
{
    
	public function login (Request $request) {

		if(empty($request->get('username')) || empty($request->get('password'))){
            $errorArr = array();
            if (empty($request->get('username'))) {
                $errorArr['username'] = array('Bu alan zorunlu.');
            }
            if (empty($request->get('password'))) {
                $errorArr['password'] = array('Bu alan zorunlu.');
            }
            return response($errorArr, 400);
        }
	
        // Get user record
        // $user = User::where('email', $request->get('username'))->first();
        $user = User::select('users.*', 'profiles.identity_number as identity_number')->
        leftJoin('profiles', 'users.id', '=', 'profiles.user_id')->
        where('email', $request->get('username'))->
        orWhere('phone', $request->get('username'))->
        orWhere('identity_number', $request->get('username'))->first();
        
        if(is_null($user)) {
            return response(array('detail' => 'No active account found with the given credentials'), 401);
        }

        $pv = $this->django_password_verify(
            $request->get('password'),
            $user->password
        );

        if(!$pv) {
            return response(array('detail' => 'No active account found with the given credentials'), 401);
        }

        if(!$user->active && !$user->phone_confirmed){
            $user->phone_code = mt_rand(100000, 999000); // SMS ONAYLANDIĞINDA NULL OLACAK;
            $user->save();
            $smsHelper = new SmsClass();
            $smsHelper->smsSend($user->phone, $user->phone_code);
            return response(array('detail' => 'smsactivationreq', 'phone' => $user->phone), 401);
        }

        $response = array('refresh' => $user->createToken('refresh')->accessToken, 'access' => $user->createToken('access')->accessToken);

        // Redirect home page
        return response($response, 200);
	}

	public function logout (Request $request) {
	    $token = $request->user()->token();
	    $token->revoke();
	    $response = ['message' => 'You have been successfully logged out!'];
	    return response($response, 200);
	}

    public function register (Request $request) {
    	/*
	    $validator = Validator::make($request->all(), [
	        'name' => 'required|string|max:255',
	        'email' => 'required|string|email|max:255|unique:users',
	        'password' => 'required|string|min:6|confirmed',
	    ]);
	    if ($validator->fails())
	    {
	        return response(['errors'=>$validator->errors()->all()], 422);
	    }
	    $request['password']=Hash::make($request['password']);
	    $request['remember_token'] = Str::random(10);
	    $user = User::create($request->toArray());
	    $token = $user->createToken('Laravel Password Grant Client')->accessToken;
	    $response = ['token' => $token];
	    return response($response, 200);
	    */
	}

    public function changePassword(Request $request) {
        $user = Auth::user();
        $currentPassword = $request->input('currentPassword');
        $password = $request->input('password');
        $passwordRepeat = $request->input('passwordRepeat');

        if(strlen($request->input('password')) < 5){
            return response(array('message' => 'Şifre en az 5 karakter olmalıdır'), 401);
        }

        $pv = $this->django_password_verify(
            $currentPassword,
            $user->password
        );

        if(!$pv){
            return response(array('message' => 'Geçerli parolayı yanlış girdiniz'), 401);
        }
        
        $pv2 = $this->django_password_create($password);

        $user->password = $pv2;
        $user->save();

        return response(array('message' => 'Tamam'), 200);
    }

    public function django_password_create(string $password) {
        $algorithm = "pbkdf2_sha256";
        $iterations = 150000;

        $salt = base64_encode(openssl_random_pseudo_bytes(9));
        //$salt = base64_encode($salt);

        $hash = hash_pbkdf2("SHA256", $password, $salt, $iterations, 0, true);    
        $toDBStr = $algorithm ."$". $iterations ."$". $salt ."$". base64_encode($hash);

        return $toDBStr;
    }

	public function django_password_verify(string $password, string $djangoHash): bool {
        $pieces = explode('$', $djangoHash);
        if (count($pieces) !== 4) {
            throw new Exception("Illegal hash format");
        }
        list($header, $iter, $salt, $hash) = $pieces;
        // Get the hash algorithm used:
        if (preg_match('#^pbkdf2_([a-z0-9A-Z]+)$#', $header, $m)) {
            $algo = $m[1];
        } else {
            throw new Exception(sprintf("Bad header (%s)", $header));
        }
        if (!in_array($algo, hash_algos())) {
            throw new Exception(sprintf("Illegal hash algorithm (%s)", $algo));
        }

        $calc = hash_pbkdf2(
            $algo,
            $password,
            $salt,
            (int) $iter,
            32,
            true
        );

        return hash_equals($calc, base64_decode($hash));
    }
}
