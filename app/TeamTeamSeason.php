<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class TeamTeamSeason extends Model
{
    protected $table = 'team_teamseason';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;

    //public.team_teamseason.team_teamseason_season_id_8269f079_fk_seasons_id
    public function season()
    {
        return $this->belongsTo('App\Season', 'season_id', 'id');
    }

	//public.team_teamseason.team_teamseason_squad_locked_player_id_d24c4ce7_fk_players_id

	//public.team_teamseason.team_teamseason_team_id_aaafe647_fk_teams_id
    public function team()
    {
        return $this->hasOne('App\Team', 'id', 'team_id');
    }

    //public.reservations.reservations_teamseason_id_5f8d4381_fk_team_teamseason_id

	//public.team_teamseason_add_point_log.team_teamseason_add__teamseason_id_ee7fb7f6_fk_team_team
    public function teamseason_add_point_log()
    {
        return $this->hasMany('App\TeamTeamSeasonAddPointLog', 'teamseason_id', 'id');
    }

}