<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class LeaguesProvince extends Model
{
    protected $table = 'leagues_provinces';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;

    public function league()
    {
        return $this->belongsTo('App\League', 'league_id', 'id');
    }

    public function province()
    {
        return $this->belongsTo('App\Province', 'province_id', 'id');
    }
    
}