<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class TeamPotential extends Model
{
    protected $table = 'team_potentials';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;

    public function team_survey()
    {
        return $this->belongsTo('App\TeamSurvey', 'id', 'potential_id');
    }
    
}