<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class TeamTeamPlayer extends Model
{
    protected $table = 'team_teamplayer';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;


    public function player()
    {
        return $this->belongsTo('App\Player', 'player_id', 'id');
    }


    public function team()
    {
        return $this->belongsTo('App\Team', 'team_id', 'id');
    }

}