<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class MatchVideo extends Model
{
    protected $table = 'match_video';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;

    // public.match_video.match_video_match_id_47d9c0bb_fk_matches_id
    public function match()
    {
        return $this->belongsTo('App\Match', 'id', 'match_id');
    }

}