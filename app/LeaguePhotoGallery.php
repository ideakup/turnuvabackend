<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class LeaguePhotoGallery extends Model
{
    protected $table = 'league_photo_gallery';
    protected $dateFormat = 'Y-m-d H:i:sO';
    public $timestamps = false;

}