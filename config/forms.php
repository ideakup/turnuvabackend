<?php

return [

    'conferences' => [
        'name' => [
            'type' => 'text',
            'validation' => 'required|max:100',
            'title' => 'words.name',
            'placeholder' => 'words.name',
            'description' => '',
        ],

        'province_id' => [
            'type' => 'select2',
            'validation' => 'required',

            'dataSource' => 'App\Province',
            
            'title' => 'words.city',
            'description' => '',
        ],
        
        'district_id' => [
            'type' => 'select2',
            'validation' => 'required',
            'multiple' => 'multiple',

            'relationship' => 'conferences_district', //model function
            'relation_col' => 'district_id',
            'dataSource' => 'App\District',

            'ajax' => true,
            'ajax_url' => '/getDistrictsF',
            'trigger' => 'province_id',

            'title' => 'words.districts',
            'description' => '',
        ],
    ],

    'provinces' => [
        'name' => [
            'type' => 'text',
            'validation' => 'required|max:100',

            'slug' => true,
            'title' => 'words.name',
            'placeholder' => 'words.name',
            'description' => '',
        ],
        'plate_code' => [
            'type' => 'number',
            'step' => '1',
            'validation' => 'required|numeric|min:0|max:9999',
            'title' => 'words.city_plate_code',
            'placeholder' => 'words.city_plate_code',
            'description' => '',
        ],
    ],

    'districts' => [
        'name' => [
            'type' => 'text',
            'validation' => 'required|max:100',

            'slug' => true,
            'title' => 'words.name',
            'placeholder' => 'words.name',
            'description' => '',
        ],
        /*
        'slug' => [
            'type' => 'text',
            'validation' => 'required|max:50',
            'title' => 'Slug',
            'placeholder' => 'Slug',
            'description' => '',
        ],
        */
        'province_id' => [
            'type' => 'select2',
            'validation' => 'required',

            'dataSource' => 'App\Province',

            'title' => 'words.city',
            'description' => '',
        ],
    ],

    'leagues' => [
        'name' => [
        	'type' => 'text',
        	'validation' => 'required|max:100',
        	'title' => 'words.name',
        	'placeholder' => 'words.name',
        	'description' => '',
	    ],
        /*
	    'facebook' => [
        	'type' => 'text',
        	'validation' => 'nullable|max:32',
        	'title' => 'words.facebook_username',
        	'placeholder' => 'words.facebook_username',
        	'description' => '',
	    ],
	    'twitter' => [
        	'type' => 'text',
        	'validation' => 'nullable|max:32',
        	'title' => 'words.twitter_username',
        	'placeholder' => 'words.twitter_username',
        	'description' => '',
	    ],
	    'instagram' => [
        	'type' => 'text',
        	'validation' => 'nullable|max:32',
        	'title' => 'words.instagram_username',
        	'placeholder' => 'words.instagram_username',
        	'description' => '',
	    ],
        */
	    'active' => [
	    	'type' => 'checkbox',
        	'title' => 'words.active',
        	'description' => '',
        	'value' => 'active', 
            'checkvalue' => true, 
            'uncheckvalue' => false,
	    ],
	    'province' => [
	    	'type' => 'select2',
	    	'multiple' => 'multiple',

            'relationship' => 'league_province', //model function
            'relation_col' => 'province_id',
        	'dataSource' => 'App\Province',
        	
            'title' => 'words.cities',
        	'description' => '',
	    ],
    ],

    'branches' => [
        'name' => [
            'type' => 'text',
            'validation' => 'required|max:100',
            'title' => 'words.name',
            'placeholder' => 'words.name',
            'description' => '',
        ],
        'order' => [
            'type' => 'number',
            'min' => '0',
            'max' => '9999',
            'step' => '1',
            'validation' => 'required|numeric|min:0|max:9999',
            'title' => 'words.order',
            'placeholder' => 'words.order',
            'description' => '',
        ],
        'team_branch' => [
            'type' => 'checkbox',
            'title' => 'words.team_branch',
            'description' => '',
            'value' => 'team_branch', 
            'checkvalue' => true, 
            'uncheckvalue' => false,
        ],
        'active' => [
            'type' => 'checkbox',
            'title' => 'words.active',
            'description' => '',
            'value' => 'active', 
            'checkvalue' => true, 
            'uncheckvalue' => false,
        ],
    ],

    'seasonsp' => [

        'polymorphic_ctype_id' => [
            'type' => 'hidden',
            'defaultValue' => '34',
            'validation' => 'nullable',
            'title' => '',
            'placeholder' => '',
            'description' => '',
        ],
        'closed' => [
            'type' => 'hidden',
            'defaultValue' => 'false',
            'validation' => 'nullable',
            'title' => '',
            'placeholder' => '',
            'description' => '',
        ],
        'name' => [
            'type' => 'text',
            'validation' => 'required|max:100',
            'title' => 'words.name',
            'placeholder' => 'words.name',
            'description' => '',
        ],
        //rwr
        'league_id' => [
            'type' => 'select2',
            'validation' => 'required',
            'dataSource' => 'App\League',
            'title' => 'words.league',
            'description' => '',
        ],
        'year' => [
            'type' => 'number',
            'validation' => 'required|numeric|min:2005|max:2100',
            'step' => '1',

            'title' => 'words.year',
            'placeholder' => 'words.year',
            'description' => '',
        ],
        'start_date' => [
            'type' => 'datepicker',
            'validation' => 'required|date|date_format:d.m.Y',

            'title' => 'words.starting_date',
            'placeholder' => 'words.starting_date',
            'description' => '',
        ],
        'end_date' => [
            'type' => 'datepicker',
            'validation' => 'required|date|date_format:d.m.Y',

            'title' => 'words.end_date',
            'placeholder' => 'words.end_date',
            'description' => '',
        ],
        'min_team_point' => [
            'type' => 'number',
            'validation' => 'required|numeric|min:0|max:9999',
            'step' => '1',

            'relationship' => 'league_pointseason',
            'relation_col' => 'min_team_point',

            'title' => 'words.minimum_team_points',
            'placeholder' => 'words.minimum_team_points',
            'description' => '',
        ],
        'active' => [
            'type' => 'checkbox',
            'title' => 'words.active',
            'description' => '',
            'value' => 'active', 
            'checkvalue' => true, 
            'uncheckvalue' => false,
        ],

        'header_t' => [
            'type' => 'h3',
            'title' => 'Transfer',
        ],
        'transfer_start_date' => [
            'type' => 'datepicker',
            'validation' => 'date|date_format:d.m.Y',
            'title' => 'words.transfer_start_date',
            'placeholder' => 'words.transfer_start_date',
            'description' => '',
        ],
        'transfer_end_date' => [
            'type' => 'datepicker',
            'validation' => 'date|date_format:d.m.Y',
            'title' => 'words.transfer_end_date',
            'placeholder' => 'words.transfer_end_date',
            'description' => '',
        ],
        'allowed_transfer_count' => [
            'type' => 'number',
            'validation' => 'required|numeric|min:0|max:9999',
            'step' => '1',

            'title' => 'words.number_of_transfers_allowed',
            'placeholder' => 'words.number_of_transfers_allowed',
            'description' => '',
        ],
        'locking_match_count' => [
            'type' => 'number',
            'validation' => 'required|numeric|min:0|max:9999',
            'step' => '1',

            'title' => 'words.squad_lock_matches',
            'placeholder' => 'words.squad_lock_matches',
            'description' => '',
        ],

        'header_b' => [
            'type' => 'h3',
            'title' => 'Bonus',
        ],
        'has_bonus' => [
            'type' => 'checkbox',

            'relationship' => 'league_pointseason',
            'relation_col' => 'has_bonus',

            'title' => 'words.monthly_match_bonus',
            'description' => '',
            'value' => 'has_bonus', 
            'checkvalue' => true, 
            'uncheckvalue' => false,
        ],
        'bonus_match_count' => [
            'type' => 'number',
            'validation' => 'nullable|numeric|min:0|max:9999',
            'step' => '1',

            'relationship' => 'league_pointseason',
            'relation_col' => 'bonus_match_count',

            'title' => 'words.monthly_min_number_of_matches',
            'placeholder' => 'words.monthly_min_number_of_matches',
            'description' => '',
        ],
        'bonus_point' => [
            'type' => 'number',
            'validation' => 'nullable|numeric|min:0|max:9999',
            'step' => '1',

            'relationship' => 'league_pointseason',
            'relation_col' => 'bonus_point',

            'title' => 'words.bonus_points',
            'placeholder' => 'words.bonus_points',
            'description' => '',
        ],
    ],

    'seasonsf' => [

        'polymorphic_ctype_id' => [
            'type' => 'hidden',
            'defaultValue' => '33',
            'validation' => 'nullable',
            'title' => '',
            'placeholder' => '',
            'description' => '',
        ],
        'closed' => [
            'type' => 'hidden',
            'defaultValue' => 'false',
            'validation' => 'nullable',
            'title' => '',
            'placeholder' => '',
            'description' => '',
        ],
        'name' => [
            'type' => 'text',
            'validation' => 'required|max:100',
            'title' => 'words.name',
            'placeholder' => 'words.name',
            'description' => '',
        ],
        'league_id' => [
            'type' => 'select2',
            'validation' => 'required',
            'dataSource' => 'App\League',
            'title' => 'words.league',
            'description' => '',
        ],
        'year' => [
            'type' => 'number',
            'validation' => 'required|numeric|min:2005|max:2100',
            'step' => '1',

            'title' => 'words.year',
            'placeholder' => 'words.year',
            'description' => '',
        ],
        'start_date' => [
            'type' => 'datepicker',
            'validation' => 'required|date|date_format:d.m.Y',

            'title' => 'words.starting_date',
            'placeholder' => 'words.starting_date',
            'description' => '',
        ],
        'end_date' => [
            'type' => 'datepicker',
            'validation' => 'required|date|date_format:d.m.Y',

            'title' => 'words.end_date',
            'placeholder' => 'words.end_date',
            'description' => '',
        ],
        'active' => [
            'type' => 'checkbox',
            'title' => 'words.active',
            'description' => '',
            'value' => 'active', 
            'checkvalue' => true, 
            'uncheckvalue' => false,
        ],
        'fs_api' => [
            'type' => 'checkbox',
            'title' => 'words.fs_api',
            'description' => '',
            'value' => 'fs_api', 
            'checkvalue' => true, 
            'uncheckvalue' => false,
        ],
        /*
        'header_t' => [
            'type' => 'h3',
            'title' => 'Transfer',
        ],
        'transfer_start_date' => [
            'type' => 'datepicker',
            'validation' => 'date|date_format:d.m.Y',
            'title' => 'words.transfer_start_date',
            'placeholder' => 'words.transfer_start_date',
            'description' => '',
        ],
        'transfer_end_date' => [
            'type' => 'datepicker',
            'validation' => 'date|date_format:d.m.Y',
            'title' => 'words.transfer_end_date',
            'placeholder' => 'words.transfer_end_date',
            'description' => '',
        ],
        'allowed_transfer_count' => [
            'type' => 'number',
            'validation' => 'required|numeric|min:0|max:9999',
            'step' => '1',

            'title' => 'words.number_of_transfers_allowed',
            'placeholder' => 'words.number_of_transfers_allowed',
            'description' => '',
        ],
        'locking_match_count' => [
            'type' => 'number',
            'validation' => 'required|numeric|min:0|max:9999',
            'step' => '1',

            'title' => 'words.squad_lock_matches',
            'placeholder' => 'words.squad_lock_matches',
            'description' => '',
        ],
        */
    ],

    'seasonse' => [

        'polymorphic_ctype_id' => [
            'type' => 'hidden',
            'defaultValue' => '32',
            'validation' => 'nullable',
            'title' => '',
            'placeholder' => '',
            'description' => '',
        ],
        'closed' => [
            'type' => 'hidden',
            'defaultValue' => 'false',
            'validation' => 'nullable',
            'title' => '',
            'placeholder' => '',
            'description' => '',
        ],
        'name' => [
            'type' => 'text',
            'validation' => 'required|max:100',
            'title' => 'words.name',
            'placeholder' => 'words.name',
            'description' => '',
        ],
        //rwr
        'league_id' => [
            'type' => 'select2',
            'validation' => 'required',
            'dataSource' => 'App\League',
            'title' => 'words.league',
            'description' => '',
        ],
        'season_id' => [
            'type' => 'select2',
            'validation' => 'required',

            //'relationship' => 'league_eliminationseason', //model function
            'relation_col' => 'league_id',
            'dataSource' => 'App\Season',
            'seperator' => 'base_season',

            'ajax' => true,
            'ajax_url' => '/getSeasonsE',
            'search' => true,
            'hoop' => true,

            'title' => 'words.main_season',
            'description' => '',
        ],
        'year' => [
            'type' => 'number',
            'validation' => 'required|numeric|min:2005|max:2100',
            'step' => '1',

            'title' => 'words.year',
            'placeholder' => 'words.year',
            'description' => '',
        ],
        'start_date' => [
            'type' => 'datepicker',
            'validation' => 'required|date|date_format:d.m.Y',

            'title' => 'words.starting_date',
            'placeholder' => 'words.starting_date',
            'description' => '',
        ],
        'end_date' => [
            'type' => 'datepicker',
            'validation' => 'required|date|date_format:d.m.Y',

            'title' => 'words.end_date',
            'placeholder' => 'words.end_date',
            'description' => '',
        ],
        'active' => [
            'type' => 'checkbox',
            'title' => 'words.active',
            'description' => '',
            'value' => 'active', 
            'checkvalue' => true, 
            'uncheckvalue' => false,
        ],
        'fs_api' => [
            'type' => 'checkbox',
            'title' => 'words.fs_api',
            'description' => '',
            'value' => 'fs_api', 
            'checkvalue' => true, 
            'uncheckvalue' => false,
        ],
    ],

    'elimination_tree' => [

        'name' => [
            'type' => 'text',
            'validation' => 'required|max:100',
            'title' => 'words.name',
            'placeholder' => 'words.name',
            'description' => '',
        ],
        //rwr
        'league_id' => [
            'type' => 'select2',
            'validation' => 'required',
            'dataSource' => 'App\League',
            'seperator' => 'elimination_tree_league_id',

            'title' => 'words.league',
            'description' => '',
        ],
        'season_id' => [
            'type' => 'select2',
            'validation' => 'required',

            //'relationship' => 'league_eliminationseason', //model function
            'relation_col' => 'league_id',
            'dataSource' => 'App\Season',
            'seperator' => 'elimination_tree_season_id',

            'ajax' => true,
            'ajax_url' => '/getSeasonsE2',
            'search' => true,
            'hoop' => true,

            'title' => 'words.season',
            'description' => '',
        ],
        'type' => [
            'type' => 'select2',
            'validation' => 'required',
            'dataSource' => 'json_data',
            'title' => 'words.tree_type',
            'description' => '',
            'json_data' => [
                0 => ['id' => 'conference', 'name' => 'Konferans Finali'],
                1 => ['id' => 'final', 'name' => 'İl Finali']
            ]
        ],
        'tree_build' => [
            'type' => 'select2',
            'validation' => 'required',
            'dataSource' => 'json_data',
            'title' => 'words.tree_structure',
            'description' => '',
            'json_data' => [
                0 => ['id' => 1, 'name' => 'Final'],
                1 => ['id' => 2, 'name' => 'Yarı Final, Final'],
                2 => ['id' => 3, 'name' => 'Çeyrek Final, Yarı Final, Final'],
                3 => ['id' => 4, 'name' => 'Last 16, Quarter, Semi, Final']
            ]
        ],
        'teams' => [
            'type' => 'select2',
            'validation' => 'nullable',
            'multiple' => 'multiple',

            'relationship' => 'team',
            'relation_col' => 'season_id',
            //'dataSource' => 'App\TeamTeamSeason',
            'seperator' => 'elimination_tree_teams',

            'ajax' => true,
            'ajax_url' => '/getTeamsR',
            'search' => true,
            'hoop' => true,
            //'trigger' => 'season_id',

            'title' => 'words.teams',
            'description' => '',
        ],
    ],

    'fixture_group' => [

        'name' => [
            'type' => 'text',
            'validation' => 'required|max:100',
            'title' => 'words.name',
            'placeholder' => 'words.name',
            'description' => '',
        ],
        //rwr
        'league_id' => [
            'type' => 'select2',
            'validation' => 'required',
            'dataSource' => 'App\League',
            'seperator' => 'fixture_group_league_id',

            'title' => 'words.league',
            'description' => '',
        ],
        'season_id' => [
            'type' => 'select2',
            'validation' => 'required',

            //'relationship' => 'league_eliminationseason', //model function
            'relation_col' => 'league_id',
            'dataSource' => 'App\Season',
            'seperator' => 'fixture_group_season_id',

            'ajax' => true,
            'ajax_url' => '/getSeasonsE2',
            'search' => true,
            'hoop' => true,

            'title' => 'words.season',
            'description' => '',
        ],
        'teams' => [
            'type' => 'select2',
            'validation' => 'nullable',
            'multiple' => 'multiple',

            'relationship' => 'team',
            'relation_col' => 'season_id',
            //'dataSource' => 'App\TeamTeamSeason',
            'seperator' => 'fixture_group_teams',

            'ajax' => true,
            'ajax_url' => '/getTeamsR',
            'search' => true,
            'hoop' => true,
            //'trigger' => 'season_id',

            'title' => 'words.teams',
            'description' => '',
        ],
        'active' => [
            'type' => 'checkbox',
            'title' => 'words.active',
            'description' => '',
            'value' => 'active', 
            'checkvalue' => true, 
            'uncheckvalue' => false,
        ],
    ],

    'teams' => [

        'name' => [
            'type' => 'text',
            'validation' => 'required|max:100',
            'title' => 'words.name',
            'placeholder' => 'words.name',
            'description' => '',
        ],
        'country_id' => [
            'type' => 'select2',
            'validation' => 'required',

            'dataSource' => 'App\CountryList',
            
            'title' => 'words.country',
            'description' => '',
        ],
        'league_id' => [
            'type' => 'select2',
            'validation' => 'required',
            'dataSource' => 'App\League',
            'title' => 'words.league',
            'description' => '',
        ],
        'tactic_id' => [
            'type' => 'select2',
            'validation' => 'required',

            'dataSource' => 'App\Tactic',
            
            'title' => 'words.tactic',
            'description' => '',
        ],
        
        'province_id' => [
            'type' => 'select2',
            'validation' => 'required',

            'dataSource' => 'App\Province',
            
            'title' => 'words.city',
            'description' => '',
        ],
        /*
        'district_id' => [
            'type' => 'select2',
            'validation' => 'nullable',
            //'multiple' => 'multiple',

            //'relationship' => 'conferences_district', //model function
            //'relation_col' => 'district_id',
            'dataSource' => 'App\District',

            'ajax' => true,
            'ajax_url' => '/getDistrictsF',
            'trigger' => 'province_id',

            'title' => 'words.districts',
            'description' => '',
        ],
        

        'captain_id' => [
            'type' => 'select2',
            'validation' => 'nullable',

            'relationship' => 'user',
            //'relation_col' => 'district_id',
            'dataSource' => 'App\Player',

            'ajax' => true,
            'ajax_url' => '/getUsersF',
            'search' => true,

            'title' => 'words.captain',
            'description' => '',
        ],
        */
        /*
        'header_ta' => [
            'type' => 'h3',
            'title' => 'words.team_survey',
        ],
        'potential_id' => [
            'type' => 'select2',
            'validation' => 'nullable',

            'relationship' => 'team_survey',
            'relation_col' => 'potential_id',
            'dataSource' => 'App\TeamPotential',
            
            'title' => 'words.potential',
            'description' => '',
        ],        
        'satisfaction_id' => [
            'type' => 'select2',
            'validation' => 'nullable',

            'relationship' => 'team_survey',
            'relation_col' => 'satisfaction_id',
            'dataSource' => 'App\TeamSatisfaction',
            
            'title' => 'words.satisfaction',
            'description' => '',
        ],
        */
        'header_d' => [
            'type' => 'h3',
            'title' => 'words.other',
        ],
        'facebook' => [
            'type' => 'text',
            'validation' => 'nullable|max:32',

            'title' => 'words.facebook_username',
            'placeholder' => 'words.facebook_username',
            'description' => '',
        ],
        'twitter' => [
            'type' => 'text',
            'validation' => 'nullable|max:32',

            'title' => 'words.twitter_username',
            'placeholder' => 'words.twitter_username',
            'description' => '',
        ],
        'instagram' => [
            'type' => 'text',
            'validation' => 'nullable|max:32',

            'title' => 'words.instagram_username',
            'placeholder' => 'words.instagram_username',
            'description' => '',
        ],
    ],

    /*
    'team_custom_details' => [
        
        'custom_transfer_start_date' => [
            'type' => 'text',
            'validation' => 'nullable',

            'custom' => true,

            'title' => 'words.transfer_start_date',
            'placeholder' => 'words.transfer_start_date',
            'description' => '',
        ],

        'custom_transfer_end_date' => [
            'type' => 'text',
            'validation' => 'nullable',

            'custom' => true,

            'title' => 'words.transfer_end_date',
            'placeholder' => 'words.transfer_end_date',
            'description' => '',
        ],
        
        'custom_remaining_transfer_count' => [
            'type' => 'number',
            'validation' => 'required|numeric|min:0|max:9999',
            'step' => '1',

            'custom' => true,

            'title' => 'words.remaining_transfers',
            'placeholder' => 'words.remaining_transfers',
            'description' => '',
        ],

    ],
    */
    
    'matches' => [

        'season_id' => [
            'type' => 'select2',
            'validation' => 'required',

            'relationship' => 'active_season',
            //'relation_col' => 'season_id',
            'dataSource' => 'App\Season',
            'seperator' => 'active_season',

            'ajax' => true,
            'ajax_url' => '/getSeasonsF',
            'search' => true,
            'hoop' => true,

            'title' => 'words.season',
            'description' => '',
        ],
        'team1_id' => [
            'type' => 'select2',
            'validation' => 'required',

            'relationship' => 'team',
            'relation_col' => 'season_id',
            'dataSource' => 'App\TeamTeamSeason',
            'seperator' => 'match_team',

            'ajax' => true,
            'ajax_url' => '/getTeamsR',
            'search' => true,
            //'trigger' => 'season_id',

            'title' => 'words.first_team',
            'description' => '',
        ],
        'team2_id' => [
            'type' => 'select2',
            'validation' => 'required',

            'relationship' => 'team',
            'relation_col' => 'season_id',
            'dataSource' => 'App\TeamTeamSeason',
            'seperator' => 'match_team',

            'ajax' => true,
            'ajax_url' => '/getTeamsR',
            'search' => true,
            //'trigger' => 'season_id',

            'title' => 'words.second_team',
            'description' => '',
        ],
        'ground_id' => [
            'type' => 'select2',
            'validation' => 'required',

            //'relationship' => 'province',
            'relation_col' => 'season_id',
            //'dataSource' => 'App\Ground',
            'seperator' => 'match_ground',

            'ajax' => true,
            'ajax_url' => '/getGroundsRe',
            'search' => true,
            'hoop' => true,

            'title' => 'words.ground',
            'description' => '',
        ],
        'date' => [
            'type' => 'datepicker',
            'validation' => 'required|date|date_format:d.m.Y',

            'title' => 'words.match_date',
            'placeholder' => 'words.match_date',
            'description' => '',
        ],
        'time_e' => [ 
            'renamecol' => 'time',
            'type' => 'timepicker',
            'validation' => 'required',

            //'relation_col' => 'date',
            //'seperator' => 'match_time',

            //'ajax' => true,
            //'ajax_url' => '/getGroundHourFE',
            //'search' => true,
            //'hoop' => true,

            'title' => 'words.match_time',
            'description' => '',
        ],
        /*
        'referee_id' => [
            'type' => 'select2',
            'validation' => 'nullable',

            //'relationship' => 'referee',
            'relation_col' => 'season_id',
            //'dataSource' => 'App\RefereeLeague',
            'seperator' => 'match_referee',

            'ajax' => true,
            'ajax_url' => '/getUsersRe',
            'search' => true,
            'hoop' => true,

            'title' => 'words.referee',
            'description' => '',
        ],
        */
        'coordinator_id' => [
            'type' => 'select2',
            'validation' => 'nullable',

            //'relationship' => 'referee',
            'relation_col' => 'season_id',
            //'dataSource' => 'App\RefereeLeague',
            'seperator' => 'match_coordinator',

            'ajax' => true,
            'ajax_url' => '/getUsersCo',
            'search' => true,
            'hoop' => true,

            'title' => 'words.coordinator',
            'description' => '',
        ],
        'completed_status' => [
            'type' => 'select2',
            'addvisible' => 'no',
            'validation' => 'nullable',
            'dataSource' => 'json_data',
            'title' => 'words.won_by_judgment',
            'description' => '',
            'json_data' => [
                1 => ['id' => 1, 'name' => 'words.first_team_won_by_judgment'],
                2 => ['id' => 2, 'name' => 'words.second_team_won_by_judgment'],
            ]
        ],
        'fsdatasave' => [
            'type' => 'fs_switch',
            'addvisible' => 'no',
            'validation' => 'nullable',

            'data' => [
                'on' => ['name' => 'words.yes'],
                'off' => ['name' => 'words.no'],
            ],

            'title' => 'Formastar Verisini Kaydet',
            'description' => '', //'Maç aksiyonları ile ilgili güncellemeler yapmak için maçın DEVAM EDİYOR modunda olması gerekmektedir!',
        ],
        'completed' => [
            'type' => 'switch',
            'addvisible' => 'no',
            'validation' => 'nullable',

            'data' => [
                'on' => ['name' => 'words.completed'],
                'off' => ['name' => 'words.continues'],
            ],

            'title' => 'words.match_status',
            'description' => '', //'Maç aksiyonları ile ilgili güncellemeler yapmak için maçın DEVAM EDİYOR modunda olması gerekmektedir!',
        ]
    ],

    'match_video' => [
        
        'status' => [
            'type' => 'select2',
            'validation' => 'required',
            'dataSource' => 'json_data',
            'title' => 'words.status',
            'description' => '',
            'json_data' => [
                0 => ['id' => 'recordable', 'name' => 'words.recordable'],
                1 => ['id' => 'recording', 'name' => 'words.recording'],
                2 => ['id' => 'recorded', 'name' => 'words.recorded'],
                3 => ['id' => 'finished', 'name' => 'words.completed'],
            ]
        ],
        'live_source' => [
            'type' => 'select2',
            'validation' => 'required',
            'dataSource' => 'json_data',
            'title' => 'words.live_video_source',
            'description' => '',
            'json_data' => [
                0 => ['id' => 'app', 'name' => 'App'],
                1 => ['id' => 'youtube', 'name' => 'YouTube'],
                2 => ['id' => 'vimeo', 'name' => 'Vimeo'],
                3 => ['id' => 'izlesene', 'name' => 'Izlesene'],
                4 => ['id' => 'facebook', 'name' => 'facebook'],
                5 => ['id' => 'banner', 'name' => 'banner'],
            ]
        ],
        'live_embed_code' => [
            'type' => 'text',
            'validation' => 'nullable|max:255',

            'title' => 'words.live_video_embed_code',
            'placeholder' => 'words.live_video_embed_code',
            'description' => '',
        ],
        'full_source' => [
            'type' => 'select2',
            'validation' => 'required',
            'dataSource' => 'json_data',
            'title' => 'words.full_match_video_source',
            'description' => '',
            'json_data' => [
                0 => ['id' => 'app', 'name' => 'App'],
                1 => ['id' => 'youtube', 'name' => 'YouTube'],
                2 => ['id' => 'vimeo', 'name' => 'Vimeo'],
                3 => ['id' => 'izlesene', 'name' => 'Izlesene'],
                4 => ['id' => 'facebook', 'name' => 'facebook'],
            ]
        ],
        'full_embed_code' => [
            'type' => 'text',
            'validation' => 'nullable|max:255',

            'title' => 'words.full_match_embed_code',
            'placeholder' => 'words.full_match_embed_code',
            'description' => '',
        ],
        'summary_source' => [
            'type' => 'select2',
            'validation' => 'required',
            'dataSource' => 'json_data',
            'title' => 'words.highlights_video_source',
            'description' => '',
            'json_data' => [
                0 => ['id' => 'app', 'name' => 'App'],
                1 => ['id' => 'youtube', 'name' => 'YouTube'],
                2 => ['id' => 'vimeo', 'name' => 'Vimeo'],
                3 => ['id' => 'izlesene', 'name' => 'Izlesene'],
                4 => ['id' => 'facebook', 'name' => 'facebook'],
            ]
        ],
        'summary_embed_code' => [
            'type' => 'text',
            'validation' => 'nullable|max:255',

            'title' => 'words.highlights_embed_code',
            'placeholder' => 'words.highlights_embed_code',
            'description' => '',
        ],
    ],

    'match_player' => [
        
        'header_oe' => [
            'type' => 'h3',
            'title' => 'words.add_edit_player',
        ],
        'match_id' => [
            'type' => 'hidden',
            'model_col' => 'id',
            'validation' => 'nullable',
            'title' => '',
            'placeholder' => '',
            'description' => '',
        ],
        'match_player_id' => [
            'type' => 'hidden',
            'validation' => 'nullable',
            'title' => '',
            'placeholder' => '',
            'description' => '',
        ],
        'player_id' => [
            'add_only' => true,
            'type' => 'select2',
            'validation' => 'nullable',
            
            'seperator' => 'match_team_player',

            'title' => 'words.player',
            'description' => '',
        ],
        'guest_name' => [
            'add_only' => true,
            'type' => 'text',
            'validation' => 'nullable|max:255',

            'title' => 'words.guest_name',
            'placeholder' => 'words.guest_name',
            'description' => '',
        ],
        'position_id' => [
            'add_only' => true,
            'type' => 'select2',
            'validation' => 'required',

            //'relationship' => 'player',
            //'relation_col' => 'position_id',
            'dataSource' => 'App\PlayerPosition',

            'title' => 'words.position',
            'description' => '',
        ],
        'rating' => [
            'add_only' => true,
            'type' => 'number',
            'validation' => 'required|numeric|min:0|max:9999',
            'step' => '1',

            'title' => 'words.rating',
            'placeholder' => 'words.rating',
            'description' => '',
        ],
        'saving' => [
            'add_only' => true,
            'type' => 'number',
            'validation' => 'nullable|numeric|min:0|max:9999',
            'step' => '1',

            'title' => 'words.saving',
            'placeholder' => 'words.saving',
            'description' => '',
        ],
    ],

    'match_action' => [

        'header_oe' => [
            'type' => 'h3',
            'title' => 'words.add_edit_action',
        ],
        'match_id' => [
            'type' => 'hidden',
            'model_col' => 'id',
            'validation' => 'required',
            'title' => '',
            'placeholder' => '',
            'description' => '',
        ],
        'match_action_id' => [
            'type' => 'hidden',
            'validation' => 'nullable',
            'title' => '',
            'placeholder' => '',
            'description' => '',
        ],
        'embed_source' => [
            'add_only' => true,
            'type' => 'select2',
            'validation' => 'nullable',
            'dataSource' => 'json_data',
            'title' => 'words.video_source',
            'description' => '',
            'json_data' => [
                0 => ['id' => 'app', 'name' => 'App'],
                1 => ['id' => 'youtube', 'name' => 'YouTube'],
                2 => ['id' => 'vimeo', 'name' => 'Vimeo'],
                3 => ['id' => 'izlesene', 'name' => 'Izlesene'],
            ]
        ],
        'embed_code' => [
            'add_only' => true,
            'type' => 'text',
            'validation' => 'nullable|max:255',

            'title' => 'words.video_embed_code',
            'placeholder' => 'words.video_embed_code',
            'description' => '',
        ],
        'match_player_id' => [
            'add_only' => true,
            'type' => 'select2',
            'validation' => 'required',
            
            'seperator' => 'match_team_player_all',

            'title' => 'words.team-player',
            'description' => '',
        ],
        'type' => [
            'add_only' => true,
            'type' => 'select2',
            'validation' => 'required',

            'seperator' => 'match_action_type',

            'dataSource' => 'json_data',
            'title' => 'words.action_type',
            'description' => '',
            'json_data' => [
                /* STATİK */
                0 => ['id' => 'goal', 'name' => 'words.goal'],
                1 => ['id' => 'yellow_card', 'name' => 'words.yellow_card'],
                2 => ['id' => 'red_card', 'name' => 'words.red_card'],
                /* DEĞER GİRİLMEDİYSE FRONTEND DE GÖRÜNMEYECEK */
                3 => ['id' => 'critical', 'name' => 'words.critical_position'],
                4 => ['id' => 'assist', 'name' => 'words.assist'],
                5 => ['id' => 'foul', 'name' => 'words.foul'],
                6 => ['id' => 'p_goal', 'name' => 'words.goal_scored_on_penalties'],
                // 7 => ['id' => 'shot', 'name' => 'words.shot'],
                // 8 => ['id' => 'shotsongoal', 'name' => 'words.shotsongoal'],
                // 9 => ['id' => 'corner_kick', 'name' => 'words.corner_kick']
            ]
        ],
        'minute' => [
            'add_only' => true,
            'type' => 'number',
            'validation' => 'required|numeric|min:0|max:9999',
            'step' => '1',

            'title' => 'words.minute',
            'placeholder' => 'words.minute',
            'description' => '',
        ],
    ],

    'match_panorama' => [

        'header_mpa' => [
            'type' => 'h3',
            'title' => 'words.add_edit_panorama',
        ],
        'match_id' => [
            'type' => 'hidden',
            'model_col' => 'id',
            'validation' => 'required',
            'title' => '',
            'placeholder' => '',
            'description' => '',
        ],
        'match_panorama_id' => [
            'type' => 'hidden',
            'validation' => 'nullable',
            'title' => '',
            'placeholder' => '',
            'description' => '',
        ],
        'panorama_id' => [
            'type' => 'select2',
            'validation' => 'required',

            'dataSource' => 'App\PanoramaType',
            
            'title' => 'words.panorama',
            'description' => '',
        ],
        'match_player_id_mpa' => [
            'renamecol' => 'match_player_id',
            'add_only' => true,
            'type' => 'select2',
            'validation' => 'nullable',
            
            'seperator' => 'match_team_player_all',

            'title' => 'words.player',
            'description' => '',
        ],
        'embed_source_mpa' => [
            'renamecol' => 'match_player_id',
            'add_only' => true,
            'type' => 'select2',
            'validation' => 'nullable',
            'dataSource' => 'json_data',
            'title' => 'words.video_source',
            'description' => '',
            'json_data' => [
                0 => ['id' => 'app', 'name' => 'App'],
                1 => ['id' => 'youtube', 'name' => 'YouTube'],
                2 => ['id' => 'vimeo', 'name' => 'Vimeo'],
                3 => ['id' => 'izlesene', 'name' => 'Izlesene'],
            ]
        ],
        'embed_code_mpa' => [
            'renamecol' => 'embed_code',
            'add_only' => true,
            'type' => 'text',
            'validation' => 'nullable|max:255',

            'title' => 'words.video_embed_code',
            'placeholder' => 'words.video_embed_code',
            'description' => '',
        ],

        

        /*
        'embed_source' => [
            'add_only' => true,
            'type' => 'select2',
            'validation' => 'nullable',
            'dataSource' => 'json_data',
            'title' => 'words.video_source',
            'description' => '',
            'json_data' => [
                0 => ['id' => 'app', 'name' => 'App'],
                1 => ['id' => 'youtube', 'name' => 'YouTube'],
                2 => ['id' => 'vimeo', 'name' => 'Vimeo'],
                3 => ['id' => 'izlesene', 'name' => 'Izlesene'],
            ]
        ],
        'embed_code' => [
            'add_only' => true,
            'type' => 'text',
            'validation' => 'nullable|max:255',

            'title' => 'words.video_embed_code',
            'placeholder' => 'words.video_embed_code',
            'description' => '',
        ],
        'match_player_id' => [
            'add_only' => true,
            'type' => 'select2',
            'validation' => 'nullable',
            
            'seperator' => 'match_team_player_all',

            'title' => 'words.player',
            'description' => '',
        ],
        'type' => [
            'add_only' => true,
            'type' => 'select2',
            'validation' => 'required',

            'seperator' => 'match_action_type',

            'dataSource' => 'json_data',
            'title' => 'words.action_type',
            'description' => '',
            'json_data' => [
                0 => ['id' => 'goal', 'name' => 'words.goal'],
                1 => ['id' => 'yellow_card', 'name' => 'words.yellow_card'],
                2 => ['id' => 'red_card', 'name' => 'words.red_card'],
                3 => ['id' => 'critical', 'name' => 'words.critical_position'],
            ]
        ],
        'minute' => [
            'add_only' => true,
            'type' => 'number',
            'validation' => 'required|numeric|min:0|max:9999',
            'step' => '1',

            'title' => 'words.minute',
            'placeholder' => 'words.minute',
            'description' => '',
        ],
        */
    ],

    'match_press' => [
        
        'match_press_id' => [
            'type' => 'hidden',
            'validation' => 'nullable',
            'title' => '',
            'placeholder' => '',
            'description' => '',
        ],
        'embed_source_mprs' => [
            'renamecol' => 'embed_source',
            'type' => 'select2',
            'validation' => 'required',
            'dataSource' => 'json_data',

            'title' => 'words.video_source',
            'description' => '',
            'json_data' => [
                0 => ['id' => 'app', 'name' => 'App'],
                1 => ['id' => 'youtube', 'name' => 'YouTube'],
                2 => ['id' => 'vimeo', 'name' => 'Vimeo'],
                3 => ['id' => 'izlesene', 'name' => 'Izlesene'],
            ]
        ],
        'embed_code_mprs' => [
            'renamecol' => 'embed_code',
            'add_only' => true,
            'type' => 'text',
            'validation' => 'nullable|max:255',

            'title' => 'words.video_embed_code',
            'placeholder' => 'words.video_embed_code',
            'description' => '',
        ],
        'team_id_mprs' => [
            'renamecol' => 'team_id',
            'add_only' => true,
            'type' => 'select2',
            'validation' => 'nullable',

            'relationship' => 'team', //model function
            'relation_col' => 'team_id',
            
            'seperator' => 'match_teams_press',

            'title' => 'words.team',
            'description' => '',
        ],
        'players_id_mprs' => [
            'renamecol' => 'player_id',
            'multiple' => 'multiple',
            'add_only' => true,
            'type' => 'select2',
            'validation' => 'nullable',

            'seperator' => 'match_team_player_all',

            'title' => 'words.player',
            'description' => '',
        ]
        /*
        'players_id_mprs' => [
            'renamecol' => 'player_id',
            'multiple' => 'multiple',
            'add_only' => true,
            'type' => 'select2',
            'validation' => 'nullable',

            'relationship' => 'team', //model function
            'relation_col' => 'player_id',
            'dataSource' => 'App\MatchPlayer',

            'ajax' => true,
            'ajax_url' => '/getMatchPlayerTeam',
            'trigger' => 'team_id_mprs',
            
            'seperator' => 'match_team_player_press',

            'title' => 'words.player',
            'description' => '',
        ]
        */
    ],

    'match_statistic' => [
        
        'team1percent' => [
            'type' => 'number',
            'validation' => 'nullable|numeric|min:0|max:100',
            'step' => '1',

            'title' => 'words.team1percent',
            'placeholder' => 'words.team1percentplaceholder',
            'description' => '',
        ],
        'team1shot' => [
            'type' => 'number',
            'validation' => 'nullable|numeric|min:0|max:100',
            'step' => '1',

            'title' => 'Takım 1 İsabetli Şut',
            'placeholder' => 'Takım 1 İsabetli Şut',
            'description' => '',
        ],
        'team1failedShot' => [
            'type' => 'number',
            'validation' => 'nullable|numeric|min:0|max:100',
            'step' => '1',

            'title' => 'Takım 1 İsabetsiz Şut',
            'placeholder' => 'Takım 1 İsabetsiz Şut',
            'description' => '',
        ],
        'team1fouls' => [
            'type' => 'number',
            'validation' => 'nullable|numeric|min:0|max:100',
            'step' => '1',

            'title' => 'Takım 1 Faul',
            'placeholder' => 'Takım 1 Faul',
            'description' => '',
        ],
        'team1corners' => [
            'type' => 'number',
            'validation' => 'nullable|numeric|min:0|max:100',
            'step' => '1',

            'title' => 'Takım 1 Korner',
            'placeholder' => 'Takım 1 Korner',
            'description' => '',
        ],
        'team1playedTime' => [
            'type' => 'number',
            'validation' => 'nullable|numeric|min:0|max:100',
            'step' => '1',

            'title' => 'Takım 1 Topla Oynama Süresi',
            'placeholder' => 'Takım 1 Topla Oynama Süresi',
            'description' => '',
        ],
        'team1totalPlayTime' => [
            'type' => 'number',
            'validation' => 'nullable|numeric|min:0|max:100',
            'step' => '1',

            'title' => 'Takım 1 Topun Oyunda Kalma Süresi',
            'placeholder' => 'Takım 1 Topun Oyunda Kalma Süresi',
            'description' => '',
        ],





        'team2percent' => [
            'type' => 'number',
            'validation' => 'nullable|numeric|min:0|max:100',
            'step' => '1',

            'title' => 'words.team2percent',
            'placeholder' => 'words.team2percentplaceholder',
            'description' => '',
        ],
        'team2shot' => [
            'type' => 'number',
            'validation' => 'nullable|numeric|min:0|max:100',
            'step' => '1',

            'title' => 'Takım 2 İsabetli Şut',
            'placeholder' => 'Takım 2 İsabetli Şut',
            'description' => '',
        ],
        'team2failedShot' => [
            'type' => 'number',
            'validation' => 'nullable|numeric|min:0|max:100',
            'step' => '1',

            'title' => 'Takım 2 İsabetsiz Şut',
            'placeholder' => 'Takım 2 İsabetsiz Şut',
            'description' => '',
        ],
        'team2fouls' => [
            'type' => 'number',
            'validation' => 'nullable|numeric|min:0|max:100',
            'step' => '1',

            'title' => 'Takım 2 Faul',
            'placeholder' => 'Takım 2 Faul',
            'description' => '',
        ],
        'team2corners' => [
            'type' => 'number',
            'validation' => 'nullable|numeric|min:0|max:100',
            'step' => '1',

            'title' => 'Takım 2 Korner',
            'placeholder' => 'Takım 2 Korner',
            'description' => '',
        ],
        'team2playedTime' => [
            'type' => 'number',
            'validation' => 'nullable|numeric|min:0|max:100',
            'step' => '1',

            'title' => 'Takım 2 Topla Oynama Süresi',
            'placeholder' => 'Takım 2 Topla Oynama Süresi',
            'description' => '',
        ],
        'team2totalPlayTime' => [
            'type' => 'number',
            'validation' => 'nullable|numeric|min:0|max:100',
            'step' => '1',

            'title' => 'Takım 2 Topun Oyunda Kalma Süresi',
            'placeholder' => 'Takım 2 Topun Oyunda Kalma Süresi',
            'description' => '',
        ],





        'ballinplay' => [
            'type' => 'text',
            'validation' => 'max:30',

            'title' => 'words.ballinplay',
            'placeholder' => 'words.ballinplayexample',
            'description' => '',
        ],

    ],

    'users' => [

        'first_name' => [
            'type' => 'text',
            'validation' => 'required|max:30',

            'title' => 'words.first_name',
            'placeholder' => 'words.first_name',
            'description' => '',
        ],
        'last_name' => [
            'type' => 'text',
            'validation' => 'required|max:150',

            'title' => 'words.last_name',
            'placeholder' => 'words.last_name',
            'description' => '',
        ],
        
        'email' => [
            'type' => 'email',
            'validation' => 'required|email|max:254',

            'title' => 'words.email',
            'placeholder' => 'words.email',
            'description' => '',
        ],
        'phone' => [
            'type' => 'text',
            'validation' => 'required|max:20',

            'title' => 'words.phone',
            'placeholder' => 'words.phone',
            'description' => '',
        ],
        'header_y' => [
            'type' => 'h3',
            'title' => 'words.admin_authority',
        ],
        'role' => [
            'type' => 'select2',
            'validation' => 'nullable',
            //'multiple' => 'multiple',

            //'relationship' => 'profile', //model function
            //'relation_col' => 'province_id',
            'dataSource' => 'Spatie\Permission\Models\Role',
            'seperator' => 'user_role_id',
            
            'title' => 'words.authority',
            'description' => '',
        ],
        'role_province' => [
            'type' => 'select2',
            'validation' => 'nullable',
            'multiple' => 'multiple',

            'relationship' => 'roleprovince', //model function
            'relation_col' => 'province_id',
            'dataSource' => 'App\Province',
            
            'title' => 'words.authorized_cities',
            'description' => '',
        ],

        'header_i' => [
            'type' => 'h3',
            'title' => 'words.profile',
        ],
        'province' => [
            'type' => 'select2',
            'validation' => 'nullable',
            //'multiple' => 'multiple',

            'relationship' => 'profile', //model function
            'relation_col' => 'province_id',
            'dataSource' => 'App\Province',
            
            'title' => 'words.cities',
            'description' => '',
        ],
        'birth_date' => [
            'type' => 'datepicker',
            'validation' => 'required|date|date_format:d.m.Y',

            'relationship' => 'profile',
            'relation_col' => 'birth_date',

            'title' => 'words.date_of_birth',
            'placeholder' => 'words.date_of_birth',
            'description' => '',
        ],

        'header_o' => [
            'type' => 'h3',
            'relationship' => 'player',
            'title' => 'words.player',
        ],

        'facebook' => [
            'type' => 'text',
            'validation' => 'nullable|max:32',

            'relationship' => 'player',
            'relation_col' => 'facebook',

            'title' => 'words.facebook_username',
            'placeholder' => 'words.facebook_username',
            'description' => '',
        ],
        'twitter' => [
            'type' => 'text',
            'validation' => 'nullable|max:32',

            'relationship' => 'player',
            'relation_col' => 'twitter',

            'title' => 'words.twitter_username',
            'placeholder' => 'words.twitter_username',
            'description' => '',
        ],
        'instagram' => [
            'type' => 'text',
            'validation' => 'nullable|max:32',

            'relationship' => 'player',
            'relation_col' => 'instagram',

            'title' => 'words.instagram_username',
            'placeholder' => 'words.instagram_username',
            'description' => '',
        ],
        'position_id' => [
            'type' => 'select2',
            'validation' => 'nullable',

            'relationship' => 'player',
            'relation_col' => 'position_id',
            'dataSource' => 'App\PlayerPosition',

            'title' => 'words.position_1',
            'description' => '',
        ],
        'number' => [
            'type' => 'number',
            'validation' => 'nullable|numeric|min:0|max:9999',
            'step' => '1',

            'relationship' => 'player',
            'relation_col' => 'number',

            'title' => 'words.uniform_number',
            'placeholder' => 'words.uniform_number',
            'description' => '',
        ],
        'foot' => [
            'type' => 'radio',
            'validation' => 'nullable',

            'relationship' => 'player',
            'relation_col' => 'foot',

            'title' => 'words.strong_foot',
            'description' => '',
            'values' => [
                'none' => ['title' => 'words.undefined', 'value' => ''],
                'right' => ['title' => 'words.right_foot', 'value' => 'right'],
                'left' => ['title' => 'words.left_foot', 'value' => 'left'],
                'both' => ['title' => 'words.both', 'value' => 'both'],
            ],
        ],
        'weight' => [
            'type' => 'number',
            'validation' => 'nullable|numeric|min:0|max:9999',
            'step' => '1',

            'relationship' => 'player',
            'relation_col' => 'weight',
            
            'title' => 'words.weight',
            'placeholder' => 'words.weight',
            'description' => '',
        ],
        'height' => [
            'type' => 'number',
            'validation' => 'nullable|numeric|min:0|max:9999',
            'step' => '1',

            'relationship' => 'player',
            'relation_col' => 'height',

            'title' => 'words.size',
            'placeholder' => 'words.size',
            'description' => '',
        ],
    ],

    'roles' => [
        'name' => [
            'type' => 'text',
            'validation' => 'required|max:50',

            'title' => 'words.name',
            'placeholder' => 'words.name',
            'description' => '',
        ],
    ],

    'grounds' => [
        // PROBLEM
        'name' => [
            'type' => 'text',
            'validation' => 'required|max:50',

            'slug' => true,
            'title' => 'words.name',
            'placeholder' => 'words.name',
            'description' => '',
        ],
        /*
        'capacity' => [
            'type' => 'number',
            'validation' => 'nullable|numeric|min:0|max:9999',
            'step' => '1',

            'title' => 'Kapasite',
            'placeholder' => 'Kapasite',
            'description' => '',
        ],
        'width' => [
            'type' => 'number',
            'validation' => 'nullable|numeric|min:0|max:9999',
            'step' => '1',

            'title' => 'Genişlik',
            'placeholder' => 'Genişlik',
            'description' => '',
        ],
        'length__s' => [
            'renamecol' => 'length',
            'type' => 'number',
            'validation' => 'nullable|numeric|min:0|max:9999',
            'step' => '1',

            'title' => 'Uzunluk',
            'placeholder' => 'Uzunluk',
            'description' => '',
        ],
        'description' => [
            'type' => 'textarea',
            'validation' => 'nullable',
            'title' => 'words.description',
            'placeholder' => 'words.description',
            'description' => '',
        ],
        'header_i' => [
            'type' => 'h3',
            'title' => 'İletişim Bilgileri',
        ],
        'phone' => [
            'type' => 'text',
            'validation' => 'nullable|max:20',
            'title' => 'Telefon Numarası 1',
            'placeholder' => 'Telefon Numarası 1',
            'description' => '',
        ],
        'phone2' => [
            'type' => 'text',
            'validation' => 'nullable|max:20',
            'title' => 'Telefon Numarası 2',
            'placeholder' => 'Telefon Numarası 2',
            'description' => '',
        ],
        'email' => [
            'type' => 'email',
            'validation' => 'nullable|email|max:254',
            'title' => 'words.email',
            'placeholder' => 'words.email',
            'description' => '',
        ],
        'website' => [
            'type' => 'text',
            'validation' => 'nullable|max:200',
            'title' => 'Website',
            'placeholder' => 'Website',
            'description' => '',
        ],
        'header_k' => [
            'type' => 'h3',
            'title' => 'Konum',
        ],
        */
        /*
        'province_id' => [ 
            'type' => 'hidden',
            'defaultValue' => '1',
            'validation' => 'nullable',
            'title' => '',
            'placeholder' => '',
            'description' => '',
        ],
        */
        'province_id' => [
            'type' => 'select2',
            'validation' => 'required',

            'dataSource' => 'App\Province',
            
            'title' => 'Şehir',
            'description' => '',
        ],
        'district_id' => [
            'type' => 'select2',
            'validation' => 'nullable',
            //'multiple' => 'multiple',
            //'relationship' => 'conferences_district', //model function
            //'relation_col' => 'district_id',
            'dataSource' => 'App\District',

            'ajax' => true,
            'ajax_url' => '/getDistrictsF',
            'trigger' => 'province_id',

            'title' => 'words.districts',
            'description' => '',
        ],
        /*
        'latitude' => [
            'type' => 'number',
            'step' => '0.000001',
            'validation' => 'nullable|numeric|min:0|max:9999',
            'title' => 'Enlem',
            'placeholder' => 'Enlem',
            'description' => '',
        ],
        'longitude' => [
            'type' => 'number',
            'step' => '0.000001',
            'validation' => 'nullable|numeric|min:0|max:9999',
            'title' => 'Boylam',
            'placeholder' => 'Boylam',
            'description' => '',
        ],
        'ground_property' => [
            'type' => 'select2',
            'validation' => 'nullable',
            'multiple' => 'multiple',

            'relationship' => 'grounds_properties', //model function
            'relation_col' => 'property_id',
            'dataSource' => 'App\GroundProperty',
            
            'title' => 'Halı Saha Özellikleri',
            'description' => '',
        ],
        */
    ],

    'ground_properties' => [
        'name' => [
            'type' => 'text',
            'validation' => 'required|max:100',
            'title' => 'words.name',
            'placeholder' => 'words.name',
            'description' => '',
        ],
    ],

    'reservations' => [
        //rwr
        'league_id' => [
            'type' => 'select2',
            'validation' => 'required',

            'dataSource' => 'App\League',
            'seperator' => 'reservation_league_id',

            'title' => 'words.league',
            'description' => '',
        ],
        'season_id' => [
            'type' => 'select2',
            'validation' => 'required',

            'relation_col' => 'league_id',
            'seperator' => 'reservation_season_id',

            'ajax' => true,
            'ajax_url' => '/getSeasonsL',
            'search' => true,
            'hoop' => true,

            'title' => 'words.season',
            'description' => '',
        ],



        'ground_id' => [
            'type' => 'select2',
            'validation' => 'required',

            'relation_col' => 'season_id',
            'seperator' => 'reservation_ground_id',

            'ajax' => true,
            'ajax_url' => '/getGroundsRe',
            'search' => true,
            'hoop' => true,

            'title' => 'words.ground',
            'description' => '',
        ],

        'team_id' => [
            'type' => 'select2',
            'validation' => 'required',

            'relationship' => 'team',
            'relation_col' => 'season_id',
            'dataSource' => 'App\TeamTeamSeason',
            'seperator' => 'reservation_team_id',

            'ajax' => true,
            'ajax_url' => '/getTeamsR',
            'search' => true,
            //'trigger' => 'season_id',

            'title' => 'words.team',
            'description' => '',
        ],



        'date' => [
            'type' => 'datepicker',
            'validation' => 'required|date|date_format:d.m.Y',
            'seperator' => 'reservation_date',

            'title' => 'words.match_date',
            'placeholder' => 'words.match_date',
            'description' => '',
        ],
        'time' => [
            'type' => 'select2',
            'validation' => 'required',

            'relation_col' => 'date',
            'seperator' => 'reservation_time',

            'ajax' => true,
            'ajax_url' => '/getGroundHourF',
            'search' => true,
            'hoop' => true,

            'title' => 'words.match_time',
            'description' => '',
        ],
    ],

    'player_positions' => [
        'name' => [
            'type' => 'text',
            'validation' => 'required|max:100',
            'title' => 'words.name',
            'placeholder' => 'words.name',
            'description' => '',
        ],
        'code' => [
            'type' => 'text',
            'validation' => 'required|max:3',
            'title' => 'words.code',
            'placeholder' => 'words.code',
            'description' => '',
        ],
        'order' => [
            'type' => 'number',
            'min' => '0',
            'max' => '9999',
            'step' => '1',
            'validation' => 'required|numeric|min:0|max:9999',
            'title' => 'words.order',
            'placeholder' => 'words.order',
            'description' => '',
        ],
    ],

    'point_types' => [
        'name' => [
            'type' => 'text',
            'validation' => 'required|max:100',
            'title' => 'words.name',
            'placeholder' => 'words.name',
            'description' => '',
        ],
        'active' => [
            'type' => 'checkbox',
            'title' => 'words.active',
            'description' => '',
            'value' => 'active', 
            'checkvalue' => true, 
            'uncheckvalue' => false,
        ],
        'description' => [
            'type' => 'text',
            'validation' => 'required|max:150',
            'title' => 'words.description',
            'placeholder' => 'words.description',
            'description' => '',
        ],
        'min_point' => [
            'type' => 'number',
            'min' => '0',
            'max' => '9999',
            'step' => '1',
            'validation' => 'required|numeric|min:0|max:9999|min:0',
            'title' => 'words.min_points',
            'placeholder' => 'words.min_points',
            'description' => '',
        ],
        'max_point' => [
            'type' => 'number',
            'min' => '0',
            'max' => '9999',
            'step' => '1',
            'validation' => 'required|numeric|min:0|max:9999|min:0',
            'title' => 'words.max_points',
            'placeholder' => 'words.max_points',
            'description' => '',
        ],
        'win_score' => [
            'type' => 'number',
            'min' => '0',
            'max' => '9999',
            'step' => '1',
            'validation' => 'required|numeric|min:0|max:9999|min:0',
            'title' => 'words.win_points',
            'placeholder' => 'words.win_points',
            'description' => '',
        ],
        'defeat_score' => [
            'type' => 'number',
            'min' => '0',
            'max' => '9999',
            'step' => '1',
            'validation' => 'required|numeric|min:0|max:9999|min:0',
            'title' => 'words.defeat_score',
            'placeholder' => 'words.defeat_score',
            'description' => '',
        ],
        'draw_score' => [
            'type' => 'number',
            'min' => '0',
            'max' => '9999',
            'step' => '1',
            'validation' => 'required|numeric|min:0|max:9999|min:0',
            'title' => 'words.draw_score',
            'placeholder' => 'words.draw_score',
            'description' => '',
        ],
        'color' => [
            'type' => 'text',
            'validation' => 'required|max:6',
            'title' => 'words.color',
            'placeholder' => 'words.color',
            'description' => '',
        ],
    ],

    'tactics' => [
        'name' => [
            'type' => 'text',
            'validation' => 'required|max:100',
            'title' => 'words.name',
            'placeholder' => 'words.name',
            'description' => '',
        ],
    ],

    'team_potentials' => [
        'name' => [
            'type' => 'text',
            'validation' => 'required|max:100',
            'title' => 'words.name',
            'placeholder' => 'words.name',
            'description' => '',
        ],
    ],

    'team_satisfactions' => [
        'name' => [
            'type' => 'text',
            'validation' => 'required|max:100',
            'title' => 'words.name',
            'placeholder' => 'words.name',
            'description' => '',
        ],
    ],

    'reservation_offer_convert_match' => [
        'header_cm' => [
            'type' => 'h3',
            'title' => 'words.convert_match',
        ],
        'reservation_offer_id' => [
            'type' => 'hidden',
            'exception' => 'reservation_offer_id',
            'validation' => 'nullable',
            'title' => '',
            'placeholder' => '',
            'description' => '',
        ],
        'referee_id' => [
            'type' => 'select2',
            'validation' => 'required',

            //'relationship' => 'referee',
            'relation_col' => 'season_id',
            //'dataSource' => 'App\RefereeLeague',
            'seperator' => 'match_referee',

            'ajax' => true,
            'ajax_url' => '/getUsersRe',
            'search' => true,
            'hoop' => true,

            'title' => 'words.referee',
            'description' => '',
        ],
        'coordinator_id' => [
            'type' => 'select2',
            'validation' => 'required',

            //'relationship' => 'referee',
            'relation_col' => 'season_id',
            //'dataSource' => 'App\RefereeLeague',
            'seperator' => 'match_coordinator',

            'ajax' => true,
            'ajax_url' => '/getUsersCo',
            'search' => true,
            'hoop' => true,

            'title' => 'words.coordinator',
            'description' => '',
        ],
    ],

    'player_penalties' => [
        'polymorphic_ctype_id' => [
            'type' => 'hidden',
            'defaultValue' => '44',
            'validation' => 'nullable',
            'title' => '',
            'placeholder' => '',
            'description' => '',
        ],
        'league_id' => [
            'type' => 'select2',
            'validation' => 'required',
            'dataSource' => 'App\League',
            'title' => 'words.league',
            'description' => '',
        ],
        'season_id' => [
            'type' => 'select2',
            'validation' => 'required',

            //'relationship' => 'league_eliminationseason', //model function
            'relation_col' => 'league_id',
            'dataSource' => 'App\Season',
            'seperator' => 'player_penalties_season_id',

            'ajax' => true,
            'ajax_url' => '/getSeasonsL',
            'search' => true,
            'hoop' => true,

            'title' => 'words.season',
            'description' => '',
        ],
        'match_id' => [
            'type' => 'select2',
            'validation' => 'required',
            //'multiple' => 'multiple',

            //'relationship' => 'team',
            'relation_col' => 'season_id',
            //'dataSource' => 'App\TeamTeamSeason',
            'seperator' => 'player_penalties_match_id',

            'ajax' => true,
            'ajax_url' => '/getSeasonMatches',
            'search' => true,
            'hoop' => true,
            //'trigger' => 'season_id',

            'title' => 'words.match',
            'description' => '',
        ],
        'description' => [
            'type' => 'textarea',
            'validation' => 'nullable',
            'title' => 'words.description',
            'placeholder' => 'words.description',
            'description' => '',
        ],
        'penalties_type_id' => [
            'type' => 'select2',
            'validation' => 'nullable',
            //'multiple' => 'multiple',

            //'relationship' => 'team',
            //'relation_col' => 'season_id',
            //'dataSource' => 'App\TeamTeamSeason',
            'seperator' => 'ppenalties_type_id',

            'ajax' => true,
            'ajax_url' => '/getPlayerPenaltyTypesS',
            'search' => true,
            'hoop' => true,
            //'trigger' => 'season_id',

            'title' => 'words.penalty_type',
            'description' => '',
        ],
        'match_player_id' => [
            'type' => 'select2',
            'validation' => 'required',

            //'relationship' => 'team',
            'relation_col' => 'match_id',
            //'dataSource' => 'App\TeamTeamSeason',
            'seperator' => 'player_penalties_match_player_id',

            'ajax' => true,
            'ajax_url' => '/getMatchPlayer',
            'search' => true,
            'hoop' => true,
            //'trigger' => 'season_id',

            'title' => 'words.player',
            'description' => '',
        ],
    ],

    'team_penalties' => [
        'polymorphic_ctype_id' => [
            'type' => 'hidden',
            'defaultValue' => '47',
            'validation' => 'nullable',
            'title' => '',
            'placeholder' => '',
            'description' => '',
        ],
        'league_id' => [
            'type' => 'select2',
            'validation' => 'required',
            'dataSource' => 'App\League',
            'title' => 'words.league',
            'description' => '',
        ],
        'season_id' => [
            'type' => 'select2',
            'validation' => 'required',

            //'relationship' => 'league_eliminationseason', //model function
            'relation_col' => 'league_id',
            'dataSource' => 'App\Season',
            'seperator' => 'team_penalties_season_id',

            'ajax' => true,
            'ajax_url' => '/getSeasonsL',
            'search' => true,
            'hoop' => true,

            'title' => 'words.season',
            'description' => '',
        ],
        'match_id' => [
            'type' => 'select2',
            'validation' => 'required',
            //'multiple' => 'multiple',

            //'relationship' => 'team',
            'relation_col' => 'season_id',
            //'dataSource' => 'App\TeamTeamSeason',
            'seperator' => 'team_penalties_match_id',

            'ajax' => true,
            'ajax_url' => '/getSeasonMatches',
            'search' => true,
            'hoop' => true,
            //'trigger' => 'season_id',

            'title' => 'words.match',
            'description' => '',
        ],
        'description' => [
            'type' => 'textarea',
            'validation' => 'nullable',
            'title' => 'words.description',
            'placeholder' => 'words.description',
            'description' => '',
        ],
        'penalties_type_id' => [
            'type' => 'select2',
            'validation' => 'nullable',
            //'multiple' => 'multiple',

            //'relationship' => 'team',
            //'relation_col' => 'season_id',
            //'dataSource' => 'App\TeamTeamSeason',
            'seperator' => 'tpenalties_type_id',

            'ajax' => true,
            'ajax_url' => '/getTeamPenaltyTypesS',
            'search' => true,
            'hoop' => true,
            //'trigger' => 'season_id',

            'title' => 'words.penalty_type',
            'description' => '',
        ],
        'match_team_id' => [
            'type' => 'select2',
            'validation' => 'required',

            //'relationship' => 'team',
            'relation_col' => 'match_id',
            //'dataSource' => 'App\TeamTeamSeason',
            'seperator' => 'team_penalties_match_team_id',

            'ajax' => true,
            'ajax_url' => '/getMatchTeam',
            'search' => true,
            'hoop' => true,
            //'trigger' => 'season_id',

            'title' => 'words.team',
            'description' => '',
        ],
    ],


    'penalty_types' => [
        'name' => [
            'type' => 'text',
            'validation' => 'required|max:100',
            'title' => 'words.name',
            'placeholder' => 'words.name',
            'description' => '',
        ],
        'to_who' => [
            'type' => 'select2',
            'validation' => 'required',
            'dataSource' => 'json_data',
            'title' => 'words.to_who',
            'description' => '',
            'json_data' => [
                1 => ['id' => 'player', 'name' => 'words.player'],
                2 => ['id' => 'team', 'name' => 'words.team'],
            ]
        ],
    ],


];