<?php

return [

    'users' => [
        'tab_01' => [
            'title' => 'words.general',
            'icon' => 'fas fa-user',
            'form' => 'users',
        ],
        'tab_02' => [
            'title' => 'words.profile_photo',
            'icon' => 'fas fa-image',
            'content' => 'user_photo',
        ],
    ],
    'teams' => [
        'tab_01' => [
            'title' => 'words.general',
            'icon' => 'fas fa-users',
            'form' => 'teams',
        ],
        'tab_02' => [
            'title' => 'words.seasons',
            'icon' => 'fas fa-box',
            'content' => 'seasons',
            'relationship' => 'team_teamseason',
        ],
        'tab_03' => [
            'title' => 'words.squad',
            'icon' => 'fas fa-user-friends',
            'content' => 'players',
            'relationship' => 'player',
            //'form' => 'team_custom_details',
        ],
        /*
        'tab_04' => [
            'title' => 'words.termination_status',
            'icon' => 'fas fa-times-circle text-danger',
            'content' => 'team_status',
        ],
        */
        'tab_05' => [
            'title' => 'words.team_images',
            'icon' => 'fas fa-image',
            'content' => 'team_photo',
        ],
    ],
    'matches' => [
        'tab_01' => [
            'title' => 'words.general',
            'icon' => 'fas fa-futbol',
            'form' => 'matches',
        ],
        'tab_02' => [
            'title' => 'words.video',
            'icon' => 'fas fa-play-circle',
            'form' => 'match_video',
            'relationship' => 'match_video',
        ],
        'tab_03' => [
            'title' => 'words.players',
            'icon' => 'fas fa-user',
            'content' => 'match_player',
            'form' => 'match_player',
            'relationship' => 'match_player',
        ],
        'tab_04' => [
            'title' => 'words.match_actions',
            'icon' => 'fas fa-angle-double-right',
            'content' => 'match_action',
            'form' => 'match_action',
            'relationship' => 'match_action',
        ],
        'tab_05' => [
            'title' => 'words.panoramas',
            'icon' => 'fas fa-map',
            'content' => 'match_panorama',
            'form' => 'match_panorama',
            'relationship' => 'match_panorama',
        ],
        'tab_06' => [
            'title' => 'words.press_conference',
            'icon' => 'fas fa-microphone-alt',
            'content' => 'match_press',
            'form' => 'match_press',
            'relationship' => 'match_press',
        ],
        'tab_07' => [
            'title' => 'words.photos',
            'icon' => 'fas fa-image',
            'content' => 'match_image',
        ],
        'tab_08' => [
            'title' => 'words.otherstatistics',
            'icon' => 'fas fa-signal',
            'form' => 'match_statistic',
        ],
        'tab_09' => [
            'title' => 'words.heatmap',
            'icon' => 'fas fa-image',
            'content' => 'match_heatmap',
        ],
    ],
    'reservations' => [
        'tab_01' => [
            'title' => 'words.general',
            'icon' => 'fas fa-futbol',
            'form' => 'reservations',
        ],
        'tab_02' => [
            'title' => 'words.orders',
            'icon' => 'fas fa-play-circle',
            'content' => 'reservation_offer',
            'form' => 'reservation_offer_convert_match'
        ],
    ],

];